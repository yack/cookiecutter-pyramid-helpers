cookiecutter-pyramid-helpers
============================

A Cookiecutter (project template) for creating a Pyramid-Helpers starter project.

* template language: Mako
* persistent backend: SQLAlchemy
* mapping of URLs to routes: URL dispatch

Available themes
----------------

* AdminLTE: AdminLTE 4 Admin Dashboard Template

Requirements
------------

* Python 3.7+
* [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/installation.html)

Usage
-----

```
$ cookiecutter <directory containing this file>
```
