import os
import requests
import shutil
from textwrap import dedent
import zipfile

THEMES_DEPENDENCIES = {
    'adminlte': [
        # 'adminlte',
        'babel',
        'bootstrap5',
        'bootstrap-icons',
        'easepick',
        'fontawesome',
        'handlebars',
        'inputmask',
        'inter',
        'moment',
        'popper',
        'overlay-scrollbars',
        'pretty-checkbox-css',
        'tom-select',
    ],
}

STATIC_DEPENDENCIES = {
    key: value
    for key, value in {{ cookiecutter.static_dependencies }}.items()
    if key in THEMES_DEPENDENCIES['{{ cookiecutter.theme }}']
}


def main():
    install_static_dependencies()
    install_theme()

    print(dedent(
        """
        %(separator)s
        Documentation: https://docs.pylonsproject.org/projects/pyramid/en/latest/
        Tutorials:     https://docs.pylonsproject.org/projects/pyramid_tutorials/en/latest/
        Twitter:       https://twitter.com/PylonsProject
        Mailing List:  https://groups.google.com/forum/#!forum/pylons-discuss
        %(separator)s

        Change directory into your newly created project.
            cd {{ cookiecutter.repo_name }}

        Copy and adapt configuration files
            cp -a conf .conf
            cp -a env.dist env.local

        Enable direnv and create virtualenv if needed
            direnv allow

        Upgrade packaging tools.
            pip install --upgrade pip setuptools wheel

        Install poetry.
            pip install poetry

        Install application in development mode
            poetry install --extras "{source|binary} [auth-ldap] [auth-radius]"

        Initialize translations (with French as first locale)
            invoke i18n.extract
            invoke i18n.init fr
            invoke i18n.generate

        Initialize DB.
            {{ cookiecutter.repo_name }}-init-db .conf/application.ini

        Run web server in development mode.
            invoke service.httpd
        """ % {
            'separator': '=' * 79,
        }
    ))


def install_static_dependencies():
    static_path = os.path.join('{{cookiecutter.module_name}}', 'static')

    for dependency, data in STATIC_DEPENDENCIES.items():
        if 'url' in data:
            urls = [data['url']]
        elif data.get('urls'):
            urls = data['urls']

        urls = [
            url.format(**data)
            for url in urls
        ]

        # Check if file was previously downloaded
        target_path = os.path.join(static_path, data['relpath'].format(**data))

        print(f'Install {dependency}')

        for url in urls:
            if data.get('filename'):
                filename = data['filename'].format(**data)
            else:
                filename = url.split('/')[-1]

            if filename.endswith('.zip') and not data.get('noprefix'):
                dirpath = os.path.dirname(target_path)
            else:
                dirpath = target_path

            os.makedirs(dirpath, exist_ok=True)

            filepath = os.path.join(dirpath, filename)

            print(f'\tDownload {filename} into {dirpath}')

            r = requests.get(url)
            with open(filepath, 'wb+') as fp:
                fp.write(r.content)

            if filename.endswith('.zip'):
                print('\tDecompress ZIP archive')

                members = data.get('extract')
                if members and not data.get('noprefix'):
                    prefix = os.path.basename(target_path)
                    members = [
                        os.path.join(prefix, member)
                        for member in members
                    ]

                with zipfile.ZipFile(filepath, 'r') as zip_file:
                    zip_file.extractall(dirpath, members=members)

                os.remove(filepath)


def install_theme():
    locations = ['templates', 'static/css']

    for location in locations:
        path = os.path.join('{{cookiecutter.module_name}}', location)
        themes_path = os.path.join(path, 'themes')
        theme_path = os.path.join(themes_path, '{{cookiecutter.theme}}')

        for file_or_dir in os.listdir(theme_path):
            src = os.path.join(theme_path, file_or_dir)
            dst = os.path.join(path, file_or_dir)

            if os.path.exists(dst) and os.path.isdir(dst):
                shutil.rmtree(dst)

            shutil.move(src, dst)

        shutil.rmtree(themes_path)


if __name__ == '__main__':
    main()
