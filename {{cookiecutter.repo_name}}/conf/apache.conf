<Virtualhost *:80>
    ServerName {{ cookiecutter.repo_name }}.domain.tld

    DocumentRoot /var/www/empty

    Redirect / https://{{ cookiecutter.repo_name }}.domain.tld/

    ErrorLog ${APACHE_LOG_DIR}/{{ cookiecutter.repo_name }}.domain.tld-error.log
    CustomLog ${APACHE_LOG_DIR}/{{ cookiecutter.repo_name }}.domain.tld-access.log combined
</VirtualHost>

<Virtualhost *:443>
    ServerName  {{ cookiecutter.repo_name }}.domain.tld

    # SSL
    SSLEngine On
    SSLCertificateFile    /etc/letsencrypt/live/{{ cookiecutter.repo_name }}.domain.tld/fullchain.pem
    SSLCertificateKeyFile /etc/letsencrypt/live/{{ cookiecutter.repo_name }}.domain.tld/privkey.pem

    # Custom error documents
    # 403 and 404 errors are handled by application
    ErrorDocument 500 /static/error-500-fr.html

    # data/static should be a symbolic link to the real static directory
    Alias /static /srv/{{ cookiecutter.repo_name }}/data/static
    <Directory /srv/{{ cookiecutter.repo_name }}/data/static>
        Options +SymLinksIfOwnerMatch
        Require all granted
    </Directory>

    ####################
    # Gunicorn
    ####################

    # Uncomment the following lines when authentication is performed by Apache
    # RewriteEngine on
    # RewriteCond %{REMOTE_USER} (.*)
    # RewriteRule .* - [E=X_REMOTE_USER:%1]
    # RequestHeader set X-FORWARDED-USER %{X_REMOTE_USER}e

    # Uncomment the following lines when authentication is performed by application
    # ProxyPreserveHost on
    # RequestHeader set X-Forwarded-Proto "https"

    ProxyPass           /static !
    ProxyPass           /       http://127.0.0.1:8000/ retry=1 timeout=300
    ProxyPassReverse    /       http://127.0.0.1:8000/

    ErrorLog ${APACHE_LOG_DIR}/{{ cookiecutter.repo_name }}.domain.tld-error.log
    CustomLog ${APACHE_LOG_DIR}/{{ cookiecutter.repo_name }}.domain.tld-access.log combined
</VirtualHost>
