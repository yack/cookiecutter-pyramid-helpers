# {{ cookiecutter.project_name }}

Here is {{ cookiecutter.project_name }} description.


## Development
The project is managed using [Poetry](https://poetry.eustace.io/docs/#installation):
```
# Enable direnv and create virtualenv if needed
direnv allow

# Upgrade packaging tools
pip install --upgrade pip setuptools wheel

# Install Poetry
pip install poetry

# Install application in development mode
poetry install --extras "{source|binary} [auth-ldap] [auth-radius]"
invoke i18n.generate

# Copy data template
cp -a data .data

# Copy and adapt configuration files
cp -a conf .conf
cp -a env.dist env.local

# Initialize database
{{ cookiecutter.repo_name }}-init-db .conf/application.ini

# Run web server in development mode
invoke service.httpd

# Run static and functional tests
invoke test
```

### Extra dependencies

| extra       | package                                                      | description                 | build prerequisites                                                                              |
|-------------|--------------------------------------------------------------|-----------------------------|--------------------------------------------------------------------------------------------------|
| source      | [psycopg2](https://pypi.org/project/psycopg2/)               | PostgreSQL database adapter | [build prerequisites](https://www.psycopg.org/docs/install.html#build-prerequisites)             |
| binary      | [psycopg2-binary](https://pypi.org/project/psycopg2-binary/) | PostgreSQL database adapter | -                                                                                                |
| auth-ldap   | [python-ldap](https://pypi.org/project/python-ldap/)         | LDAP client                 | [build prerequisites](https://www.python-ldap.org/en/latest/installing.html#build-prerequisites) |
| auth-radius | [pyrad](https://pypi.org/project/pyrad/)                     | RADIUS tools                | -                                                                                                |


### Useful commands
Fix "TooManyRedirects" poetry error
```
poetry cache clear --all pypi
```


## Tests
### Static code validation
```
# ESLint
invoke test.eslint

# flake8
invoke test.flake8

# pylint
invoke test.pylint

# All
invoke test.static
```


### Functional tests
```
# API views
invoke test.functional --test='test_51_api_views.py'

# All
invoke test.functional
```


## I18n
Extract messages
```
invoke i18n.extract
invoke i18n.update
```

Create new language
```
invoke i18n.init {locale_name}
```

NOTE: Don't forget to adjust configuration accordingly (`i18n.available_languages` and `i18n.default_languages`).

Compile catalogs and update JSON files
```
invoke i18n.generate
```

## Installation

```
pip install {{ cookiecutter.repo_name }}

{{ cookiecutter.repo_name }}-init-db conf/application.ini
```
