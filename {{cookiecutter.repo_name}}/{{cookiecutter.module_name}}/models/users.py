# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pylint: disable=access-member-before-definition,attribute-defined-outside-init

""" {{ cookiecutter.project_name }} data models """

import datetime
import logging

from passlib.context import CryptContext
from passlib.handlers.ldap_digests import ldap_crypt_schemes

from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Text
from sqlalchemy import Unicode

from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship

from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.models import ArrayType
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models import Base

log = logging.getLogger(__name__)


class Search(Base):
    """ ORM class mapped to searches table """

    __tablename__ = 'searches'

    # Primary key
    id = Column(Integer, primary_key=True)

    # Foreign keys
    user_id = Column(Integer(), ForeignKey('users.id'))

    # Fields
    creation_date = Column(DateTime(timezone=True), nullable=False)
    description = Column(Text)
    label = Column(Unicode(255), nullable=False)
    modification_date = Column(DateTime(timezone=True), nullable=False)
    parameters = Column(ArrayType(), nullable=False)
    shared = Column(Boolean(), nullable=False)
    target = Column(Unicode(255), nullable=False)

    # Relationship
    user = relationship('User', backref=backref('searches', cascade='all, delete-orphan', lazy='dynamic', passive_deletes=True))

    @property
    def batch(self):
        """ Flag that indicates search is a batch """

        return bool(self.parameters.get('selected_ids'))

    @property
    def schema(self):
        """ Get FormEncode schema """

        config = get_config(self.target)
        if config is None:
            raise ValueError(f'Invalid target: {self.target}')

        return config['search_form']

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.now(datetime.timezone.utc)

        if 'description' in data:
            self.description = data['description']

        if 'label' in data:
            self.label = data['label']

        if 'parameters' in data:
            self.parameters = data['parameters']

        if 'shared' in data:
            self.shared = data['shared']

        if 'target' in data:
            self.target = data['target']

        if 'user' in data:
            self.user = data['user']

        # Sanity checks
        if self.parameters is None:
            self.parameters = {}

        if self.shared is None:
            self.shared = False

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def to_dict(self, context=None):
        """ Dump search data to dict """

        data = {
            'id': self.id,
            'description': self.description,
            'label': self.label,
            'shared': self.shared,
            'target': self.target,
        }

        if context == 'brief':
            return data

        if context:
            data['user'] = self.user.to_dict(context='brief') if self.user else None

            if context == 'search':
                return data
        else:
            data['user'] = self.user

        data.update({
            'creation_date': self.creation_date,
            'modification_date': self.modification_date,
            'parameters': self.parameters.copy(),
        })

        return data

    def to_python(self, state=None):
        """ Get Python values from form parameters """

        data = self.parameters.copy()
        if self.batch:
            return data

        return self.schema.to_python(data, state=state)


class User(Base):
    """ ORM class mapped to users table """

    __tablename__ = 'users'

    PROFILES = {
        'super-admin': N_('Super administrator'),
        'admin': N_('Administrator'),
        'user': N_('Simple user'),
    }

    STATUSES = {
        'active': N_('Active'),
        'disabled': N_('Disabled'),
    }

    STATUSES_COLORS = {
        'active': 'success',
        'disabled': 'warning',
    }

    STATUSES_ICONS = {
        'active': 'check',
        'disabled': 'pause',
    }

    STATUS_FRAMEWORK = {
        'active': ['disabled'],
        'disabled': ['active'],
    }

    # Primary key
    id = Column(Integer, primary_key=True)

    # Fields
    creation_date = Column(DateTime(timezone=True), nullable=False)
    description = Column(Text)
    modification_date = Column(DateTime(timezone=True), nullable=False)
    status = Column(Unicode(255), nullable=False)

    email = Column(Unicode(255))
    firstname = Column(Unicode(255))
    lastname = Column(Unicode(255))
    password = Column(Unicode(255))
    profiles = Column(ArrayType(), nullable=False)
    timezone = Column(Unicode(255))
    token = Column(Unicode(255))
    username = Column(Unicode(255), nullable=False, unique=True)

    @property
    def fullname(self):
        """ Compute fullname from firstname, lastname or username """

        fullname = []
        if self.firstname:
            fullname.append(self.firstname)
        if self.lastname:
            fullname.append(self.lastname)
        if not fullname:
            fullname.append(self.username)

        return ' '.join(fullname)

    def from_dict(self, data):
        """ Load data from dict """

        utcnow = datetime.datetime.now(datetime.timezone.utc)

        if 'description' in data:
            self.description = data['description']

        if 'email' in data:
            self.email = data['email']

        if 'firstname' in data:
            self.firstname = data['firstname']

        if 'lastname' in data:
            self.lastname = data['lastname']

        if 'profiles' in data:
            self.profiles = data['profiles']

        if 'status' in data:
            self.status = data['status']

        if 'timezone' in data:
            self.timezone = data['timezone']

        if 'token' in data:
            self.token = data['token']

        if 'username' in data:
            # Transform username to lowercase
            self.username = data['username'].lower()

        # Sanity checks
        if self.status is None:
            self.status = 'active'

        if self.creation_date is None:
            self.creation_date = data.get('creation_date') or utcnow

        if 'password' in data:
            self.set_password(data['password'])

        if data.get('modification_date'):
            self.modification_date = data['modification_date']

        elif DBSession.is_modified(self):
            self.modification_date = utcnow

        return DBSession.is_modified(self)

    def set_password(self, password, scheme='ldap_sha512_crypt'):
        """ Hash and set password """

        if password is None:
            self.password = None
            return

        if self.status != 'active':
            return

        if self.validate_password(password):
            # Password is unchanged
            return

        # Encrypt password
        ctx = CryptContext(default=scheme, schemes=ldap_crypt_schemes)
        self.password = ctx.hash(password)

    def to_dict(self, context=None):
        """ Dump data to dict """

        data = {
            'id': self.id,
            'description': self.description,
            'email': self.email,
            'fullname': self.fullname,
            'status': self.status,
        }

        if context in ('brief', 'search'):
            return data

        data.update({
            'creation_date': self.creation_date,
            'firstname': self.firstname,
            'lastname': self.lastname,
            'modification_date': self.modification_date,
            'profiles': self.profiles,
            'timezone': self.timezone,
            'token': self.token,
            'username': self.username,
        })

        return data

    def validate_password(self, password):
        """ Validate password """

        if self.status != 'active':
            return False

        if self.password is None:
            return False

        # Verify password
        ctx = CryptContext(schemes=ldap_crypt_schemes)
        try:
            validated = ctx.verify(password, self.password)
        except ValueError:
            log.exception('Failed to verify password using CryptContext.verify() for user #%d', self.id)
            validated = False

        return validated
