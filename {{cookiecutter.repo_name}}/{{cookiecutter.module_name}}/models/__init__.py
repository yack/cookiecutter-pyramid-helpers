# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} data models """

import json
import logging

from sqlalchemy import TypeDecorator
from sqlalchemy import Unicode
from sqlalchemy import engine_from_config

from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from zope.sqlalchemy import register

Base = declarative_base()


log = logging.getLogger(__name__)


class ArrayType(TypeDecorator):
    """ Custom Array type decorator for SQLite """

    impl = Unicode
    python_type = list

    def process_bind_param(self, value, dialect):
        """ Dump value to JSON """
        return json.dumps(value)

    def process_literal_param(self, value, dialect):
        """ Return value as is """
        return value

    def process_result_value(self, value, dialect):
        """ Load value from JSON """
        return json.loads(value)

    def copy(self, **kw):
        """ Copy object """
        return ArrayType(self.impl.length, **kw)


# Create ORM objects
DBSession = scoped_session(sessionmaker())
register(DBSession)


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('{{ cookiecutter.module_name }}.models')
    """

    registry = config.registry
    settings = registry.settings

    # Initialize model
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    log.info('[MODELS] Initialization complete: dialect=%s, engine=%s, url=%r', engine.dialect.name, engine.driver, engine.url)
