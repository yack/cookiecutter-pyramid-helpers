# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} resources """

from pyramid.authorization import Allow
from pyramid.authorization import Authenticated


ALL_PERMISSIONS = [
    # Global
    'index',
    'api-doc',
    'import',
    'profile',
    'select',

    # Batches
    'batches',
    'batches.users',
    'batches.users.delete',
    'batches.users.status',

    # Searches
    'searches.advanced',
    'searches.create',
    'searches.delete',
    'searches.modify',
    'searches.visual',

    # Users
    'users.create',
    'users.delete',
    'users.export',
    'users.import',
    'users.modify',
    'users.search',
    'users.select',
    'users.visual',
]


class Root:
    """ Base class for {{ cookiecutter.project_name }} resources """

    # pylint: disable=too-few-public-methods

    __acl__ = [
        (Allow, Authenticated, (
            'index',
            'profile',
        )),

        (Allow, 'profile:user', (
            # Searches
            'searches.create',
            'searches.delete',
            'searches.modify',
            'searches.visual',

            # Users
            'users.export',
            'users.search',
            'users.visual',
        )),

        (Allow, 'profile:admin', (
            # Global
            'api-doc',
            'import',
            'select',

            # Batches
            'batches',
            'batches.users',
            'batches.users.delete',
            'batches.users.status',

            # Searches
            'searches.advanced',
            'searches.create',
            'searches.delete',
            'searches.modify',
            'searches.visual',

            # Users
            'users.create',
            'users.delete',
            'users.export',
            'users.import',
            'users.modify',
            'users.search',
            'users.select',
            'users.visual',
        )),

        (Allow, 'profile:super-admin', ALL_PERMISSIONS),
    ]

    def __init__(self, request):
        self.request = request
