/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ESLint options */
/* global formValues, language, messages, templates */
/* global getScrollable, getStylePropertyValue, pluralize, toCamelCase, translate */


/*
 * API error callback
 */
async function apiErrorCallback(response) {
    const isJson = response.headers.get('content-type')?.includes('application/json');

    if (response.status >= 500) {
        notify.error(translate('Failed to communicate with remote server'));
        return;
    }

    const data = isJson ? await response.json() : null;
    if (data?.message) {
        notify.error(data.message);
        return;
    }

    if (response.status == 403) {
        notify.error(translate('Access denied'));
    }
    else {
        notify.error(translate('An unexplained error occurred'));
    }
}


/*
 * API form setup
 */
function clearFormErrors(form) {
    form.querySelectorAll('.form-error').forEach(element => { element.remove(); });
}


function setFormErrors(form, errors) {
    let field;
    let message;
    let parts;
    let position;
    let values;

    for (let key in errors) {
        message = errors[key];

        field = form.querySelector('[name="' + key + '"]');
        if (!field) {
            // Field is multiple - field data type is NumberList, StringList or ForEach
            parts = key.split('-');
            if (parts.length != 2) {
                continue;
            }

            field = form.querySelector('[name="' + parts[0] + '"]');
            if (!field) {
                continue;
            }

            position = parseInt(parts.pop(1));

            if (!isNaN(position)) {
                values = field.value.split(',');
                message = values[position] + ': ' + message;
            }
        }

        field.after(document.createElementFromString(templates.formError({message: message})));
    }
}


function apiFormSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('form.api-form').forEach(element => {
        element.onsubmit = function(evt) {
            evt.preventDefault();

            function callAPI() {
                let loading;
                if (element.dataset.loading) {
                    loading = document.createElementFromString(templates.loading());
                    document.body.append(loading);
                }

                let action = element.getAttribute('action');
                let method = element.getAttribute('method') || 'post';

                clearFormErrors(element);

                let data = new FormData(element);

                fetch(action, {
                    method: method,
                    body: data,
                }).then(response => {
                    // Check for error response
                    if (!response.ok && response.status != 400) {
                        return Promise.reject(response);
                    }

                    return response.json();
                }).then(response => {
                    if (response.message) {
                        if (response.result) {
                            notify.success(response.message);
                        }
                        else {
                            notify.error(response.message);
                        }
                    }

                    if (response.result) {
                        // Reload the page
                        if (element.dataset.reload || response.reload) {
                            document.location.reload();
                        }
                        else if (response.location) {
                            document.location = response.location;
                        }
                        else {
                            // If form is in a modal dialog, close it
                            const modalEl = element.closest('.modal');
                            if (modalEl) {
                                bootstrap.Modal.getOrCreateInstance(modalEl).hide();
                            }
                            if (loading) {
                                loading.remove();
                            }
                        }
                    }
                    else if (response.errors) {
                        setFormErrors(element, response.errors);
                        if (loading) {
                            loading.remove();
                        }
                    }
                }).catch(async response => {
                    apiErrorCallback(response);
                    if (loading) {
                        loading.remove();
                    }
                });

                // Prevent «normal» submit when calling `form.submit()`
                return false;
            }

            if (element.dataset.confirmMessage) {
                confirmDialog({
                    title: '<i class="fa fa-check fa-fw me-2"></i> ' + translate('Confirmation is required'),
                    message: element.dataset.confirmMessage,
                    buttons: {
                        confirm: {
                            label: '<i class="fas fa-check fa-fw me-2"></i> ' + translate('OK'),
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: '<i class="fas fa-times fa-fw me-2"></i> ' + translate('Cancel'),
                            className: 'btn-secondary'
                        }
                    },
                    callback: function(result) {
                        if (!result) {
                            return;
                        }
                        callAPI();
                    }
                });
            }
            else {
                callAPI();
            }
        };
    });
}


/*
 * API links
 *
 * An API link is a link written like this:
 * <a href="/some/where" data-method="post" date-foo="bar">Do something using AJAX call to API</a>
 *
 * In this case, method employed to call the API will be `POST` and `{foo: "bar"}` will be sent to the service.
 * Default method is `GET`

 * On success the page is reloaded
 * On error return message is displayed
 */
function apiLinkSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('a.api-link').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            function callAPI() {
                let data;
                let loading;
                let method = 'GET';
                let modal;
                let url = new URL(element.getAttribute('href'), document.location);

                if (element.dataset.loading) {
                    loading = document.createElementFromString(templates.loading());
                    document.body.append(loading);
                }

                // Get HTTP method (default GET)
                if (element.dataset.method) {
                    method = element.dataset.method.toUpperCase();
                }

                if (method == 'GET') {
                    data = url.searchParams;
                }
                else {
                    data = new FormData();
                }

                for (let key in element.dataset) {
                    if (['confirmMessage', 'method', 'location', 'reload'].indexOf(key) == -1) {
                        data.append(key, element.dataset[key]);
                    }
                }

                // Are we in the context of a modal?
                if (element.dataset.modal) {
                    // Get modal element
                    modal = bootstrap.Modal.getOrCreateInstance(element.dataset.modal);
                }

                element.classList.add('disabled');

                if (modal) {
                    modal.show();
                }

                fetch(url, {
                    method: method,
                    body: method === 'GET' ? null : data,
                }).then(response => {
                    if (modal) {
                        modal.hide();
                    }

                    element.classList.remove('disabled');

                    // Check for error response
                    if (!response.ok) {
                        return Promise.reject(response);
                    }

                    return response.json();
                }).then(response => {
                    if (modal) {
                        modal.hide();
                    }

                    if (response.message) {
                        if (response.result) {
                            notify.success(response.message);
                        }
                        else {
                            notify.error(response.message);
                        }
                    }
                    if (response.result) {
                        if (element.dataset.reload) {
                            document.location.reload();
                        }
                        else if (element.dataset.location) {
                            document.location = element.dataset.location;
                        }
                        if (loading) {
                            loading.remove();
                        }
                    }
                }).catch(async response => {
                    apiErrorCallback(response);

                    if (loading) {
                        loading.remove();
                    }
                });
            }

            if (element.dataset.confirmMessage) {
                confirmDialog({
                    title: '<i class="fa fa-check fa-fw me-2"></i> ' + translate('Confirmation is required'),
                    message: element.dataset.confirmMessage,
                    buttons: {
                        confirm: {
                            label: '<i class="fas fa-check fa-fw me-2"></i> ' + translate('OK'),
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: '<i class="fas fa-times fa-fw me-2"></i> ' + translate('Cancel'),
                            className: 'btn-secondary'
                        }
                    },
                    callback: function(result) {
                        if (!result) {
                            return;
                        }
                        callAPI();
                    }
                });
            }
            else {
                callAPI();
            }
        };
    });
}


/*
 * Confirm dialog
 */
function confirmDialog(options) {
    const modalDiv = document.createElementFromString(templates.confirmDialog(options));
    const modal = new bootstrap.Modal(modalDiv);

    modal.result = false;

    modalDiv.addEventListener('hidden.bs.modal', _e => {
        options.callback(modal.result);
    });

    modalDiv.querySelector('button[name="confirm"]').onclick = function(evt) {
        evt.preventDefault();

        modal.result = true;
        modal.hide();
    };

    modal.show();
}


/*
 * Form reset button
 */
function formResetSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('form .form-reset, form .form-erase').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            let erase = element.classList.contains('form-erase');
            let form = element.closest('form');
            let formName = form.getAttribute('name');
            let values = formValues[formName];

            if (!values) {
                return;
            }

            for (let inputName in values) {
                let field = form.querySelector('[name="' + inputName + '"]');
                if (!field) {
                    continue;
                }
                let value = erase ? '' : values[inputName];

                if (field.getAttribute('type') == 'checkbox') {
                    field.checked = Boolean(value);
                }
                else {
                    field.value = value;
                }
                field.dispatchEvent(new Event('change'));
            }
        };
    });
}


/*
 * Nav tabs setup
 */
function navTabsSetup() {
    let anchor = document.location.hash;

    if (anchor) {
        const button = document.querySelector('.card-header-pills .nav-link[href="' + anchor + '"]');
        if (button) {
            bootstrap.Tab.getOrCreateInstance(button).show();
        }
    }

    document.querySelectorAll('.card-header-pills a[data-bs-toggle=pill]').forEach(button => {
        button.addEventListener('click', event => {
            document.location.hash = event.target.attributes.href.value;
        });
    });
}


/*
 * Partial block setup
 */
function partialBlockSetup(context) {
    if (!context) {
        context = document;
    }

    /* Handle «API» forms */
    apiFormSetup(context);

    /* Handle «API» links */
    apiLinkSetup(context);

    /* Handle form reset button */
    formResetSetup(context);

    /* Handle text expanders */
    textExpanderSetup(context);

    context.querySelectorAll('a.partial-link').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            let url = new URL(element.getAttribute('href'), document.location);

            let loading;
            let target;

            let targetSelector = element.dataset.target;
            if (targetSelector) {
                target = document.querySelector(targetSelector);
            }
            else {
                target = element.closest('.partial-block');
            }

            let partialKey = target.dataset.partialKey;
            let partialMethod = target.dataset.partialMethod;

            url.searchParams.set(partialKey, 'true');

            if (partialMethod == 'append') {
                element.classList.add('loading');
            }
            else {
                loading = document.createElementFromString(templates.loading());
                target.append(loading);
            }

            fetch(url, {
                method: 'GET',
            }).then(response => {
                // Check for error response
                if (!response.ok) {
                    return Promise.reject(response);
                }

                return response.text();
            }).then(response => {
                if (partialMethod == 'append') {
                    if (response) {
                        element.remove();

                        let div = document.createElement('DIV');
                        div.innerHTML = response;
                        target.append(div);
                    }
                    else {
                        element.classList.remove('loading');
                    }
                }
                else if (response) {
                    target.innerHTML = response;
                }
                else {
                    loading.remove();
                }

                // Scroll to target
                let scrollable = getScrollable(target);
                if (scrollable) {
                    let paddingTop = getStylePropertyValue(scrollable, 'padding-top', true);
                    scrollable.scrollTo({top: target.offsetTop - paddingTop - 20, behaviour: 'smooth'});
                }

                // Trigger «load» event
                target.dispatchEvent(new Event('load'));
            }).catch(async response => {
                apiErrorCallback(response);
            });
        };
    });
}


/*
 * Text expander setup
 */
function textExpanderSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('a.text-expander').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            element.nextElementSibling.classList.toggle('d-none');
        };
    });
}


/*
 * Notifications
 */
const notify = function() {
    const DEFAULT_DELAY = 5000;

    const colors = {
        'error': 'text-danger',
        'info': 'text-primary',
        'success': 'text-success',
        'warning': 'text-warning'
    };

    const icons = {
        'error': 'fa-exclamation-triangle',
        'info': 'fa-info-circle',
        'success': 'fa-check-circle',
        'warning': 'fa-exclamation-triangle'
    };

    const titles = {
        'error': 'Error',
        'info': 'Information',
        'success': 'Information',
        'warning': 'Warning'
    };

    function display(type, message, title=null, delay=DEFAULT_DELAY) {
        let toasts = document.getElementById('toast-container');

        if (title === null) {
            title = translate(titles[type]);
        }

        let element = document.createElementFromString(templates.toast({
            color: colors[type],
            delay: delay,
            icon: icons[type],
            message: message,
            time: new Date().toLocaleTimeString(),
            title: title
        }));

        toasts.prepend(element);

        let toast = new bootstrap.Toast(toasts.firstChild);

        toast.show();
    }

    return {
        display: display,
        error: function(message, title, delay=DEFAULT_DELAY) { display('error', message, title, delay); },
        info: function(message, title, delay=DEFAULT_DELAY) { display('info', message, title, delay); },
        success: function(message, title, delay=DEFAULT_DELAY) { display('success', message, title, delay); },
        warning: function(message, title, delay=DEFAULT_DELAY) { display('warning', message, title, delay); },
    };
}();


/*
 * Global initialization
 */

/* Load Gettext translations */
const catalog = window['BABEL_TO_LOAD_' + language] || {};
const translations = babel.Translations.load(catalog);
/* exported translations */

/* Handlebars helpers */
Handlebars.registerHelper({
    translate: translate,
    pluralize: pluralize
});
Handlebars.registerHelper({
    not: function(v) {
        return !v;
    },
    eq: function(v1, v2) {
        return v1 === v2;
    },
    ne: function(v1, v2) {
        return v1 !== v2;
    },
    lt: function(v1, v2) {
        return v1 < v2;
    },
    gt: function(v1, v2) {
        return v1 > v2;
    },
    lte: function(v1, v2) {
        return v1 <= v2;
    },
    gte: function(v1, v2) {
        return v1 >= v2;
    },
    and: function(v1, v2) {
        return v1 && v2;
    },
    or: function(v1, v2) {
        return v1 || v2;
    },
    in: function(v1, v2) {
        if (!v1 || !v2) {
            return false;
        }
        return v2.includes(v1);
    },
    minus: function(v1, v2) {
        return v1 - v2;
    },
    plus: function(v1, v2) {
        return v1 + v2;
    },
    toJSON: function(v) {
        return JSON.stringify(v, null, 3);
    }
});

/* Handlebars templates */
document.querySelectorAll('script[type="text/x-handlebars-template"]').forEach(element => {
    templates[toCamelCase(element.id.replace('tpl-', ''))] = Handlebars.compile(element.innerHTML);
});

/* Popover */

// Override default options
Object.assign(
    bootstrap.Popover.Default,
    {
        container: 'body',
        html: true,
        trigger: 'hover'
    }
);

document.querySelectorAll('[data-bs-toggle="popover"]').forEach(element => {
    return new bootstrap.Popover(element);
});

/* Selects */

// Assign TomSelect default options
TomSelect.AutocompleteDefault = {
    create: false,
    pagerLimit: 10,
    maxOptions: null, // unlimited
    plugins: {
        change_listener: {},
        dropdown_input: {},
        virtual_scroll: {},
    },
};

TomSelect.Default = {
    dropdownParent: 'body',
    plugins: [
        'change_listener',
        'clear_button',
        'remove_button'
    ]
};

document.querySelectorAll('.form-select:not(.autocompleter)').forEach(element => {
    let options = structuredClone(TomSelect.Default);

    if (element.classList.contains('no-search')) {
        // Disable search if `no-search` class is set
        options.controlInput = null;
    }

    if (element.classList.contains('no-clear')) {
        // Remove `Clear button` plugin if 'no-clear' class is set
        options.plugins.splice(options.plugins.indexOf('clear_button'), 1);
    }

    if (element.classList.contains('tags')) {
        // Remove `Clear Button` and `Change Listener` plugins
        options.plugins.splice(options.plugins.indexOf('change_listener'), 1);
        options.plugins.splice(options.plugins.indexOf('clear_button'), 1);
        options.persist = false;
        options.create = true;
    }

    if (!element.hasAttribute('multiple') && !element.classList.contains('tags')) {
        // Remove `Remove button` plugin if select is not multiple
        options.plugins.splice(options.plugins.indexOf('remove_button'), 1);
    }

    if (element.tagName === 'SELECT') {
        // Remove `Clear button` plugin if no empty option
        let optionElements = element.querySelectorAll('option');
        if (optionElements.length > 0 && optionElements[0].value !== '') {
            options.plugins.splice(options.plugins.indexOf('clear_button'), 1);
        }
    }

    tomSelect(element, options);
});

/* Sidebar */

// Save state: open or collapse
setTimeout(() => {
    ['[data-lte-toggle="sidebar"]', '.sidebar-overlay'].forEach(function(selector) {
        ['collapse.lte.push-menu', 'open.lte.push-menu'].forEach(function(name) {
            document.querySelector(selector).addEventListener(name, evt => {
                // Sidebar state has been toggled: open <=> collapse

                // Save sidebar state in session
                const sidebarEl = evt.target;
                const url = sidebarEl.dataset.url;
                const params = new FormData();
                const state = evt.type == 'collapse.lte.push-menu' ? 'collapse' : 'open';

                params.append('sidebar_state', state);
                document.body.dataset.sidebarState = state;

                fetch(url, {
                    method: 'PUT',
                    body: params,
                }).then(response => {
                    // Check for error response
                    if (!response.ok) {
                        return Promise.reject(response);
                    }

                    return response.json();
                }).then(response => {
                    if (!response.result) {
                        notify.error(translate('Failed to save sidebar state'));
                    }
                }).catch(async response => {
                    notify.error(translate('Failed to save sidebar state'));
                    apiErrorCallback(response);
                });
            });
        });
    });
}, 400);

/* Tooltip */
bootstrap.Tooltip.Default.allowList.dl = [];
bootstrap.Tooltip.Default.allowList.dt = [];
bootstrap.Tooltip.Default.allowList.dd = [];
bootstrap.Tooltip.Default.allowList.tt = [];

document.querySelectorAll('[data-bs-toggle="tooltip"]').forEach(element => {
    // Init all tooltips
    return new bootstrap.Tooltip(element);
});

/* Handle required fields */
document.querySelectorAll('.form-required label.form-label:first-child').forEach(element => {
    let span = document.createElementFromString('<span class="text text-danger ms-1">*</span>');

    element.after(span);
});

/* Handle nav tabs */
navTabsSetup();

/* Handle partial blocks */
partialBlockSetup();

document.querySelectorAll('div.partial-block').forEach(element => {
    element.addEventListener('load', _e => {
        partialBlockSetup(element);
    });
});

/* Display flash messages */
for (let msg of messages) {
    notify[msg.type](msg.message, null, msg.wait);
}

/* Adjust .form-error vertical padding and margin bottom */
document.querySelectorAll('.form-error').forEach(element => {
    element.classList.add('py-1', 'mb-0');
});
/* Move .form-error div outside input-group */
document.querySelectorAll('.input-group .form-error').forEach(element => {
    element.parentElement.after(element);
});
