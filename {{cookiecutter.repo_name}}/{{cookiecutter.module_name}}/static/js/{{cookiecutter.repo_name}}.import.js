/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Global variables
 */
let columnsSelect = document.getElementById('columns');
let createOnlyCheckbox = document.getElementById('create-only');
let partialCheckbox = document.getElementById('partial');
let suffixInput = document.getElementById('suffix');


/*
 * Disable partial import when «create only» is activated
 */
function onCreateOnlyChange() {
    if (createOnlyCheckbox.checked) {
        partialCheckbox.checked = false;
        partialCheckbox.disabled = true;
        onPartialChange();
    }
    else {
        partialCheckbox.disabled = false;
    }
}


/*
 * Allow to select columns when partial import is activated
 */
function onPartialChange() {
    if (partialCheckbox.checked) {
        columnsSelect.tomselect.enable();
    }
    else {
        columnsSelect.tomselect.clear();
        columnsSelect.tomselect.disable();
    }
}


/*
 * Global initialization
 */
Inputmask('99999999[-**]').mask(suffixInput);

if (createOnlyCheckbox) {
    createOnlyCheckbox.onchange = onCreateOnlyChange;
    onCreateOnlyChange();
}

partialCheckbox.onchange = onPartialChange;
onPartialChange();
