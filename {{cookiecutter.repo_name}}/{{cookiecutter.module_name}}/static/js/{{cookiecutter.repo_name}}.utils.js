/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global translations */

/*
 * Add `.createElementFromString()` method to document
 * Create a new dom element from raw html
 */
document.createElementFromString = function(html) {
    let div = document.createElement('div');

    div.innerHTML = html;

    return div.firstElementChild;
};


/*
 * Get first scrollable ancestor
 */
function getScrollable(element) {
    if (element === null) {
        return null;
    }

    if (element.scrollHeight > element.clientHeight) {
        return element;
    }

    return getScrollable(element.parentNode);
}
/* exported getScrollable */


/*
 * Get a style property value
 */
function getStylePropertyValue(element, property, ispx=false) {
    let value = window.getComputedStyle(element, null).getPropertyValue(property);

    if (!value || !ispx) {
        return value;
    }

    return value.slice(0, -2);
}
/* exported getStylePropertyValue */


/*
 * Pad a string to width with character
 */
function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
/* exported pad */


/*
 * Transform a string to camel case
 */
function toCamelCase(str) {
    return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
}
/* exported toCamelCase */


/*
 * I18n
 */
function translate(string) {
    let translated = translations.gettext(string);
    return (translated !== '') ? translated : string;
}
/* exported translate */


function pluralize(singular, plural, n) {
    return translations.ngettext(singular, plural, n);
}
/* exported pluralize */
