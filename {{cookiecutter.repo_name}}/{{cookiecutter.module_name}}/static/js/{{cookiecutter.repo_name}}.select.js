/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ESLint options */
/* global translate */


/*
 * Global variables
 */
let inputText = document.querySelector('.input-upload input[type="text"]');
let inputFile = document.querySelector('.input-upload input[type="file"]');


/*
 * Display selected file name
 */
function onFileChange() {
    let filename = inputFile.value;
    inputText.value = filename || translate('No file selected');
}


/*
 * Global initialization
 */
inputFile.onchange = onFileChange;
onFileChange();
