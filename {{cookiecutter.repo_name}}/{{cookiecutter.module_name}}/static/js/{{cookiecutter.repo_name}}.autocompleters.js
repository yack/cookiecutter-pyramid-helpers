/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ESLint options */
/* exported autocompleter */
/* global notify, statuses, translate */


const autocompleter = function() {

    const autocompleters = {};

    /*
     * Generic auto-completer function
     */
    function initAutocompleter(element, options) {
        const endpoint = element.dataset.url;

        options = Object.assign(structuredClone(TomSelect.AutocompleteDefault), options);

        if (element.multiple) {
            options.plugins.checkbox_options = {};
            options.plugins.remove_button = {
                title: translate('Remove this item'),
            };
        }
        else if (!element.classList.contains('no-clear')) {
            options.plugins.clear_button = {};
        }

        if (!options.render_option) {
            // Default option renderer
            options.render_option = function(data, escape) {
                const optionEl = document.createElement('div');
                optionEl.className = 'd-flex align-items-center';

                const labelEl = document.createElement('span');
                labelEl.className = 'flex-grow-1';
                labelEl.innerText = escape(data[options.labelField]);
                if (data.description) {
                    labelEl.title = escape(data.description);
                }
                optionEl.append(labelEl);

                if (data.status) {
                    const status = statuses[data.status];
                    const statusEl = document.createElement('span');
                    statusEl.className = 'badge text-bg-' + status.color;
                    statusEl.innerText = escape(translate(status.label));

                    const iconEl = document.createElement('i');
                    iconEl.className = 'me-1 fa fa-' + status.icon;
                    statusEl.prepend(iconEl);

                    optionEl.append(statusEl);
                }

                return optionEl;
            };
        }

        function getParams(query, page) {
            let params = {};

            if (element.dataset.params) {
                params = Object.assign(params, JSON.parse(element.dataset.params));
            }

            if (options.params !== undefined) {
                params = Object.assign(params, options.params());
            }

            if (query) {
                params.term = query;
            }

            params[options.pagerId + '.limit'] = options.pagerLimit;
            params[options.pagerId + '.page'] = page || 1;

            return params;
        }

        function getUrl(params) {
            let searchParams = new URLSearchParams();

            for (let field in params) {
                if (Array.isArray(params[field])) {
                    params[field].forEach(value => { searchParams.append(field, value); });
                }
                else {
                    searchParams.append(field, params[field]);
                }
            }
            return endpoint + '?' + searchParams.toString();
        }

        let tomselectOptions = {
            valueField: options.valueField,
            labelField: options.labelField,
            searchField: options.searchField,
            create: options.create || element.dataset.create == 'true',
            maxItems: element.multiple ? null : 1,
            maxOptions: options.maxOptions,
            plugins: options.plugins,
            preload: 'focus',
            render: {
                loading_more: function() {
                    return '<div class="loading-more-results">' + translate('Loading more results…') + '</div>';
                },
                no_results: function() {
                    return '<div class="no-results">' + translate('No results found') + '</div>';
                },
                option: options.render_option,
            },
            firstUrl: function(query) {
                const params = getParams(query);

                return getUrl(params);
            },
            load: function(query, callback) {
                const url = this.getUrl(query);

                fetch(url)
                    .then(response => response.json())
                    .then(json => {
                        if (!json.result) {
                            callback();

                            const message = json.message ? json.message : translate('Failed to communicate with remote server');
                            notify.error(message);
                            return;
                        }

                        const page = json[options.pagerId].pager.page;
                        const pages = json[options.pagerId].pager.pages;

                        if (page < pages) {
                            const params = getParams(query, page + 1);
                            this.setNextUrl(query, getUrl(params));
                        }

                        callback(json[options.pagerId].items);
                    }).catch((_e) => {
                        callback();

                        notify.error(translate('Failed to communicate with remote server'));
                    });
            },
            onBlur: function(_e) {
                // Remove preload state to force preload on every focus
                this.wrapper.classList.remove('preloaded');
                this.getUrl();
                this.clearOptions();
            },
            shouldLoad: function(query) {
                return query.length >= 0;
            },
        };

        // Destroy any previously created instance
        if (typeof(element.tomselect) !== 'undefined') {
            element.tomselect.destroy();
        }

        if (!element.value || options.valueField == options.labelField) {
            tomSelect(element, tomselectOptions);
            element.dispatchEvent(new Event('ready'));
            return;
        }

        // Get options and selected items from API
        let selectedItems = element.value.split(',');

        let params = getParams();

        params.selected_ids = selectedItems;
        params[options.pagerId + '.limit'] = selectedItems.length;

        let url = getUrl(params);

        fetch(url)
            .then(response => response.json())
            .then(json => {
                if (!json.result) {
                    const message = json.message ? json.message : translate('Failed to communicate with remote server');
                    notify.error(message);
                    return;
                }

                tomselectOptions.options = json[options.pagerId].items;
                tomselectOptions.items = selectedItems;

                tomSelect(element, tomselectOptions);
                element.dispatchEvent(new Event('ready'));
            }).catch((_e) => {
                notify.error(translate('Failed to communicate with remote server'));
            });
    }


    /*
     * Auto-completer initialization
     */
    function init(selector, target, options) {
        if (!(target in autocompleters)) {
            return false;
        }

        if (typeof(options) === 'undefined') {
            options = {};
        }

        if (typeof(selector) === 'string') {
            document.querySelectorAll(selector).forEach(element => {
                autocompleters[target](element, options);
            });
        }
        else {
            autocompleters[target](selector, options);
        }

        return true;
    }


    /*
     * Register an auto-completer
     */
    function register(target, targetOptions) {
        autocompleters[target] = function(element, options) {
            return initAutocompleter(element, Object.assign(structuredClone(options), structuredClone(targetOptions)));
        };
    }


    return {
        'init': init,
        'register': register,
    };
}();


/* Register common auto-completers */
autocompleter.register('users', {
    pagerId: 'users',
    valueField: 'id',
    labelField: 'fullname',
    searchField: ['email', 'fullname'],
});


/*
 * Setup function
 */
function autocompleterSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('.autocompleter').forEach(element => {
        autocompleter.init(element, element.dataset.target);
    });
}


/* Initialization */
autocompleterSetup();

/* Handle partial blocks */
document.querySelectorAll('div.partial-block').forEach(element => {
    element.addEventListener('load', _e => {
        autocompleterSetup(element);
    });
});
