/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Global variables
 */

let formValues = {};
let language = null;
let messages = [];
let statuses = {};
let templates = {};
/* exported formValues, language, messages, statuses, templates */

/*
 * Dark/Light themes switching
 */

const getAutomaticTheme = () => {
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
};

const getPreferredTheme = () => {
    const storedTheme = localStorage.getItem('theme');
    if (storedTheme) {
        return storedTheme;
    }

    return getAutomaticTheme();
};

const setTheme = function (theme) {
    if (theme === 'auto') {
        theme = getAutomaticTheme();
    }

    document.documentElement.dataset.bsTheme = theme;

    const event = new Event('themechange');
    event.value = theme;

    document.documentElement.dispatchEvent(event);
};

setTheme(getPreferredTheme());

const showActiveTheme = (theme, focus = false) => {
    const themeSwitcher = document.querySelector('#bd-theme');

    if (!themeSwitcher) {
        return;
    }

    const themeSwitcherText = document.querySelector('#bd-theme-text');
    const activeThemeIcon = document.querySelector('.theme-icon-active i');
    const btnToActive = document.querySelector(`[data-bs-theme-value="${theme}"]`);
    const iconCheckToActive = document.querySelector(`[data-bs-theme-value="${theme}"] i:last-child`);
    const svgOfActiveBtn = btnToActive.querySelector('i').getAttribute('class');

    for (const element of document.querySelectorAll('[data-bs-theme-value]')) {
        element.classList.remove('active');
        element.setAttribute('aria-pressed', 'false');
        element.querySelector('i:last-child').classList.add('d-none');
    }

    btnToActive.classList.add('active');
    btnToActive.setAttribute('aria-pressed', 'true');
    iconCheckToActive.classList.remove('d-none');
    activeThemeIcon.setAttribute('class', svgOfActiveBtn);
    const themeSwitcherLabel = `${themeSwitcherText.textContent} (${btnToActive.dataset.bsThemeValue})`;
    themeSwitcher.setAttribute('aria-label', themeSwitcherLabel);

    if (focus) {
        themeSwitcher.focus();
    }
};

window.matchMedia('(prefers-color-scheme: dark)')
    .addEventListener('change', () => {
        const storedTheme = localStorage.getItem('theme');
        if (storedTheme !== 'light' || storedTheme !== 'dark') {
            setTheme(getPreferredTheme());
        }
    });

window.addEventListener('DOMContentLoaded', () => {
    showActiveTheme(getPreferredTheme());

    for (const toggle of document.querySelectorAll('[data-bs-theme-value]')) {
        toggle.addEventListener('click', () => {
            const theme = toggle.dataset.bsThemeValue;

            localStorage.setItem('theme', theme);
            setTheme(theme);
            showActiveTheme(theme, true);
        });
    }
});
