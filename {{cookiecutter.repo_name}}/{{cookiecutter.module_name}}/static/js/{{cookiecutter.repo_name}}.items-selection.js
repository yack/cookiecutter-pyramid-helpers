/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ESLint options */
/* global apiErrorCallback, confirmDialog, notify, translate */

/*
 * Update server side selection
 */
function callItemsSelectionAPI(context, url, params) {

    if (!context) {
        context = document;
    }

    function setAllChecked(checked) {
        context.querySelectorAll('input.select-one').forEach(element => { element.checked = checked; });
    }

    function setAllDisabled(disabled) {
        context.querySelectorAll('input.select-one').forEach(element => { element.disabled = disabled; });
    }

    // Disable checkboxes to prevent from user to click too fast
    setAllDisabled(true);

    let data = new URLSearchParams(params);

    fetch(url, {
        method: 'PUT',
        body: data,
    }).then(response => {
        setAllDisabled(false);

        // Check for error response
        if (!response.ok) {
            return Promise.reject(response);
        }

        return response.json();
    }).then(response => {
        if (!response.result) {
            notify.error(response.message ? response.message : translate('Failed to communicate with remote server'));
            return;
        }

        let btnClass;
        let icoClass;

        if (response.selected_count == 0) {
            // Make sure that no checkbox is checked
            setAllChecked(false);

            btnClass = ['btn-tool'];
            icoClass = ['far', 'fa-circle'];

            notify.info(response.message);
        }
        else if (response.selected_count == 1) {
            if (response.selected_count == response.matched_count) {
                setAllChecked(true);
            }

            btnClass = ['btn-secondary'];
            icoClass = ['far', 'fa-check-circle'];

            notify.info(response.message);
        }
        else if (response.selected_count == response.matched_count) {
            // Make sure that all checkboxes are checked
            setAllChecked(true);

            btnClass = ['btn-danger'];
            icoClass = ['fas', 'fa-exclamation-triangle', 'text-danger'];

            // Ask user for confirmation
            confirmDialog({
                title: '<i class="fa fa-list fa-fw me-2"></i> ' + translate('Selection'),
                message: '<b>' + response.message + '</b>' +
                    '<br><br>' +
                    translate('Make sure this is what you want before using any batch process or clear the selection.'),
                buttons: {
                    confirm: {
                        label: '<i class="fas fa-check fa-fw me-2"></i> ' + translate('Yes, continue'),
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: '<i class="fas fa-broom fa-fw me-2"></i> ' + translate('Clear selection'),
                        className: 'btn-secondary'
                    }
                },
                callback: function(result) {
                    if (!result) {
                        // Cancel: clear selection
                        callItemsSelectionAPI(context, url, {select_none: 1});
                        return;
                    }

                    // Ok: keep selection
                    notify.info(response.message);
                },
            });
        }
        else {
            if (params.select_list) {
                if (response.action == 'select') {
                    setAllChecked(true);
                }
                else {
                    setAllChecked(false);
                }
            }

            btnClass = ['btn-warning'];
            icoClass = ['fas', 'fa-exclamation-circle', 'text-warning'];

            notify.info(response.message);
        }

        if (!response.selected_count) {
            context.querySelector('.select-batch').classList.add('disabled');
        }
        else {
            context.querySelector('.select-batch').classList.remove('disabled');
        }

        let btn = context.querySelector('.dropdown-toggle');
        btn.classList.remove('btn-danger', 'btn-secondary', 'btn-tool', 'btn-warning');
        btn.classList.add(...btnClass);

        let ico = context.querySelector('.select-count > i');
        ico.classList.remove('fas', 'far', 'fa-check-circle', 'fa-circle', 'fa-exclamation-circle', 'fa-exclamation-triangle', 'text-danger', 'text-warning');
        ico.classList.add(...icoClass);

        if (response.message) {
            context.querySelector('.select-count > span').innerText = response.message;
        }
    }).catch(async response => {
        apiErrorCallback(response);
    });
}


function itemsSelectionSetup(context) {
    if (!context) {
        context = document;
    }

    context.querySelectorAll('a.select-all').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            callItemsSelectionAPI(context, this.getAttribute('href'), {select_all: 1});
        };
    });

    context.querySelectorAll('a.select-none').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            callItemsSelectionAPI(context, this.getAttribute('href'), {select_none: 1});
        };
    });

    context.querySelectorAll('a.select-page').forEach(element => {
        element.onclick = function(evt) {
            evt.preventDefault();

            let itemIds = [...context.querySelectorAll('input.select-one')].map(item => item.value);

            callItemsSelectionAPI(context, this.getAttribute('href'), {select_list: itemIds});
        };
    });

    context.querySelectorAll('input.select-one').forEach(element => {
        element.onchange = function(_evt) {
            callItemsSelectionAPI(context, this.dataset.url, {select_one: this.value});
        };
    });
}


/*
 * Initialization
 */
document.querySelectorAll('.items-selection').forEach(element => {
    element.addEventListener('load', _evt => {
        itemsSelectionSetup(element);
    });

    itemsSelectionSetup(element);
});
