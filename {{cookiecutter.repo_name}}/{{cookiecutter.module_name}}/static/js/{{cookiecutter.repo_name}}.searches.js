/*
 * {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
 * By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
 *
 * Copyright (C) {{ cookiecutter.copyright_name }}
 * {{ cookiecutter.project_url }}
 *
 * This file is part of {{ cookiecutter.project_name }}.
 *
 * {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ESLint options */
/* global apiErrorCallback, clearFormErrors, confirmDialog, notify, translate, setFormErrors */


/*
 * Searches
 */
let searches = function() {
    let infoDiv;
    let leaveButton;
    let menuDropdown;
    let saveButton;
    let saveForm;
    let saveModal;

    let search;
    let userId;


    function initMenu() {
        // Init input filter
        let input = document.getElementById('searches-input-filter');

        if (input) {
            input.addEventListener('input', e => {
                let term = e.target.value.toLowerCase();

                menuDropdown.querySelectorAll('div.dropdown-item:not(.dropdown-header)').forEach(item => {
                    let link = item.firstElementChild;
                    let label = link.innerText.toLowerCase();
                    let title = link.getAttribute('title').toLowerCase();

                    if (!term || label.search(term) >= 0 || title.search(term) >= 0) {
                        item.classList.remove('d-none');
                    }
                    else {
                        item.classList.add('d-none');
                    }
                });
            });
        }

        // Init delete buttons
        menuDropdown.querySelectorAll('.searches-del').forEach(item => {
            item.onclick = function(evt) {
                evt.preventDefault();

                let label = this.dataset.label;
                let searchId = this.dataset.id;
                let shared = this.dataset.shared === 'true';
                let url = this.getAttribute('href');

                let message = babel.format(
                    translate('Are you sure you want to delete saved search «%s»?'),
                    label
                );
                if (shared) {
                    message += '<br><br>';
                    message += translate('<b>Warning:</b> This search is shared with other users and possibly used in scheduled tasks. Make sure it is safe to delete this search in order to proceed.');
                }

                confirmDialog({
                    title: '<i class="fa fa-trash-alt fa-fw me-2"></i> ' + translate('Delete search'),
                    message: message,
                    buttons: {
                        confirm: {
                            label: '<i class="fa fa-trash-alt fa-fw me-2"></i> ' + translate('Delete'),
                            className: 'btn-danger'
                        },
                        cancel: {
                            label: '<i class="fas fa-times fa-fw me-2"></i> ' + translate('Cancel'),
                            className: 'btn-secondary'
                        }
                    },
                    callback: function(result) {
                        if (!result) {
                            return;
                        }

                        fetch(url, {
                            method: 'DELETE',
                        }).then(response => {
                            // Check for error response
                            if (!response.ok) {
                                return Promise.reject(response);
                            }

                            return response.json();
                        }).then(response => {
                            if (!response.result) {
                                notify.error(response.message ? response.message : translate('Failed to communicate with remote server'));
                                return;
                            }

                            notify.success(response.message);

                            updateMenu();

                            if (search && searchId == search.id) {
                                search = null;
                                updateInfoAndActions();
                            }
                        }).catch(async response => {
                            apiErrorCallback(response);
                        });
                    }
                });
            };
        });
    }


    function updateInfoAndActions() {
        if (search) {
            leaveButton.classList.remove('d-none');

            infoDiv.classList.remove('d-none');
            infoDiv.firstElementChild.innerHTML = search.label;
            infoDiv.lastElementChild.innerHTML = search.description ? search.description.replace(/\n/g, '<br>') : '';
        }
        else if (leaveButton) {
            leaveButton.classList.add('d-none');
            infoDiv.classList.add('d-none');
        }

        if (!saveButton) {
            return;
        }

        if (!search || search.user.id == userId) {
            saveButton.classList.remove('d-none');
        }
        else {
            saveButton.classList.add('d-none');
        }
    }


    function updateMenu() {
        fetch(menuDropdown.dataset.url, {
            method: 'GET',
        }).then(response => {
            // Check for error response
            if (!response.ok) {
                return Promise.reject(response);
            }

            return response.json();
        }).then(response => {
            if (!response.result) {
                let message = response.message ? response.message : translate('Failed to communicate with remote server');
                notify.error(message);
                return;
            }

            menuDropdown.innerHTML = response.data;
            initMenu();
        }).catch(async response => {
            apiErrorCallback(response);
        });
    }


    function init() {
        // Retrieve dom elements
        infoDiv = document.getElementById('searches-info');
        leaveButton = document.getElementById('searches-leave');
        menuDropdown = document.getElementById('searches');
        saveButton = document.getElementById('searches-save');
        saveForm = document.querySelector('#searches-save-modal form[name="save"]');
        saveModal = document.getElementById('searches-save-modal');

        if (infoDiv?.dataset.search) {
            search = JSON.parse(infoDiv.dataset.search);
            userId = infoDiv.dataset.userId;
        }

        updateInfoAndActions();
        initMenu();

        if (leaveButton) {
            leaveButton.onclick = function(evt) {
                evt.preventDefault();

                confirmDialog({
                    title: '<i class="fa fa-sign-out-alt fa-fw me-2"></i> ' + translate('Leave current search'),
                    message: translate('Are you sure you want to leave current saved search?'),
                    buttons: {
                        confirm: {
                            label: '<i class="fas fa-sign-out-alt fa-fw me-2"></i> ' + translate('Leave'),
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: '<i class="fas fa-times fa-fw me-2"></i> ' + translate('Cancel'),
                            className: 'btn-secondary'
                        }
                    },
                    callback: function(result) {
                        if (!result) {
                            return;
                        }

                        fetch(leaveButton.dataset.url, {
                            method: 'POST',
                        }).then(response => {
                            // Check for error response
                            if (!response.ok) {
                                return Promise.reject(response);
                            }

                            return response.json();
                        }).then(response => {
                            if (!response.result) {
                                let message = response.message ? response.message : translate('Failed to communicate with remote server');
                                notify.error(message);
                                return;
                            }

                            notify.success(response.message);

                            search = null;
                            updateInfoAndActions();
                        }).catch(async response => {
                            apiErrorCallback(response);
                        });
                    }
                });
            };
        }

        if (saveModal) {
            saveModal.addEventListener('show.bs.modal', _e => {
                clearFormErrors(saveForm);

                saveForm.querySelector('input[name="label"]').value = search ? search.label : '';
                saveForm.querySelector('textarea[name="description"]').value = search ? search.description : '';

                let checkbox = saveForm.querySelector('input[name="shared"]');
                if (checkbox) {
                    // only users with searches.advanced permission have shared option
                    checkbox.checked = search?.shared;
                }
            });

            saveForm.onsubmit = function(evt) {
                evt.preventDefault();

                let action = saveForm.getAttribute('action');
                let method;
                let target = saveForm.dataset.target;

                if (search) {
                    action = action.replace(target, search.id);
                    method = 'PUT';
                }
                else {
                    method = 'POST';
                }

                clearFormErrors(saveForm);

                let data = new FormData(saveForm);

                fetch(action, {
                    method: method,
                    body: data,
                }).then(response => {
                    // Check for error response
                    if (!response.ok) {
                        return Promise.reject(response);
                    }

                    return response.json();
                }).then(response => {
                    if (!response.result) {
                        let message = response.message ? response.message : translate('Failed to communicate with remote server');
                        notify.error(message);

                        if (response.errors) {
                            setFormErrors(saveForm, response.errors);
                        }
                        return;
                    }

                    notify.success(response.message);

                    search = response.search;

                    updateInfoAndActions();
                    updateMenu();

                    bootstrap.Modal.getInstance(saveModal).hide();
                }).catch(async response => {
                    apiErrorCallback(response);
                });
            };
        }
    }

    // Return public methods
    return {
        init: init,
    };
}();
/* exported searches */

searches.init();
