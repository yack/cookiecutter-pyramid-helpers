## This a custom version of AdminLTE 4.0.0-beta1

It has been compiled from sources.

Customizations:
- The `$font-size-base` variable has been modified to `0.8rem` instead of `1rem`.
  The variable is located in `src/scss/_bootstrap-variables.scss` file.
- A custom version of admin.js (named admin.custom.js) file is provided.
  It allows to memorize the sidebar state (collapsed, uncollapsed).
