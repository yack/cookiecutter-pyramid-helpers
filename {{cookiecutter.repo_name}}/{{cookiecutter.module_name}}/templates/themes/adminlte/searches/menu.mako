<%!
import sqlalchemy as sa

from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import Search
%>\
<%
select = sa.select(Search).where(
    sa.or_(
        Search.user == request.authenticated_user,
        Search.shared.is_(True),
    )
)

count = DBSession.execute(select.order_by(None).with_only_columns(sa.func.count(Search.id))).scalar()

select = select.order_by(
    sa.case((Search.user == request.authenticated_user, 0), else_=1),
    Search.target,
    Search.label,
)
%>\
<a class="nav-link" data-bs-toggle="dropdown" href="#">
    <i class="bi bi-search"></i>
    <span class="navbar-badge badge text-bg-primary">${count}</span>
</a>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-end" style="min-width: 280px;">
% if count:
    <div class="dropdown-header">
        <input id="searches-input-filter" type="text" class="form-control" placeholder="${translate('Filter searches')}" />
    </div>
    <div class="dropdown-divider"></div>
    <div class="dropdown-items overflow-auto" style="max-height: 200px;">
    % for search in DBSession.execute(select).scalars():
        <% config = get_config(search.target) %>\
        <div class="dropdown-item d-flex">
            <a class="dropdown-item flex-grow-1 p-1" role="menuitem" tabindex="-1" href="${request.route_path('searches.load', search=search.id)}" title="${search.description or ''}">
        % if config and config.get('icons'):
                <i class="fa fa-${config['icons'][1]} fa-fw me-1"></i>
        % endif
                ${search.label}
            </a>
            <div class="pt-1">
        % if search.batch:
                <i class="fa fa-fw fa-cogs fa-fw" title="${translate('Batch processing')}"></i>
        % else:
                <i class="fa fa-fw fa-cogs fa-fw text-secondary"></i>
        % endif
        % if search.shared:
                <i class="fa fa-fw fa-share-alt fa-fw" title="${translate('Shared')}"></i>
        % else:
                <i class="fa fa-fw fa-share-alt fa-fw text-secondary"></i>
        % endif
        % if search.user == request.authenticated_user:
                <a href="${request.route_path('api.searches.delete', search=search.id)}" title="${translate('Remove')}" class="searches-del text-danger" data-id="${search.id}" data-label="${search.label}" data-shared="${'true' if search.shared else 'false'}">
                    <i class="far fa-trash-alt fa-lg"></i>
                </a>
        % else:
                <i class="fa fa-fw fa-lock text-secondary"></i>
        % endif
            </div>
        </div><!-- /.dropdown-item -->
    % endfor
    </div><!-- /.dropdown-items -->
% else:
    <div class="dropdown-header">${translate('There is no saved search')}</div>
% endif
</div><!-- /.dropdown-menu -->
