<%!
import json

from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import Search
from {{ cookiecutter.module_name }}.plugins import get_plugins
from {{ cookiecutter.module_name }}.views.batches import render_select_message
%>

<%def name="render_batch_dropdown(batch)">
<%
selected_count = len(batch['selected_ids'])
matched_count = len(batch['matched_ids'])

if selected_count == 0:
    btn_class = 'btn-tool'
    ico_class = 'far fa-circle'
elif selected_count == 1:
    btn_class = 'btn-secondary'
    ico_class = 'far fa-check-circle'
elif selected_count == matched_count:
    btn_class = 'btn-danger'
    ico_class = 'fas fa-exclamation-triangle text-danger'
else:
    btn_class = 'btn-warning'
    ico_class = 'fas fa-exclamation-circle text-warning'
%>
<div class="btn-group btn-tool">
  <button type="button" class="btn ${btn_class} dropdown-toggle" style="margin: -1rem 0" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fa fa-cogs fa-fw me-1"></i>
    ${translate('Batch processing')}
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li class="dropdown-item select-count">
      <i class="${ico_class} fa-fw me-1"></i>
      <span class="fw-bold">${render_select_message(request, batch)}</span>
    </li>
    <li class="dropdown-divider"></li>
    <li>
      <a href="${request.route_path('batches.selection', batch=batch['id'])}" class="dropdown-item select-page">
        <i class="fa fa-list fa-fw me-1"></i>
        ${translate('Select/unselect all items from this page')}
      </a>
    </li>
    <li>
      <a href="${request.route_path('batches.selection', batch=batch['id'])}" class="dropdown-item select-all">
        <i class="fa fa-list fa-fw me-1"></i>
        ${translate('Select/unselect all matching items')}
      </a>
    </li>
    <li class="dropdown-divider"></li>
    <li>
      <a href="${request.route_path('batches.index', batch=batch['id'])}" class="dropdown-item select-batch${' disabled' if selected_count == 0 else ''}">
        <i class="fa fa-cogs fa-fw me-1"></i>
        ${translate('Execute...')}
      </a>
    </li>
    <li class="dropdown-divider"></li>
    <li>
      <a href="${request.route_path('batches.selection', batch=batch['id'])}" class="dropdown-item select-none">
        <i class="fa fa-broom fa-fw me-1"></i>
        ${translate('Clear selection')}
      </a>
    </li>
  </ul>
</div>
</%def>

<%def name="render_export_button(target)">
<%
pager = request.pagers[target]
plugins = [
    plugin
    for section, plugins in get_plugins(target).items()
    for plugin in plugins
    if section == 'Exports' and (not plugin.__permission__ or request.has_permission(plugin.__permission__))
]
%>
<div class="btn-group btn-tool">
    <a class="btn btn-tool dropdown-toggle${' disabled' if not pager.total else ''}" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="${translate('Export data to file')}">
        <i class="fa fa-download fa-fw me-1"></i>
        ${translate('Exports')}
    </a>
% if pager.total:
    <div class="dropdown-menu dropdown-menu-right">
    % for index, format_ in enumerate(['xlsx', 'csv']):
        % if index > 0:
        <div class="dropdown-divider" role="separator"></div>
        % endif
        <div class="dropdown-header">${translate('{0} format').format(format_.upper())}</div>
        % for plugin in plugins:
            <%
            params = pager.params.copy()
            params['format'] = format_
            %>\
            <a class="dropdown-item" href="${request.route_path('reports.plugins', target=target, plugin=plugin.__id__, _query=params)}">
                <i class="fa fa-${plugin.__icon__} fa-fw me-1"></i>
                ${translate(plugin.__name__)}
            </a>
        % endfor
    % endfor
    </div>
% endif
</div>
</%def>

<%def name="render_searches_buttons(target)">
<a id="searches-leave" role="button" class="btn btn-tool d-none me-3" data-url="${request.route_path('api.searches.leave', target=target)}">
    <i class="fas fa-sign-out-alt fa-fw me-1"></i> ${translate('Leave')}
</a>
% if request.has_permission('searches.create'):
<a id="searches-save" href="#searches-save-modal" role="button" class="btn btn-tool d-none" data-bs-toggle="modal">
    <i class="fa fa-save fa-fw me-1"></i> ${translate('Save')}
</a>
% endif
% if request.has_permission(f'{target}.export'):
${render_export_button(target)}
% endif
</%def>

<%def name="render_searches_info(target)">
    <%
    # Get current search
    search_id = request.session.get(f'[form] {target}.search::search_id')
    search = DBSession.get(Search, search_id) if search_id else None
    %>\
<div id="searches-info" class="d-none mb-3" data-search="${json.dumps(search.to_dict(context='search'), default=str) if search else ''}" data-user-id="${request.authenticated_user.id}">
  <div class="fw-bold"></div>
  <div class="text-secondary"></div>
</div>
</%def>

<%def name="render_searches_save_modal(target, batch=None)">
% if request.has_permission('searches.create'):
<div id="searches-save-modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="${request.route_path('api.searches.create', target=target)}" name="save" method="post" role="form" data-target="${target}">
    % if batch:
        <input type="hidden" name="batch" value="${batch['id']}" />
    % endif
        <div class="modal-header">
          <h5 class="modal-title"><i class="fas fa-save fa-fw me-1"></i> ${translate('Save search')}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="${translate('Close')}"></button>
        </div><!-- /.modal-header -->

        <div class="modal-body">
          <div class="mb-3">
            <div class="mb-3 form-required">
              <label class="form-label" for="search-label">${translate('Label')}</label>
              <input type="text" id="search-label" name="label" class="form-control" />
            </div>
          </div>

          <div class="mb-3">
            <label class="form-label" for="search-description">${translate('Description')}</label>
            <textarea id="search-description" name="description" class="form-control"></textarea>
          </div>
    % if request.has_permission('searches.advanced'):

          <div class="mb-3">
            <div class="pretty p-curve p-icon p-smooth">
              <input type="checkbox" id="search-shared" name="shared" />
              <div class="state p-primary-o">
                <i class="icon fa fa-check"></i>
                <label for="search-shared">${translate('Share this search with other users')}</label>
              </div>
            </div>
          </div>
    % endif
        </div><!-- /.modal-body -->

        <div class="modal-footer border-0">
          <button type="button" class="btn btn-secondary me-3" data-bs-dismiss="modal"><i class="fa fa-times fa-fw me-1"></i> ${translate('Cancel')}</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw me-1"></i> ${translate('Save')}</button>
        </div><!-- /.modal-div -->
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
% endif
</%def>
