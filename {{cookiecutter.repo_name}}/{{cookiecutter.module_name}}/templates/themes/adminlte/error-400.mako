<%inherit file="/site.mako"/>\
<div class="w-50 mt-5 mx-auto d-flex">
    <h2 class="display-1 text-warning me-5">400</h2>
    <div>
        <h3 class="fw-light mb-3">
            <i class="fa fa-exclamation-triangle text-warning"></i>
            ${translate('Oops! Bad request')}
            <br>
            <span class="fs-6">${translate('Detail: {0}').format(translate(request.exception.message or request.exception.explanation))}</span>
        </h3>
        <p>
            ${translate('Something went wrong with your request.<br>Meanwhile, you may <a href="{}">return to homepage</a>.').format(request.route_path('index')) | n}
        </p>
    </div>
</div>
