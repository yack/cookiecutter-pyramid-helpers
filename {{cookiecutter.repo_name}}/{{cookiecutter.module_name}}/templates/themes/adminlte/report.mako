<%inherit file="/site.mako"/>\
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-upload fa-fw me-1"></i> ${translate('A file has been imported')}</h3>
    </div><!-- /.card-header -->
    <div class="card-body">
% if links is None:
        ${translate('You may want to go to the <a href="{0}">search</a> page or proceed a <a href="{1}">new import</a>.').format(request.route_path(f'{target}.search'), request.current_route_path(_query={'dismiss': 1})) | n}
% else:
        ${translate('You may want to apply a batch process to imported objects, go to the <a href="{0}">search</a> page or proceed a <a href="{1}">new import</a>.').format(request.route_path(f'{target}.search'), request.current_route_path(_query={'dismiss':1})) | n}
% endif
    </div>
</div>

% if links:
<!-- Batches -->
<h4 class="me-0 mt-5">${translate('Batch processing')}</h4>
    % for key, (count, text) in links.items():
<a class="text-dark text-decoration-none" href="${request.current_route_path(_query={'batch': key})}">
    <div class="card mb-3 p-2">
        <div class="d-flex">
            <div class="bg-primary rounded-1 text-white">
                <i class="fa fa-cogs fa-fw fa-3x m-4"></i>
            </div>
            <div class="card-body">
                <div class="mb-2 fw-bold">${text.capitalize()}</div>
                <div class="text-secondary">${pluralize('Apply a batch process to {0} object', 'Apply a batch process to {0} objects', count).format(count)}</div>
            </div>
        </div>
    </div>
</a>
    % endfor
% elif links is not None:
<div class="alert alert-primary mt-5">
    <i class="fa fa-info-circle fa-fw fa-lg me-1"></i>
    ${translate('There is no batch process to launch, you may want to go to the <a class="alert-link" href="{0}">search</a> page').format(request.route_path(f'{target}.search')) | n}
</div>
% endif
