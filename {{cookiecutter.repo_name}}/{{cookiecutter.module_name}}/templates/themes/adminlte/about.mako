<%inherit file="/site.mako"/>\

<div class="callout callout-info">
    <i class="fa fa-info-circle fa-fw fa-lg me-1"></i>
    {{ cookiecutter.project_name }} - {{ cookiecutter.project_desc }}
</div>

<div class="card mt-5">
    <div class="card-header border-bottom">
        <h3 class="card-title">${translate('Instance')}</h3>
    </div>
    <div class="card-body">
        <dl>
            <dt>${translate('Instance')}</dt>
            <dd>${app_instance}</dd>

            <dt>${translate('Version')}</dt>
            <dd>${app_version}</dd>
% if app_instance_id:

            <dt>${translate('Instance Id')}</dt>
            <dd class="font-monospace">${app_instance_id}</dd>
% endif
        </dl>
    </div><!-- /.card-body -->
</div><!-- /.card -->

<div class="card mt-5">
    <div class="card-header border-bottom">
        <h3 class="card-title">${translate('License')}</h3>
    </div>
    <div class="card-body">
        <b>{{ cookiecutter.project_name }}</b> is a free software; you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.<br>
        <br>
        {{ cookiecutter.project_name }} is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>
        See the GNU Affero General Public License for more details.<br>
        <br>
        You should have received a copy of the GNU Affero General Public License along with this program. If not, see &lt;<a href="https://www.gnu.org/licenses">https://www.gnu.org/licenses</a>&gt;.
    </div><!-- /.card-body -->
</div><!-- /.card -->
