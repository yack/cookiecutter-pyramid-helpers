<%!
import datetime

year = datetime.date.today().year
%>\
<%namespace name="form" file="/form-tags.mako"/>\
<!DOCTYPE html>
<html lang="${request.locale_name}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>${app_name} | ${title}</title>
    <link rel="shortcut icon" href="${request.static_path('{{ cookiecutter.module_name }}:static/favicon.ico')}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Inter -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/font/inter-{{ cookiecutter.static_dependencies['inter']['version'] }}/web/inter.css')}" rel="stylesheet" />

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="${request.static_path('{{ cookiecutter.module_name }}:static/font/bootstrap-icons-{{ cookiecutter.static_dependencies['bootstrap-icons']['version'] }}/font/bootstrap-icons.min.css')}">

    <!-- AdminLTE -->
    <link rel="stylesheet" href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/AdminLTE-4.0.0-beta2/dist/css/adminlte.min.css')}">

    <!-- Custom -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/css/sign-in.css', _query={'version': app_version})}" rel="stylesheet" />
</head>
<body class="login-page bg-body-secondary">
% if app_mode == 'maintenance':
    <div class="container container-fluid">
        <div class="alert alert-success">
            <i class="fa fa-4x fa-fw fa-tools pull-left"> </i>
            <h4>${translate('Under maintenance')}</h4>
            <p>${translate('{0} is currently in maintenance mode, <b>access to write operations may be restricted</b>.<br>We expect to be back shortly. Thanks for your patience.').format(app_name) | n}</p>
        </div><!-- /.alert-box -->
    </div>
% endif

    <div class="login-box">
        <div class="login-logo">
            <a href="${request.route_path('index')}" title="${app_name}">
        % if app_instance:
                <span class="fw-bold">${app_name}</span><span class="fw-light">@${app_instance}</span>
        % else:
                <span class="fw-bold">${app_name}</span>
        % endif
            </a>
        </div>

        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">${title}</p>

                <%form:form name="signin" action="${request.route_path('auth.sign-in')}" method="post" role="form">
                    <%form:hidden name="redirect" />

                    <div class="input-group mb-3">
                        <%form:text name="username" id="username" class_="form-control" placeholder="${translate('Username')}" />
                        <div class="input-group-text"><span class="bi bi-person"></span></div>
                    </div>
                    <div class="input-group mb-3">
                        <%form:password name="password" id="password" class_="form-control" placeholder="${translate('Password')}" />
                        <div class="input-group-text"><span class="bi bi-lock-fill"></span></div>
                    </div>

                    <button class="mt-3 w-100 btn btn-lg btn-primary" type="submit">${translate('Sign in')}</button>
                    <p class="mt-5 mb-3 text-center text-muted">&copy; {{ cookiecutter.copyright_name }} ${year}</p>
                </%form:form>
            </div>
        </div>
    </div>
</body>
</html>
