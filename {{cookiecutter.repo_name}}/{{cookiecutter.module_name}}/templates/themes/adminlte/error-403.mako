<%inherit file="/site.mako"/>\
<div class="w-50 mt-5 mx-auto d-flex">
    <h2 class="display-1 text-warning me-5">403</h2>
    <div>
        <h3 class="fw-light mb-3">
            <i class="fa fa-exclamation-triangle text-warning"></i>
            ${translate('Oops! Access denied')}
            <br>
            <span class="fs-6">${translate('Detail: {0}').format(translate(request.exception.message or request.exception.explanation))}</span>
        </h3>
        <p>
% if authenticated_user is not None:
            ${translate('You don\'t have enough permission to access the page you were looking for.<br>Meanwhile, you may <a href="{}">return to dashboard page</a>.').format(request.route_path('index')) | n}
% else:
            ${translate('You don\'t have enough permission to access the page you were looking for.<br>Meanwhile, you may <a href="{}">return to sign in page</a>.').format(request.route_path('auth.sign-in')) | n}
% endif
        </p>
    </div>
</div>
