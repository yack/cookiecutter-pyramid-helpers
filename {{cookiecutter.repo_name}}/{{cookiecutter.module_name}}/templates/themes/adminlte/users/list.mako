<%namespace name="paginate" file="/paginate.mako"/>
<%namespace file="/searches/render.mako" import="render_batch_dropdown" />\
<% pager = request.pagers['users'] %>
% if pager.total:
<div class="card mt-5">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-list fa-fw me-1"></i>
    % if pager.pages > 1:
            ${translate('from {0} to {1} on {2} users').format(pager.first_item, pager.last_item, pager.total)}
    % else:
            ${pluralize('{0} user', '{0} users', pager.total).format(pager.total)}
    % endif
        </h3>

    % if show_batch:
        <div class="card-tools">
${render_batch_dropdown(batch)}
        </div>
    % endif
    </div><!-- /.card-header -->

    <div class="card-body p-0 table-responsive">
        <table class="table table-sm table-hover table-striped">
            <thead>
                <tr>
    % if show_batch:
                    <th class="select-col">&nbsp;</th>
    % endif
                    <th>
                        <a class="partial-link" href="${pager.link(sort='username', order='toggle')}" title="${translate('Ordering using this column')}">
                            <span class="${pager.header_class('username')}"></span>
                            ${translate('Username')}
                        </a>
                    </th>
                    <th>
                        <a class="partial-link" href="${pager.link(sort='fullname', order='toggle')}" title="${translate('Ordering using this column')}">
                            <span class="${pager.header_class('fullname')}"></span>
                            ${translate('Name')}
                        </a>
                    </th>
                    <th>
                        <a class="partial-link" href="${pager.link(sort='email', order='toggle')}" title="${translate('Ordering using this column')}">
                            <span class="${pager.header_class('email')}"></span>
                            ${translate('Email')}
                        </a>
                    </th>
                    <th>${translate('Profiles')}</th>
                    <th>
                        <a class="partial-link" href="${pager.link(sort='status', order='toggle')}" title="${translate('Ordering using this column')}">
                            <span class="${pager.header_class('status')}"></span>
                            ${translate('Status')}
                        </a>
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    % for user in pager:
                <tr class="align-middle">
        % if show_batch:
                    <td class="select-col">
            <% checked = ' checked="checked"' if user.id in batch['selected_ids'] else '' %>
                        <div class="pretty p-curve p-icon p-smooth me-0">
                            <input type="checkbox" class="select-one" id="select-one-${user.id}" name="select_one" value="${user.id}" title="${translate('Select this user')}" data-url="${request.route_path('batches.selection', batch=batch['id'])}"${checked}>
                            <div class="state p-primary-o">
                                <i class="icon fa fa-check"></i>
                                <label for="select-one-${user.id}" class="sr-only">#${f'{user.id:06d}'}</label>
                            </div>
                        </div>
                    </td>
        % endif
                    <td>
        % if request.has_permission('users.visual'):
                        <a href="${request.route_path('users.visual', user=user.id)}">
                            <b><tt>${user.username}</tt></b>
                        </a>
        % else:
                        <b><tt>${user.username}</tt></b>
        % endif
                    </td>
                    <td>${user.fullname}</td>
                    <td>
        % if user.email:
                        <a href="mailto:${user.email}">${user.email}</a>
        % endif
                    </td>
                    <td>
        % for profile in user.profiles:
                        <span class="label label-warning label-block">${translate(user.PROFILES[profile])}</span>
        % endfor
                    </td>
                    <td><span class="badge text-bg-${user.STATUSES_COLORS[user.status]}"><i class="fa fa-${user.STATUSES_ICONS[user.status]} fa-fw me-1"></i> ${translate(user.STATUSES[user.status])}</span></td>
                    <td class="text-end">
        % if request.has_permission('users.delete') and user != request.authenticated_user:
                        ## Prevent from committing suicide
                        <a href="${request.route_path('api.users.delete', user=user.id)}" class="api-link btn btn-sm btn-outline-danger" data-method="delete" data-confirm-message="${translate('Do you really want to delete user «{0}» ?').format(user.username)}" data-reload="true" title="${translate('Delete')}">
                            <i class="fa fa-trash-alt"></i>
                        </a>
        % endif
        % if request.has_permission('users.modify'):
                        <a href="${request.route_path('users.modify', user=user.id)}" class="btn btn-sm btn-outline-primary ms-2" title="${translate('Edit')}">
                            <i class="fa fa-pen"></i>
                        </a>
        % endif
                    </td>
                </tr>
    % endfor
            </tbody>
        </table>
    </div><!-- /.card-body -->

    <div class="card-footer d-flex flex-row-reverse p-3">
${paginate.render_pages(pager, extra_class='mb-0 ms-4')}
${paginate.render_limit(pager, extra_class='mb-0')}
    </div><!-- /.card-footer -->
</div><!-- /.card -->
% else:
<div class="alert alert-primary mt-5">
    <i class="fa fa-info-circle fa-fw fa-lg me-1"></i>
    ${translate('No user match search criteria')}
</div>
% endif
