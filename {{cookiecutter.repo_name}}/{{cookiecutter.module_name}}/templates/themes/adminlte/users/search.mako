<%namespace file="/form-tags.mako" name="form"/>\
<%namespace file="/searches/render.mako" name="searches" />\
<%inherit file="/site.mako"/>\
<%def name="foot()">
${parent.foot()}\
<!-- Custom scripts -->
<script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.items-selection.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>\
<%def name="head()">
${parent.head()}\
<!-- Default form values -->
<script>
formValues.search = {
    'profiles': [],
    'term': '',
    'statuses': []
};
</script>
</%def>\

<!-- Search criteria -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><i class="fas fa-search fa-fw me-1"></i> ${translate('Search criteria')}</h3>
        <div class="card-tools">
${searches.render_searches_buttons('users')}
        </div>
    </div>

  <%form:form name="search" method="get" role="form">
    <div class="card-body">
${searches.render_searches_info('users')}
      <div class="row">
        <div class="col-md-4">
            <label class="form-label" for="term">${translate('Term')}</label>
            <%form:text name="term" id="term" class_="form-control" />
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label" for="profiles">${translate('Profiles')}</label>
            <%form:select class_="form-select" name="profiles" id="profiles" multiple="multiple">
              <%form:option value=""></%form:option>
% for value, label in profiles.items():
              <%form:option value="${value}">${translate(label)}</%form:option>
% endfor
            </%form:select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="mb-3">
            <label class="form-label" for="statuses">${translate('Statuses')}</label>
            <%form:select class_="form-select" name="statuses" id="statuses" multiple="multiple">
              <%form:option value=""></%form:option>
% for value, label in statuses.items():
              <%form:option value="${value}">${translate(label)}</%form:option>
% endfor
            </%form:select>
          </div>
        </div>
      </div>
    </div><!-- /.card-body -->

    <div class="card-footer text-end p-3">
      <a class="btn btn-secondary form-reset me-3"><i class="fa fa-broom fa-fw me-1"></i> ${translate('Reset')}</a>
      <button class="btn btn-primary" type="submit" name="search" value="search"><i class="fa fa-search fa-fw me-1"></i> ${translate('Search')}</button>
    </div><!-- /.card-footer -->
  </%form:form>
</div><!-- /.card -->

<!-- Search result -->
<div id="users-list" class="items-selection partial-block position-relative" data-partial-key="users.partial">
<%include file="list.mako" />
</div><!-- /.partial-block -->

${searches.render_searches_save_modal('users')}
