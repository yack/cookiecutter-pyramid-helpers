<%!
import sqlalchemy as sa

from {{ cookiecutter.module_name }}.funcs.users import get_user
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User
%>\
<%
def get_last_visited():
    current_user = get_user(request)
    if current_user:
        request.session['last_user'] = current_user.id
        return (current_user, True)

    if 'last_user' not in request.session:
        return (None, False)

    last_user_id = request.session['last_user']

    last_visited = DBSession.get(User, last_user_id)
    if last_visited is None:
        # Oops, user has been deleted
        del request.session['last_user']
        return (None, False)

    return (last_visited, False)
%>\
## Search
% if request.has_permission('users.search'):
<li class="nav-item">
    <a class="nav-link px-3${' active' if route_name == 'users.search' else ''}" href="${request.route_path('users.search')}">
        <i class="fas fa-users fa-lg fa-fw me-1"></i>
        ${translate('Users')}
    </a>
</li>
% endif
## Batch
% if route_name.startswith('batches.'):
<li class="nav-item">
    <a class="nav-link px-3 active" href="${request.route_path('batches.index', batch=batch['id'])}">
        <i class="fas fa-cogs fa-lg fa-fw me-1"></i>
        ${translate('Batch processing')}
    </a>
</li>
% endif
## Last visited
% if request.has_permission('users.visual'):
    <% last_visited, active = get_last_visited() %>\
    % if last_visited:
<li class="nav-item">
    <a class="nav-link px-3${' active' if active else ''}" href="${request.route_path('users.visual', user=last_visited.id)}">
        <i class="fas fa-user fa-lg fa-fw me-1"></i>
        ${last_visited.fullname}
    </a>
</li>
    % endif
% endif
## New user
% if request.has_permission('users.create'):
<li class="nav-item">
    <a class="nav-link px-3${' active' if route_name == 'users.create' else ''}" href="${request.route_path('users.create')}">
        <i class="far fa-file fa-lg fa-fw me-1"></i>
        ${translate('New user')}
    </a>
</li>
% endif
## Select
% if request.has_permission('users.select'):
<li class="nav-item">
    <a class="nav-link px-3${' active' if route_name == 'select' else ''}" href="${request.route_path('select', type='users')}">
        <i class="fas fa-cogs fa-lg fa-fw me-1"></i>
        ${translate('Select')}
    </a>
</li>
% endif
## Import
% if request.has_permission('users.import'):
<li class="nav-item">
    <a class="nav-link px-3${' active' if route_name == 'import' else ''}" href="${request.route_path('import', type='users')}">
        <i class="fas fa-upload fa-lg fa-fw me-1"></i>
        ${translate('Import')}
    </a>
</li>
% endif
