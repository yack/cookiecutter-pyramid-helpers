<%!
import zoneinfo

from {{ cookiecutter.module_name }}.funcs.users import can_set_password
%>
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="head()">
${parent.head()}
</%def>
<%form:form name="user" method="post" role="form">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title"><i class="fa fa-user fa-fw me-1"></i> ${subtitle}</h3>
    </div>

    <div class="card-body">

      <div class="row">
        <div class="mb-3 col-12 col-md-4 form-required">
          <label class="form-label" for="username">${translate('Username')}</label>
          <%form:text id="username" name="username" class_="form-control" />
        </div>

        <div class="mb-3 col-12 col-md-8 form-required">
          <label class="form-label" for="profiles">${translate('Profiles')}</label>
          <%form:select class_="form-select" id="profiles" name="profiles" multiple="multiple">
% for value, label in profiles.items():
            <%form:option value="${value}">${translate(label)}</%form:option>
% endfor
          </%form:select>
        </div>
      </div>

      <div class="row">
        <div class="mb-3 col-12 col-md-4">
          <label class="form-label" for="firstname">${translate('Firstname')}</label>
          <%form:text id="firstname" name="firstname" class_="form-control" />
        </div>

        <div class="mb-3 col-12 col-md-4">
          <label class="form-label" for="lastname">${translate('Lastname')}</label>
          <%form:text id="lastname" name="lastname" class_="form-control" />
        </div>

        <div class="mb-3 col-12 col-md-4">
          <label class="form-label" for="lastname">${translate('Email')}</label>
          <%form:text id="email" name="email" class_="form-control" />
        </div>
      </div>

      <div class="row">
% if can_set_password(request):
        <div class="mb-3 col-12 col-md-4 form-required">
          <label class="form-label" for="password">${translate('Password')}</label>
          ## fake fields are a workaround for firefox/chrome/opera autofill getting the wrong fields
          <input hidden type="text" name="fake_username_remembered" />
          <input hidden type="password" name="fake_password_remembered" />
          <%form:password id="password" name="password" class_="form-control" autocomplete="new-password" />
    % if user:
          <div class="form-text"><i class="fa fa-info-circle"></i> ${translate('Leave empty to keep password unchanged.')}</div>
    % endif
        </div>

        <div class="mb-3 col-12 col-md-4 form-required">
          <label class="form-label" for="password_confirm">${translate('Password confirmation')}</label>
          <%form:password id="password_confirm" name="password_confirm" class_="form-control" autocomplete="new-password" />
    % if user:
          <div class="form-text"><i class="fa fa-info-circle"></i> ${translate('Leave empty to keep password unchanged.')}</div>
    % endif
        </div>
% endif
        <div class="mb-3 col-12 col-md-4 form-required">
          <label class="form-label" for="status">${translate('Status')}</label>
          <%form:select class_="form-select" id="status" name="status" placeholder="--">
            <%form:option value=""></%form:option>
% for value, name in statuses.items():
            <%form:option value="${value}">${translate(name)}</%form:option>
% endfor
          </%form:select>
        </div><!-- /.col -->
      </div>

      <div class="row">
        <div class="mb-3 col-12 col-md-4">
          <label class="form-label" for="timezone">${translate('Time zone')}</label>
          <%form:select class_="form-select" id="timezone" name="timezone" placeholder="--">
            <%form:option value=""></%form:option>
% for value in sorted(zoneinfo.available_timezones()):
            <%form:option value="${value}">${translate(value.replace('_', ' '))}</%form:option>
% endfor
          </%form:select>
        </div>
      </div>

      <div class="mb-3">
        <label class="form-label" for="description">${translate('Description')}</label>
        <%form:textarea id="description" name="description" class_="form-control" rows="4" />
      </div>
    </div><!-- /.card-body -->

    <div class="card-footer text-end p-3">
      <a href="${cancel_link}" class="btn btn-secondary me-3"><i class="fa fa-times fa-fw me-1"></i> ${translate('Cancel')}</a>
      <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw me-1"></i> ${translate('Save')}</button>
    </div><!-- /.card-footer -->
  </div><!-- /.card -->
</%form:form>
