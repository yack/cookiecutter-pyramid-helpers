<%inherit file="/site.mako" />\
<%
colors = {'active': 'success', 'disabled': 'secondary'}
icons = {'active': 'user-check', 'disabled': 'user-slash'}
%>
<div class="card">
  <div class="card-header">
    <h3 class="card-title"><i class="fa fa-user fa-fw me-1"></i> ${subtitle}</h3>
    <div class="card-tools">
% if request.has_permission('users.modify') or request.has_permission('users.delete'):
      <div class="dropdown">
        <button class="btn btn-tool dropown-toggle me-2" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-cog fa-fw"></i>
            <span class="sr-only">${translate('Actions')}</span>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
    % if request.has_permission('users.modify'):
          <a class="dropdown-item" href="${request.route_path('users.modify', user=user.id)}" title="${translate('Modify user «{0}»').format(user.fullname)}">
            <i class="fa fa-pen fa-fw me-1"></i> ${translate('Modify')}
          </a>
          <div class="dropdown-divider"></div>
        % for status in user.STATUS_FRAMEWORK[user.status]:
          <a class="dropdown-item api-link" href="${request.route_path('api.users.status', user=user.id)}" data-method="put" data-reload="true" data-status="${status}" data-confirm-message="${translate('Do you really want to change status from «{0}» to «{1}» ?').format(translate(user.STATUSES[user.status]), translate(user.STATUSES[status]))}" title="${translate('Set status to «{0}»').format(translate(user.STATUSES[status]))}">
            <i class="fa fa-${icons[status]} fa-fw me-1"></i> ${translate(user.STATUSES[status])}
          </a>
        % endfor
    % endif
    ## Prevent from committing suicide
    % if request.has_permission('users.delete') and user != request.authenticated_user:
        <div class="dropdown-divider"></div>
        <a class="dropdown-item api-link" href="${request.route_path('api.users.delete', user=user.id)}" data-method="delete" data-location="${request.route_path('users.search')}" data-confirm-message="${translate('Do you really want to delete user «{0}» ?').format(user.username)}" title="${translate('Delete user «{0}»').format(user.fullname)}">
            <i class="fa fa-trash-alt fa-fw me-1"></i> ${translate('Delete')}
        </a>
    % endif
        </div><!-- /.dropdown-menu -->
      </div><!-- /.dropdown -->
% endif
    </div><!-- /.card-tools -->
  </div><!-- /.card-header -->

  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <dl class="row">
          <dt class="col-xl-4 text-xl-end">${translate('Username')}</dt>
          <dd class="col-xl-8">${user.username}</dd>

          <dt class="col-xl-4 text-xl-end">${translate('Status')}</dt>
          <dd class="col-xl-8"><span class="badge bg-${colors[user.status]}">${translate(user.STATUSES[user.status])}</span></dd>

          <dt class="col-xl-4 text-xl-end">${translate('Profiles')}</dt>
          <dd class="col-xl-8">
% for profile in user.profiles:
            <span class="badge bg-primary">${translate(user.PROFILES[profile])}</span>
% endfor
          </dd>

          <dt class="col-xl-4 text-xl-end">${translate('Email')}</dt>
          <dd class="col-xl-8">
% if user.email:
            <a href="mailto:${user.email}">${user.email}</a>
% else:
            -
% endif
          </dd>

          <dt class="col-xl-4 text-xl-end">${translate('Time zone')}</dt>
          <dd class="col-xl-8">${translate(user.timezone) if user.timezone else '-'}</dd>

          <dt class="col-xl-4 text-xl-end">${translate('Description')}</dt>
          <dd class="col-xl-8">${user.description.replace('\n', '<br>') if user.description else '-' | n}</dd>
        </dl>
      </div><!-- /.col -->
    </div><!--- /.row -->
  </div><!-- /.card-body -->
</div><!-- /.card -->
