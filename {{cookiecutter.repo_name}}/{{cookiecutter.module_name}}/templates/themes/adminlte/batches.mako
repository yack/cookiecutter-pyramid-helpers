<%!
import json
%>\
<%namespace file="/searches/render.mako" import="render_searches_save_modal"/>\
<%inherit file="/site.mako"/>\
<!-- Batch process -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-cogs fa-fw me-1"></i> ${translate('Batch processing')}</h3>
        <div class="card-tools">
            <a id="searches-leave" href="javascript:void(0);" role="button" class="btn btn-tool hidden" data-url="${request.route_path('api.searches.leave', target=batch['id'])}">
                <i class="fas fa-sign-out-alt fa-fw me-1"></i> ${translate('Leave')}
            </a>
            <a id="searches-save" href="#searches-save-modal" role="button" class="btn btn-tool ms-3 hidden" data-bs-toggle="modal">
                <i class="fa fa-save fa-fw me-1"></i> ${translate('Save')}
            </a>
            <a class="btn btn-tool ms-3" href="${cancel_link}"><i class="fa fa-undo fa-fw me-1"></i> ${translate('Back')}</a>
        </div>
    </div>

    <div class="card-body">
        <div id="searches-info" class="mb-3 hidden" data-search="${json.dumps(search.to_dict(context='search'), default=str) if search else ''}" data-user-id="${request.authenticated_user.id}">
            <div class="fw-bold"></div>
            <div class="text-secondary"></div>
        </div>
        <p>${message | n}</p>
    </div>
</div>

% for section in batches:
    % if batches[section]:
<h4 class="me-0 mt-4">${section}</h4>
        % for data in batches[section]:
<a class="text-dark" href="${data['url']}" title="${translate('Continue with batch «{0}»').format(data['name'])}">
    <div class="card mb-3 p-2">
        <div class="d-flex">
            <div class="bg-primary rounded-1 text-white">
                <i class="fa fa-${data['icon']} fa-fw fa-3x m-4"></i>
            </div>
            <div class="card-body">
                <div class="mb-2 fw-bold">${data['name']}</div>
            % if data['desc']:
                <div class="text-secondary">${data['desc']}</div>
            % endif
            </div>
        </div>
    </div>
</a>
        % endfor
    % endif
% endfor

<div id="${target}-list" class="partial-block search-result mt-5" data-partial-key="${target}.partial">
<%include file="${partial_template}" />
</div><!-- /.partial-block -->

${render_searches_save_modal(batch['target'], batch)}
