<%!
import datetime
import json

year = datetime.date.today().year
%>\
<%def name="foot()">
</%def>\
<%def name="head()">
</%def>\
<%def name="tpls()">
</%def>\
<!DOCTYPE html>
<html lang="${request.locale_name}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>${app_name} | ${title}${' | {0}'.format(subtitle) if subtitle else ''}</title>

    <meta content='width=device-width, initial-scale=1' name='viewport'>

    <link rel="shortcut icon" href="${request.static_path('{{ cookiecutter.module_name }}:static/favicon.ico')}">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="${request.static_path('{{ cookiecutter.module_name }}:static/font/bootstrap-icons-{{ cookiecutter.static_dependencies['bootstrap-icons']['version'] }}/font/bootstrap-icons.min.css')}">

    <!-- Font Awesome -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/font/fontawesome-free-{{ cookiecutter.static_dependencies['fontawesome']['version'] }}-web/css/all.min.css')}" rel="stylesheet" />

    <!-- Inter -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/font/inter-{{ cookiecutter.static_dependencies['inter']['version'] }}/web/inter.css')}" rel="stylesheet" />

    <!-- AdminLTE -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/AdminLTE-4.0.0-beta2/dist/css/adminlte.min.css')}" rel="stylesheet" />

    <!-- OverlayScrollbars -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/OverlayScrollbars-{{ cookiecutter.static_dependencies['overlay-scrollbars']['version'] }}/overlayscrollbars.min.css')}" rel="stylesheet" />

    <!-- Pretty checkbox -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/pretty-checkbox-{{ cookiecutter.static_dependencies['pretty-checkbox-css']['version'] }}/dist/pretty-checkbox.min.css')}" rel="stylesheet" />

    <!-- Tom Select -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/tom-select-{{ cookiecutter.static_dependencies['tom-select']['version'] }}/tom-select.min.css')}" rel="stylesheet" />
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/lib/tom-select-{{ cookiecutter.static_dependencies['tom-select']['version'] }}/tom-select.bootstrap5.min.css')}" rel="stylesheet" />

    <!-- Custom styles -->
    <link href="${request.static_path('{{ cookiecutter.module_name }}:static/css/{{ cookiecutter.repo_name }}.css', _query=dict(version=app_version))}" rel="stylesheet" />

    <!-- Custom scripts -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.global.js', _query=dict(version=app_version))}"></script>

    <script>
language = "${localizer.locale_name}";
messages = ${json.dumps(request.session.pop_flash()) | n};
    </script>

${self.head()}
</head>

<body class="layout-fixed sidebar-expand-lg bg-body-tertiary sidebar-${request.session['gui_settings'].get('sidebar_state', 'open')}" data-sidebar-state="${request.session['gui_settings'].get('sidebar_state', 'open')}">
    <div class="app-wrapper">
        <!-- Sidebar -->
        <aside class="app-sidebar bg-body-secondary shadow" data-bs-theme="dark">
            <!-- Brand link/logo -->
            <div class="sidebar-brand">
                <a href="${request.route_path('about')}" class="brand-link">
                    <span class="brand-text fs-2 m-0">
                        <span class="text-info fw-light fst-italic">${app_name}</span>
                    </span>
                </a>
            </div>

            <div class="sidebar-wrapper">
                <nav>
                    <ul class="nav sidebar-menu flex-column" data-lte-toggle="treeview" role="menu" data-accordion="false">
## Administration
                        <li class="nav-header text-uppercase fw-bold">${translate('Administration')}</li>

% if request.has_permission('users.search'):
                        <li class="nav-item">
                            <a href="${request.route_path('users.search')}" class="nav-link ${'active' if active_section == 'users' else ''}">
                                <i class="nav-icon fa fa-users"></i>
                                <p>${translate('Users')}</p>
                            </a>
                        </li>
% endif

## Development
% if request.has_permission('api-doc'):
                        <li class="nav-header text-uppercase fw-bold">${translate('Development')}</li>
                        <li class="nav-item">
                            <a href="${request.route_path('api-doc')}" class="nav-link ${'active' if active_section == 'api-doc' else ''}">
                                <i class="nav-icon fa fa-wrench"></i>
                                <p>${translate('API-doc')}</p>
                            </a>
                        </li>
% endif
                    </ul>
                </nav>
            </div>
        </aside>

        <div class="app-main-wrapper">
            <nav class="app-header navbar sticky-top navbar-expand bg-body">
                <div class="container-fluid">
                    <!-- Hambuger button (toggle sidebar) -->
                    <ul class="navbar-nav me-2">
                        <li class="nav-item">
                            <a class="nav-link" id="sidebar-toggler" data-url="${request.route_path('api.gui')}" data-lte-toggle="sidebar" href="#" role="button">
                                <i class="fa fa-bars fa-lg"></i>
                            </a>
                        </li>
                    </ul>

                    <!-- Current section menu -->
                    <ul class="navbar-nav nav-ribbon m-0">
% if active_section in ('users',):
<%include file="/${active_section}/menu.mako" />
% endif
                    </ul>

                    <ul class="navbar-nav ms-auto">
% if authenticated_user is not None:
                        <!-- Saved searches -->
                        <li class="nav-item dropdown" id="searches" data-url="${request.route_path('api.searches.menu')}">
<%include file="/searches/menu.mako" />
                        </li>
% endif

                        <!-- Fullscreen/unfullscreen button -->
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-lte-toggle="fullscreen">
                                <i data-lte-icon="maximize" class="bi bi-arrows-fullscreen"></i>
                                <i data-lte-icon="minimize" class="bi bi-fullscreen-exit" style="display: none;"></i>
                            </a>
                        </li>

                        <!-- Dark/Light theme switcher -->
                        <li class="nav-item dropdown">
                            <button class="btn btn-link nav-link py-2 px-0 px-lg-2 dropdown-toggle d-flex align-items-center" id="bd-theme" type="button" aria-expanded="false" data-bs-toggle="dropdown" data-bs-display="static">
                                <span class="theme-icon-active">
                                    <i></i>
                                </span>
                                <span class="ms-2 d-none" id="bd-theme-text">${translate('Toggle theme')}</span>
                            </button>

                            <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="bd-theme-text" style="--bs-dropdown-min-width: 8rem;">
                                <li>
                                    <button type="button" class="dropdown-item d-flex align-items-center active" data-bs-theme-value="light" aria-pressed="false">
                                        <i class="bi bi-sun-fill me-2"></i> ${translate('Light')}
                                        <i class="bi bi-check-lg ms-auto d-none"></i>
                                    </button>
                                </li>
                              <li>
                                  <button type="button" class="dropdown-item d-flex align-items-center" data-bs-theme-value="dark" aria-pressed="false">
                                      <i class="bi bi-moon-stars-fill me-2"></i> ${translate('Dark')}
                                      <i class="bi bi-check-lg ms-auto d-none"></i>
                                  </button>
                              </li>
                              <li>
                                  <button type="button" class="dropdown-item d-flex align-items-center" data-bs-theme-value="auto" aria-pressed="true">
                                      <i class="bi bi-circle-half me-2"></i> ${translate('Auto')}
                                      <i class="bi bi-check-lg ms-auto d-none"></i>
                                  </button>
                              </li>
                            </ul>
                        </li>

% if active_section in ('users', ):
                        <!-- Contextual search -->
                        <form action="${request.route_path(f'{active_section}.search')}" class="col-auto mb-0 me-3" method="get" role="search">
                            <input type="hidden" name="search" value="search">
                            <input type="search" name="term" class="form-control form-control-dark" placeholder="${translate('Search...')}" aria-label="Search">
                        </form>
% endif

% if authenticated_user is not None:
                        <!-- User menu -->
                        <li class="nav-item dropdown user-menu">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                                <i class="fa fa-user-circle fa-2x" style="line-height: 1.3rem"></i>
                                <span class="d-none d-md-inline" style="margin-left: 0.4rem; vertical-align: top">${authenticated_user.fullname}</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-xl dropdown-menu-end">
                                <li class="user-header text-bg-primary">
                                    <i class="fa fa-user-circle fa-5x"></i>
                                    <p class="mb-0">${authenticated_user.fullname}
    % if authenticated_user.profiles:
                                        <br>
                                        <small>${', '.join(translate(authenticated_user.PROFILES.get(profile, profile)) for profile in authenticated_user.profiles)}</small>
    % endif
                                    </p>
                                </li>

                                <li class="user-body d-none"></li>

                                <li class="user-footer">
                                    <a href="${request.route_path('profile')}" class="btn btn-secondary">
                                        <i class="fa fa-user fa-fw me-1"></i> ${translate('Profile')}
                                    </a>
                                    <a href="${request.route_path('auth.sign-out')}" class="btn btn-secondary float-end">
                                        <i class="fa fa-sign-out fa-fw me-1"></i> ${translate('Sign out')}
                                    </a>
                                </li>
                            </ul>
                        </li>
% endif
                    </ul>
                </div>
            </nav>

            <main class="app-main">
                <!-- Page title - subtitle -->
                <div class="app-content-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                                <h3 class="mb-0">${title}${' <span class="fs-5 text-secondary">{0}</span>'.format(subtitle) if subtitle else '' | n}</h3>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-end">
                                    <li class="breadcrumb-item">
                                        <a href="${request.route_path('index')}">${translate('Home')}</a>
                                    </li>
% for name, url in breadcrumb:
                                    <li class="breadcrumb-item ${'active' if url is None else '' | n}">
    % if url is not None:
                                        <a href="${url}">${name}</a>
    % else:
                                        ${name}
    % endif
                                    </li>
% endfor
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Page content -->
                <div class="app-content">
                    <div class="container-fluid">
${self.body()}\
                    </div>
                </div>
            </main><!-- .app-main -->

            <footer class="app-footer">
                <div class="float-end d-none d-sm-inline">
                    <b>${translate('Version')}</b> ${app_version}
                </div>
                <strong>Copyright &copy; ${year}, <a href="{{ cookiecutter.project_url }}">{{ cookiecutter.copyright_name }}</a>.</strong> All rights reserved.
            </footer>
        </div><!-- .app-main-wrapper -->
    </div><!-- .app-wrapper -->

    <div id="toast-container" class="toast-container position-absolute bottom-0 end-0 m-5"></div>

    <!-- Handlebar templates -->
    <script id="tpl-confirm-dialog" type="text/x-handlebars-template">
    <div class="modal {% raw %}{{modalClass}}{% endraw %}" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title h5">{% raw %}{{{title}}}{% endraw %}</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true"></button>
                </div>
                <div class="modal-body">
                    {% raw %}{{{message}}}{% endraw %}
                </div>
                <div class="modal-footer text-end">
    {% raw %}{{#if buttons.cancel }}{% endraw %}
                    <button type="button" class="btn {% raw %}{{buttons.cancel.className}}{% endraw %} me-2" data-bs-dismiss="modal" name="cancel">{% raw %}{{{buttons.cancel.label}}}{% endraw %}</button>
    {% raw %}{{/if}}{% endraw %}
                    <button type="button" class="btn {% raw %}{{buttons.confirm.className}}{% endraw %}" name="confirm">{% raw %}{{{buttons.confirm.label}}}{% endraw %}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    </script>

    <script id="tpl-form-error" type="text/x-handlebars-template">
    <div class="alert alert-danger py-1 mt-2 mb-0 form-error" role="alert">
        {% raw %}{{{message}}}{% endraw %}
    </div>
    </script>

    <script id="tpl-loading" type="text/x-handlebars-template">
    <div class="position-absolute opacity-50 top-0 start-0 w-100 h-100 bg-body" style="z-index: 1100;">
        <div class="position-absolute top-50 start-50 translate-middle">
            <i class="fa-solid fa-gear fa-spin fa-5x"></i>
        </div>
    </div>
    </script>

    <script id="tpl-toast" type="text/x-handlebars-template">
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="{% raw %}{{delay}}{% endraw %}">
        <div class="toast-header">
            <i class="fa fa-lg fa-fw me-2 {% raw %}{{icon}}{% endraw %} {% raw %}{{color}}{% endraw %}">&nbsp;</i>
            <strong class="me-auto">{% raw %}{{title}}{% endraw %}</strong>
            <small class="text-muted">{% raw %}{{time}}{% endraw %}<i class="fa-regular fa-clock fa-fw ms-1"></i></small>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="${translate('Close')}"></button>
        </div>
        <div class="toast-body">{% raw %}{{{message}}}{% endraw %}</div>
    </div>
    </script>
    ${self.tpls()}\

    <!-- Babel -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/babel-{{ cookiecutter.static_dependencies['babel']['version'] }}.js')}"></script>

    <!-- Handlebars -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/handlebars-{{ cookiecutter.static_dependencies['handlebars']['version'] }}/handlebars.min.js')}"></script>

    <!-- OverlayScrollbars -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/OverlayScrollbars-{{ cookiecutter.static_dependencies['overlay-scrollbars']['version'] }}/overlayscrollbars.browser.es6.min.js')}"></script>

    <!-- Popper -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/popper-{{ cookiecutter.static_dependencies['popper']['version'] }}/popper.min.js')}"></script>

    <!-- Tom Select -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/tom-select-{{ cookiecutter.static_dependencies['tom-select']['version'] }}/tom-select.complete.min.js')}"></script>

    <!-- Bootstrap -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/bootstrap-{{ cookiecutter.static_dependencies['bootstrap5']['version'] }}-dist/bootstrap.bundle.min.js')}"></script>

    <!-- AdminLTE -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/lib/AdminLTE-4.0.0-beta2/dist/js/adminlte.custom.js')}"></script>

    <!-- i18n -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/translations/{0}.js'.format(localizer.locale_name), _query=dict(version=app_version))}"></script>

    <!-- Custom scripts -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.utils.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.autocompleters.js', _query=dict(version=app_version))}"></script>
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.searches.js', _query=dict(version=app_version))}"></script>
${self.foot()}

    <script>
    const SELECTOR_SIDEBAR_WRAPPER = ".sidebar-wrapper";
    const Default = {
        scrollbarTheme: "os-theme-light",
        scrollbarAutoHide: "leave",
        scrollbarClickScroll: true,
    };
    document.addEventListener("DOMContentLoaded", function() {
        const sidebarWrapper = document.querySelector(SELECTOR_SIDEBAR_WRAPPER);
        if (sidebarWrapper && typeof OverlayScrollbarsGlobal?.OverlayScrollbars !== "undefined") {
            OverlayScrollbarsGlobal.OverlayScrollbars(sidebarWrapper, {
                scrollbars: {
                    theme: Default.scrollbarTheme,
                    autoHide: Default.scrollbarAutoHide,
                    clickScroll: Default.scrollbarClickScroll,
                },
            });
        }
    });
    </script>
</body>
</html>
