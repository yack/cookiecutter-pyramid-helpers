<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%def name="foot()">
${parent.foot()}
    <!-- Custom scripts -->
    <script src="${request.static_path('{{ cookiecutter.module_name }}:static/js/{{ cookiecutter.repo_name }}.select.js', _query=dict(version=app_version))}" type="text/javascript"></script>
</%def>

<%form:form name="select" method="post" role="form" enctype="multipart/form-data">
    <div class="modal modal-lg d-block overflow-visible position-relative">
        <div class="modal-dialog mt-0">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title h5"><i class="fa fa-cogs fa-fw me-1"></i> ${subtitle}</h3>
                </div><!-- /.modal-header -->

                <div class="modal-body">
                    <div class="mb-3 form-required">
                        <label class="form-label" for="qfile">${translate('File')}</label>
                        <div class="input-group input-upload">
                            <div class="input-group-text">
                                <label class="cursor-pointer">
                                    <span class="sr-only">${translate('Browse...')}</span>
                                    <i class="fa fa-upload"> </i>
                                    <%form:upload id="qfile" name="qfile" class_="d-none" accept=".csv,.xlsx" />
                                </label>
                            </div>
                            <%form:text class_="form-control" disabled="disabled" name="filename" />
                        </div>
                    </div><!-- /.mb-3 -->

                    <div class="row">
                        <div class="col-12 col-md-6 mb-3">
% if options.get('sheet') is True:
                            <label class="form-label" for="sheet">${translate('Sheet')}</label>
                            <div class="row">
                                <div class="col">
                                    <%form:text class_="form-control" id="sheet" name="sheet" aria_describedby="sheet-help" />
                                </div>
                                <div class="col-auto align-self-center" id="sheet-help">
                                    <i class="fa fa-info-circle text-secondary" data-bs-toggle="popover" data-bs-content="${translate('This is the sheet that contains data to be imported when uploaded file is a spreadsheet.') | n}"></i>
                                </div>
                            </div>
                        </div><!-- /.col -->
% endif

                        <div class="col-12 col-md-6 mb-3">
                            <div class="form-required">
                                <label class="form-label" for="version">${translate('Interface version')}</label>
                                <%form:select class_="form-select" id="version" name="version">
% for version in reversed(versions):
                                    <%form:option value="${version}">${version}</%form:option>
% endfor
                                </%form:select>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.modal-body -->

                <div class="modal-footer text-end p-3">
                    <a href="${cancel_link}" class="btn btn-secondary me-3"><i class="fa fa-times fa-fw me-1"></i> ${translate('Cancel')}</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-cogs fa-fw me-1"></i> ${translate('Select')}</button>
                </div><!-- /.modal-footer -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</%form:form>
