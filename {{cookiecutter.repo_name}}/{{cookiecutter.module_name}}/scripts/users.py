# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Import/export functions for users """

from {{ cookiecutter.module_name }}.scripts import CLI


def main(args=None):
    """ Import/export functions for users """

    cli = CLI.get('users')

    args = cli.parse_args(args=args)

    if not any([args.import_file, args.export_file]):
        cli.error('Nothing to do. Please set --import-file or --export-file.')

    # Import
    if args.import_file:
        with cli.tm:
            cli.do_import()

    # Export
    if args.export_file:
        query = cli.do_search(sort='label')

        with cli.tm:
            cli.do_export(query)

    return 0
