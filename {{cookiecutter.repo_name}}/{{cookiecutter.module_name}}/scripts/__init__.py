# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Command Line Interface """

from argparse import ArgumentParser
from functools import cached_property
import locale
import logging
import os
import sys

from pyramid.config import Configurator
from pyramid.events import NewRequest
from pyramid.paster import get_appsettings
from pyramid.paster import setup_logging
from pyramid.scripting import prepare

from pyramid_helpers.forms import State

from {{ cookiecutter.module_name }}.funcs import export_file
from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs import import_file
from {{ cookiecutter.module_name }}.funcs import register
from {{ cookiecutter.module_name }}.funcs.searches import get_search_by_label
from {{ cookiecutter.module_name }}.funcs.users import get_user_by_username


log = logging.getLogger(__name__)


class CLI(ArgumentParser):
    """ Command Line Interface """

    LOG_LEVELS = {
        'debug': logging.DEBUG,
        'error': logging.ERROR,
        'info': logging.INFO,
        'warning': logging.WARNING,
    }

    __id__ = None
    config = None
    target = None

    features = None
    requires = None

    args = None
    request = None

    def __getattr__(self, attr):
        """ Attribute lookup """

        # Request attributes
        if attr in ('pluralize', 'registry', 'tm', 'translate'):
            if self.request is None:
                raise AttributeError(f'No such attribute {attr}')

            return getattr(self.request, attr)

        # Module functions
        value = self.config.get(attr)
        if callable(value):
            return value

        raise AttributeError(f'No such attribute {attr}')

    @classmethod
    def add(cls, id_, config):
        """ Add a new CLI """

        # Register functions
        register(id_, config)

    @classmethod
    def get(cls, id_, **kwargs):
        """ Get a registered CLI """

        config = get_config(id_)
        if config is None:
            raise ValueError(f'Unconfigured CLI: {id_}')

        cli = cls(description=config['description'], **kwargs)

        cli.__id__ = id_
        cli.config = config
        cli.target = config.get('target')

        cli.features = {}
        cli.requires = {}

        # Compute features
        for feature in config['features']:
            cli.features[feature] = True
            for option, value in config[f'{feature}_options'].items():
                cli.features[f'{feature}.{option}'] = value

        # Compute requires
        for require in config['requires']:
            if ':' in require:
                require, permissions = require.rsplit(':', 1)
            else:
                permissions = 'rw'

            cli.requires[require] = permissions

        if 'import' in config['features']:
            cli.requires['exports'] = 'ro'
            cli.requires['imports'] = 'rw'

        elif 'export' in config['features']:
            cli.requires['exports'] = 'ro'

        return cli

    @cached_property
    def parent(self):
        """ Get cli parent """

        if self.__id__ == self.target:
            return None

        cli = CLI.get(self.target)
        if cli is None:
            return None

        cli.args = self.args
        cli.request = self.request

        return cli

    def add_common_arguments(self):
        """ Add common arguments """

        self.add_argument(
            'config_uri',
            help='Configuration file to use',
        )

        self.add_argument(
            '--log-level',
            choices=list(self.LOG_LEVELS),
            help='Set log level (Default is to use value from configuration file)'
        )

        if self.target:
            self.add_argument(
                '--search',
                help='Search label to get objects from',
            )

            self.add_argument(
                '-u', '--user',
                default='{{ cookiecutter.module_name }}',
                help='User (default is {{ cookiecutter.module_name }})'
            )

        if self.config.get('version'):
            self.add_argument(
                '--version',
                default=self.config['version'],
                help=f'Interface version (Default is: {self.config["version"]}, type --version=list to get list)'
            )

        if self.features.get('export') is True:
            self.add_argument(
                '-e', '--export-file',
                help='CSV/XLSX file path to export data to'
            )

            self.add_argument(
                '-f', '--force',
                action='store_true',
                help='Overwrite any existing file when exporting data'
            )

        if self.features.get('import') is True:
            self.add_argument(
                '-i', '--import-file',
                help='CSV/XLSX file path to import data from'
            )

            self.add_argument(
                '-d', '--dry-run',
                action='store_true',
                default=False,
                help='Dry RUN mode, do not commit changes into database'
            )

        if self.features.get('import.author') is True:
            self.add_argument(
                '-a', '--author',
                default='{{ cookiecutter.module_name }}',
                help='Import author (default is {{ cookiecutter.module_name }})'
            )

        if self.features.get('import.create_only') is True:
            self.add_argument(
                '-c', '--create-only',
                action='store_true',
                help='Only create missing objects (default is to create missing objects and update existing ones).'
            )

        if self.features.get('import.partial'):
            self.add_argument(
                '--column',
                action='append',
                dest='columns',
                help='Selected column for partial import (type --column=list to get list)'
            )

    def parse_args(self, args=None, namespace=None):
        """ Command line argument parsing methods """

        if args is None:
            args = sys.argv[1:]

        # Add common arguments
        self.add_common_arguments()

        # Calling inherited
        args = super().parse_args(args=args, namespace=namespace)

        config_uri = os.path.abspath(os.path.expanduser(args.config_uri))

        setup_logging(config_uri)

        if args.log_level:
            # Set main logger level
            logging.getLogger('{{ cookiecutter.module_name }}').setLevel(self.LOG_LEVELS[args.log_level])

        try:
            # Production
            settings = get_appsettings(config_uri, name='{{ cookiecutter.module_name }}')
        except LookupError:
            # Development
            settings = get_appsettings(config_uri)

        settings.setdefault('requires', self.requires)

        config = Configurator(settings=settings)

        # {{ cookiecutter.project_name }} setup
        config.include('{{ cookiecutter.module_name }}')

        # Transaction manager setup
        config.include('pyramid_tm')

        # Pyramid Helpers setup
        config.include('pyramid_helpers.i18n')

        # Make WSGI application
        config.make_wsgi_app()

        # Create a request object
        self.request = prepare()['request']

        # Initialize I18n
        self.request.headers['Accept-Language'] = locale.getlocale()[0]

        # Setup the request
        config.registry.notify(NewRequest(self.request))

        if self.target and args.search:
            search = get_search_by_label(self.request, self.target, args.search, args.user)
            if search is None:
                self.error(f'Invalid search {args.search}')

            args.search = search

        if self.features.get('export') is True and args.export_file:
            # Check export file
            filepath = os.path.abspath(os.path.expanduser(args.export_file))
            if os.path.isfile(filepath) and not args.force:
                self.error('Export file already exists, please use --force option to overwrite')

            _, extension = os.path.splitext(filepath)
            kind = extension[1:].lower()
            if kind not in ('csv', 'xlsx'):
                self.error(f'Unknown extension {extension}')

            args.export_file = filepath
            args.export_kind = kind

        partial = self.features.get('import.partial')
        if partial:
            # Check columns
            if args.columns:
                if len(args.columns) == 1 and args.columns[0] == 'list':
                    columns = ', '.join(partial)
                    self.exit(message=f'\nAvailable columns are:\n    {columns}\n')

                for column in args.columns:
                    if column not in partial:
                        self.error(f'Invalid column {column}')
            else:
                args.columns = []

        author = self.features.get('import.author')
        if author:
            # Check author
            if args.author:
                author = get_user_by_username(self.request, args.author, warn=False)
                if author is None:
                    self.error(f'Unknown author {args.author}')

                args.author = author
            else:
                args.author = None

        if self.config.get('version'):
            # Check version
            if args.version == 'list':
                versions = ', '.join(self.config['columns'])
                self.exit(message=f'\nAvailable versions are:\n    {versions}\n')
            elif args.version not in self.config['columns']:
                self.error(f'Invalid version {args.version}')

        if self.features.get('import') is True and args.import_file:
            # Check import file
            filepath = os.path.abspath(os.path.expanduser(args.import_file))
            if not os.path.isfile(filepath):
                self.error(f'Missing import file {filepath}')

            if not os.access(filepath, os.R_OK):
                self.error(f'Not enough permissions to access import file {filepath}')

            _, extension = os.path.splitext(filepath)
            kind = extension[1:].lower()
            if kind not in ('csv', 'xlsx'):
                self.error(f'Unknown extension {extension}')

            args.import_file = filepath
            args.import_kind = kind

        self.args = args

        return args

    def do_export(self, query, *args):
        """ Export data to file """

        kwargs = {
            key: getattr(self.args, key)
            for key in args
        }

        kwargs['format'] = self.args.export_kind

        if self.config.get('version'):
            kwargs['version'] = self.args.version

        with open(self.args.export_file, 'wb+' if self.args.export_kind == 'xlsx' else 'wt+') as fp:
            export_file(self.request, self.__id__, self.export_func, query, fp=fp, **kwargs)

    def do_import(self, *args):
        """ Import data from file """

        kwargs = {
            key: getattr(self.args, key)
            for key in args
        }

        kwargs['dry_run'] = self.args.dry_run
        kwargs['format'] = self.args.import_kind

        if self.config.get('version'):
            kwargs['version'] = self.args.version

        if self.features.get('import.author'):
            kwargs['author'] = self.args.author

        if self.features.get('import.create_only'):
            kwargs['create_only'] = self.args.create_only

        if self.features.get('import.partial'):
            kwargs['columns'] = self.args.columns

        with open(self.args.import_file, 'rb' if self.args.import_kind == 'xlsx' else 'rt') as fp:
            import_file(self.request, self.__id__, self.import_func, fp, **kwargs)

    def do_search(self, **kwargs):
        """ Search in database """

        if self.args.search:
            state = State(self.request)

            kwargs.update(self.args.search.to_python(state=state))

        return self.search_func(self.request, **kwargs)
