# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Database initialization script """

import logging

from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.funcs.users import get_user_by_username
from {{ cookiecutter.module_name }}.models import Base
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User
from {{ cookiecutter.module_name }}.scripts import CLI


log = logging.getLogger(__name__)


USERS = {
    'jane.doe': {
        'firstname': 'Jane',
        'lastname': 'Doe',
        'password': 'admin',
        'profiles': ['admin', ],
    },
    'john.doe': {
        'firstname': 'John',
        'lastname': 'Doe',
        'password': 'user',
        'profiles': ['user', ],
    },
}


def add_users(request, users):
    """ Add some users to database for test purpose """

    def add_user(username, **data):
        user = get_user_by_username(request, username, warn=False)
        if user is None:
            # Add new user
            user = User()
            DBSession.add(user)

            user.username = username

            action = 'create'
        else:
            action = 'modify'

        data.update({
            'status': 'active',
            'email': f'{username}@domain.tld',
        })

        if user.from_dict(data):
            log.info('[USER] [%s] username=%s, fullname=%s', action.upper(), user.username, user.fullname)

    for username, data in users.items():
        add_user(username, **data)


# Add database CLI
CLI.add(
    'database',
    {
        'title': N_('Database'),
        'description': N_('Initialization functions for database'),
    }
)


def main(args=None):
    """ Database initialization """

    cli = CLI.get('database')
    args = cli.parse_args(args=args)

    # Initialize database schema in declarative mode
    Base.metadata.create_all(bind=DBSession.bind)

    # Populate table users
    with cli.tm:
        add_users(cli.request, USERS)

    return 0
