# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Pyramid initialization """

import logging
import os
import time

import pkg_resources
import transaction

from pyramid.config import Configurator
from pyramid.settings import asbool
from pyramid.util import DottedNameResolver

from pyramid_helpers.utils import ConfigurationError

from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.resources import Root


APP_NAME = '{{ cookiecutter.project_name }}'
APP_INSTANCE = None
APP_INSTANCE_ID = None
APP_VERSION = None

log = logging.getLogger(__name__)


def get_active_section(request):
    """ Get active sidebar section from route name """

    route_name = request.matched_route.name if request.matched_route else None
    if request.exception or not route_name:
        return (route_name, None)

    if route_name.startswith('batches.'):
        batch = get_batch(request)
        return (route_name, batch['target'])

    if route_name in ('import', 'select'):
        type_ = request.matchdict.get('type')
        config = get_config(type_)
        if config is None:
            return (route_name, None)
        return (route_name, config['target'])

    for section in ('api-doc', ):
        if route_name == section:
            return (route_name, section)

    for section in ('users', ):
        if route_name.startswith(f'{section}.'):
            return (route_name, section)

    return (route_name, None)


def get_dirpath(config, key):
    """ Get a directory path from settings and check access if required """

    registry = config.registry
    settings = registry.settings

    requires = settings.get('requires') or {}

    permissions = requires.get(key)
    if permissions is None:
        return None

    if permissions == 'ro':
        permissions = os.R_OK | os.X_OK
    elif permissions == 'rw':
        permissions = os.R_OK | os.W_OK | os.X_OK
    else:
        raise ConfigurationError(f'Invalid permission for {key} dirpath entry, please fix the situation')

    dirpath = settings.get(f'{key}.dirpath')
    if dirpath is None:
        raise ConfigurationError(f'Missing {key} dirpath entry, please fix the situation')

    dirpath = os.path.abspath(dirpath)
    if not os.path.isdir(dirpath):
        raise ConfigurationError(f'Invalid {key} dirpath entry, please fix the situation')

    if not os.access(dirpath, permissions):
        raise ConfigurationError(f'Not enough permissions for {key} dirpath, please fix the situation')

    log.debug('[DIRPATH] key=%s, dirpath=%s, permissions=%s', key, dirpath, requires[key])

    return dirpath


def on_before_renderer(event):
    """ Add instance variables to context """

    request = event['request']

    registry = request.registry
    session = request.session
    settings = registry.settings

    app_settings = {}
    for key, value in settings.items():
        if not key.startswith('app.'):
            continue

        name = key.split('app.')[1]
        if name in ('instance', 'instance_id', 'name', 'version'):
            event[f'app_{name}'] = value
        else:
            app_settings[name] = value
    event['app_settings'] = app_settings

    # Set route name and active section
    route_name, active_section = get_active_section(request)

    event['route_name'] = route_name
    event['active_section'] = active_section

    session.setdefault('gui_settings', {})


def main(_, **settings):
    """
    This function returns a Pyramid WSGI application.
    """

    # Set dirpath requirements for WSGI application
    settings['requires'] = {
        'exports': 'ro',
        'imports': 'rw',
    }

    # Global config
    config = Configurator(
        root_factory=Root,
        settings=settings,
    )

    # {{ cookiecutter.project_name }}
    includeme(config)

    # Plugins setup
    with transaction.manager:
        config.include('{{ cookiecutter.module_name }}.plugins')

    # Mako setup
    config.include('pyramid_mako')

    # Session setup
    config.include('pyramid_beaker')

    # Transaction manager setup
    config.include('pyramid_tm')

    # Pyramid Helpers setup
    config.include('pyramid_helpers')

    # Pyramid debugtoolbar setup
    if asbool(settings.get('debugtoolbar.enabled')):
        config.include('pyramid_debugtoolbar')

    # Subscribers setup
    config.add_subscriber(on_before_renderer, 'pyramid.events.BeforeRender')

    # Static route
    config.add_static_view('static', '{{ cookiecutter.module_name }}:static', cache_max_age=3600)

    #
    # Applicative routes
    #
    config.add_route('index', '/')
    config.add_route('about', '/about')
    config.add_route('api-doc', '/api-doc')
    config.add_route('profile', '/profile')

    # Authentication
    config.add_route('auth.sign-in', '/auth/sign-in')
    config.add_route('auth.sign-out', '/auth/sign-out')

    # Import/Select
    config.add_route('import', '/{type}/import')
    config.add_route('select', '/{type}/select')

    # Batches
    config.add_route('batches.index', '/batches/{batch}', request_method='GET')
    config.add_route('batches.selection', '/batches/{batch}', request_method='PUT')
    config.add_route('batches.plugins', '/batches/{target}/{batch}/{plugin}')

    # Plugins
    config.add_route('reports.plugins', '/reports/{target}/{plugin}')

    # Searches
    config.add_route('searches.load', '/searches/:search', numeric_predicate='search')

    # Users
    config.add_route('users.create', '/users/create')
    config.add_route('users.modify', '/users/{user}/modify', numeric_predicate='user')
    config.add_route('users.search', '/users/search')
    config.add_route('users.visual', '/users/{user}', numeric_predicate='user')

    #
    # API routes
    #
    config.add_route('api.gui', '/api/1.0/gui', request_method='PUT')

    # Searches
    config.add_route('api.searches.create', '/api/1.0/searches/{target}', request_method='POST')
    config.add_route('api.searches.delete', '/api/1.0/searches/{search}', numeric_predicate='search', request_method='DELETE')
    config.add_route('api.searches.leave', '/api/1.0/searches/{target}/leave', request_method='POST')
    config.add_route('api.searches.menu', '/api/1.0/searches/menu', request_method='GET')
    config.add_route('api.searches.modify', '/api/1.0/searches/{search}', numeric_predicate='search', request_method='PUT')

    # Users
    config.add_route('api.users.create', '/api/1.0/users', request_method='POST')
    config.add_route('api.users.delete', '/api/1.0/users/{user}', numeric_predicate='user', request_method='DELETE')
    config.add_route('api.users.modify', '/api/1.0/users/{user}', numeric_predicate='user', request_method='PUT')
    config.add_route('api.users.search', '/api/1.0/users', request_method='GET')
    config.add_route('api.users.status', '/api/1.0/users/{user}/status', numeric_predicate='user', request_method='PUT')
    config.add_route('api.users.visual', '/api/1.0/users/{user}', numeric_predicate='user', request_method='GET')

    # Scan {{ cookiecutter.project_name }} views modules
    config.scan('{{ cookiecutter.module_name }}.views')

    # Create Pyramid WSGI app
    app = config.make_wsgi_app()

    return app


def pshell_setup(env):
    """
    pshell setup function

    This adds custom imports to `env` *after* application is initialized
    """

    custom_imports = {
        # Models
        'DBSession': '{{ cookiecutter.module_name }}.models.DBSession',
        'Search': '{{ cookiecutter.module_name }}.models.users.Search',
        'User': '{{ cookiecutter.module_name }}.models.users.User',
        # Scripts
        'CLI': '{{ cookiecutter.module_name }}.scripts.CLI',
    }

    resolver = DottedNameResolver(None)

    for key, dotted in custom_imports.items():
        env[key] = resolver.maybe_resolve(dotted)


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('{{ cookiecutter.module_name }}')
    """

    registry = config.registry
    settings = registry.settings

    config.add_directive('get_dirpath', get_dirpath)

    # Set application instance, name and version
    if settings.get('app.instance') is None:
        settings['app.instance'] = APP_INSTANCE

    if settings.get('app.instance_id') is None:
        settings['app.instance_id'] = APP_INSTANCE_ID

    if settings.get('app.name') is None:
        settings['app.name'] = APP_NAME

    if settings.get('app.version') is None:
        settings['app.version'] = f'V{pkg_resources.get_distribution(__name__).version}'

    elif settings['app.version'] == 'devel':
        settings['app.version'] = str(int(time.time()))

    # Theme & colors
    if settings.get('app.themes.sidebar') is None:
        settings['app.themes.sidebar'] = 'dark'
    if settings.get('app.colors.navbar') is None:
        settings['app.colors.navbar'] = 'white'
    if settings.get('app.colors.accent') is None:
        settings['app.colors.accent'] = 'primary'
    if settings.get('app.colors.sidebar') is None:
        settings['app.colors.sidebar'] = 'primary'
    if settings.get('app.colors.brand_logo') is None:
        settings['app.colors.brand_logo'] = 'dark'

    # Model setup
    config.include('{{ cookiecutter.module_name }}.models')

    # Functions setup
    with transaction.manager:
        config.include('{{ cookiecutter.module_name }}.funcs')
