# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Common form helpers for batches """

import formencode
from formencode import validators

from pyramid_helpers.forms.validators import List


#
# Forms
#

# batches.selection
class BatchSelectionForm(formencode.Schema):
    """
    :select_all: Whether to select all matched ids or not
    :select_list: List of ids to select
    :select_none: Whether to clear selection or not
    :select_one: Id to select
    """

    allow_extra_fields = True
    filter_extra_fields = True

    select_all = validators.StringBool(if_missing=False, if_empty=False)
    select_list = List(validators.Int())
    select_none = validators.StringBool(if_missing=False, if_empty=False)
    select_one = validators.Int(if_missing=None, if_empty=None)
