# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Common form helpers """

import logging
import zoneinfo

import formencode
from formencode import validators

from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User


log = logging.getLogger(__name__)


#
# Validators
#

class PasswordMatchValidator(validators.FormValidator):
    """
    Form validator that checks if the value of the fields `password` and
    `password_confirm` are identical.
    """

    messages = {
        'dont_match': N_('Passwords do not match'),
    }

    def _convert_from_python(self, value, state):
        return value

    def _convert_to_python(self, field_dict, state):
        errors = {}

        if field_dict['password'] != field_dict['password_confirm']:
            errors['password'] = formencode.Invalid(self.message('dont_match', state), field_dict['password'], state)
            errors['password_confirm'] = formencode.Invalid(self.message('dont_match', state), field_dict['password_confirm'], state)

        if errors:
            error_list = sorted(errors.items())
            error_message = ', '.join([value.msg for name, value in error_list if value])
            raise formencode.Invalid(error_message, field_dict, state, error_dict=errors)

        # No error
        return field_dict


#
# Object validators
#

class SimpleObjectValidatorMixin:
    """ Validator mixin for SQLAlchemy ORM classes """

    __model__ = None
    __msg__ = N_('Invalid value')
    __pkey__ = 'id'

    def __init__(self, *args, **kwargs):
        # Calling inherited
        super().__init__(*args, **kwargs)

        # Set custom message
        self._messages['integer'] = self.__msg__
        self._messages['invalid'] = self.__msg__

    def _convert_from_python(self, value, state):
        if not isinstance(value, self.__model__):   # pylint: disable=isinstance-second-argument-not-valid-type
            raise formencode.Invalid(self.message('invalid', state), value, state)

        value = getattr(value, self.__pkey__)

        # Calling inherited
        return super()._convert_from_python(value, state)

    def _convert_to_python(self, value, state):
        assert self.__model__ is not None, 'Using an unconfigured SimpleObjectValidatorMixin class, please set __model__ attribute'

        # Calling inherited
        value = super()._convert_to_python(value, state)

        instance = DBSession.get(self.__model__, value)
        if instance is None:
            raise formencode.Invalid(self.message('invalid', state), value, state)

        return instance


class UserValidator(SimpleObjectValidatorMixin, validators.Int):
    """ Fancy validator for User objects """

    __model__ = User


#
# Forms
#

# import
class ImportForm(formencode.Schema):
    """
    Import form

    :param columns: Columns to import for partial import
    :param create_only: Create only missing objects
    :param force: Bypass some checks during import
    :param partial: Whether this is a partial import or not
    :param qfile: The file to import objects from
    :param sheet: Sheet name to import data from for XLSX files
    :param suffix: Suffix of the file
    :param version: Version of the export interface
    """

    allow_extra_fields = True
    filter_extra_fields = True

    columns = formencode.ForEach(validators.String())
    create_only = validators.StringBool(if_missing=False, if_empty=False)
    force = validators.StringBool(if_missing=False, if_empty=False)
    partial = validators.StringBool(if_missing=False, if_empty=False)
    qfile = validators.FieldStorageUploadConverter(not_empty=True)
    sheet = validators.String(if_missing=None, if_empty=None)
    suffix = validators.Regex(r'^[0-9]{8,8}(-[0-9A-Za-z]{2,2})?$', not_empty=True)
    version = validators.String(not_empty=True)

    def _validate_python(self, field_dict, state):
        """ Check partial import settings """

        translate = state.translate

        errors = {}

        if field_dict['create_only'] and field_dict['partial']:
            errors['create_only'] = formencode.Invalid(translate('FIXME'), True, state)
            errors['partial'] = formencode.Invalid(translate('FIXME'), True, state)

        if field_dict['partial'] and not field_dict['columns']:
            errors['columns'] = formencode.Invalid(translate('Please select a value'), field_dict['columns'], state)

        if errors:
            error_list = sorted(errors.items())
            error_message = ', '.join([value.msg for name, value in error_list if value])
            raise formencode.Invalid(error_message, field_dict, state, error_dict=errors)

        # No error
        return field_dict


# profile
class ProfileForm(formencode.Schema):
    """
    :param description: Description of the user
    :param email: E-mail of the user
    :param firstname: Firtsname of the user
    :param lastname: Lastname of the user
    :param password: Password of the user
    :param password_confirm: Password confirmation
    :param timezone: Timezone of the user
    :param username: Username of the user
    """
    allow_extra_fields = True
    filter_extra_fields = True

    description = validators.String(if_missing=None, if_empty=None)
    email = validators.Email(if_missing=None, if_empty=None)
    firstname = validators.String(if_missing=None, if_empty=None)
    lastname = validators.String(if_missing=None, if_empty=None)
    password = validators.String(if_missing=None, if_empty=None)
    password_confirm = validators.String(if_missing=None, if_empty=None)
    timezone = validators.OneOf(zoneinfo.available_timezones(), if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)

    chained_validators = [PasswordMatchValidator(), ]


# select
class SelectForm(formencode.Schema):
    """
    Select form

    :param qfile: The file to select objects from
    :param sheet: Sheet name to import data from for XLSX files
    :param version: Version of the export interface
    """

    allow_extra_fields = True
    filter_extra_fields = True

    qfile = validators.FieldStorageUploadConverter(not_empty=True)
    sheet = validators.String(if_missing=None, if_empty=None)
    version = validators.String(not_empty=True)
