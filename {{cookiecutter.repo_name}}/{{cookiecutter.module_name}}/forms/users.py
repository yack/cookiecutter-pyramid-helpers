# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for users """

import logging
import zoneinfo

import formencode
from formencode import validators

from {{ cookiecutter.module_name }}.forms.common import PasswordMatchValidator
from {{ cookiecutter.module_name }}.models.users import User

log = logging.getLogger(__name__)


#
# Validators
#


#
# Forms
#

# api.users.create, api.users.modify, users.create, users.modify
class CreateForm(formencode.Schema):
    """
    :param description: Description of the user
    :param email: E-mail of the user
    :param firstname: Firtsname of the user
    :param groups: List of groups for the user
    :param lastname: Lastname of the user
    :param password: Password of the user
    :param password_confirm: Password confirmation
    :param status: Status of the user
    :param timezone: Timezone of the user
    :param username: Username of the user
    """

    allow_extra_fields = True
    filter_extra_fields = True

    description = validators.String(if_missing=None, if_empty=None)
    email = validators.Email(not_empty=True)
    firstname = validators.String(if_missing=None, if_empty=None)
    lastname = validators.String(if_missing=None, if_empty=None)
    password = validators.String(if_missing=None, if_empty=None)
    password_confirm = validators.String(if_missing=None, if_empty=None)
    profiles = formencode.ForEach(validators.OneOf(list(User.PROFILES)))
    status = validators.OneOf(list(User.STATUSES), not_empty=True)
    timezone = validators.OneOf(zoneinfo.available_timezones(), if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)

    chained_validators = [PasswordMatchValidator(), ]


# api.users.search, users.search
class SearchForm(formencode.Schema):
    """
    :param exact: Term must exactly match
    :param format: Rendering format
    :param group: Group name
    :param status: Status of user
    :param term: Term to search
    """

    allow_extra_fields = True
    filter_extra_fields = True

    excluded_ids = formencode.ForEach(validators.Int())
    selected_ids = formencode.ForEach(validators.Int())

    exact = validators.StringBool(if_missing=False, if_empty=False)
    format = validators.OneOf(['csv', 'html', 'json', 'xlsx'], if_missing='html', if_empty='html', if_invalid='html')
    profiles = formencode.ForEach(validators.OneOf(list(User.PROFILES)))
    statuses = formencode.ForEach(validators.OneOf(list(User.STATUSES)))
    term = validators.String(if_missing=None, if_empty=None)


# api.users.status, batches.users.status
class StatusForm(formencode.Schema):
    """
    :param status: Status of user
    """

    allow_extra_fields = True
    filter_extra_fields = True

    status = validators.OneOf(list(User.STATUSES), not_empty=True)
