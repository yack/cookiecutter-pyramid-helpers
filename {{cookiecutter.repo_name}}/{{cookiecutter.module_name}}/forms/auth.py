# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for authentication """

import logging

import formencode
from formencode import validators


log = logging.getLogger(__name__)

#
# Forms
#


# auth.sign-in
class SignInForm(formencode.Schema):
    """
    :param password: The password to validate
    :param redirect: URL to redirect to if authentication is successful
    :param username: The username to authenticate
    """
    allow_extra_fields = True
    filter_extra_fields = True

    password = validators.String(not_empty=True)
    redirect = validators.String(if_missing=None, if_empty=None)
    username = validators.String(not_empty=True)
