# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Forms for searches """

import formencode
from formencode import validators


#
# Forms
#

# api.searches.create, api.searches.modify
class CreateForm(formencode.Schema):
    """
    :param batch: Batch Id to get selected Ids from or None
    :param description: Description of the search
    :param label: Label of the search
    :param shared: Whether the search is shared or not
    """

    allow_extra_fields = True
    filter_extra_fields = True

    batch = validators.String(if_empty=None, if_missing=None)
    description = validators.String(if_empty=None, if_missing=None)
    label = validators.String(not_empty=True)
    shared = validators.StringBool(if_missing=False, if_empty=False)
