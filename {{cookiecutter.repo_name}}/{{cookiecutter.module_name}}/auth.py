# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Authentication module for {{ cookiecutter.project_name }} """

import datetime
import logging

from {{cookiecutter.module_name}}.funcs.users import get_user_by_username

log = logging.getLogger(__name__)


def get_principals(request, username):
    """ Get principals for user """

    user = get_user_by_username(request, username)
    if user is None:
        # Invalid user
        return None

    # Construct effective principals with user profile
    if user.profiles:
        principals = [f'profile:{profile}' for profile in user.profiles]
    else:
        principals = []

    if 'hello' not in request.session:
        user.last_login = datetime.datetime.now(datetime.timezone.utc)
        request.session['hello'] = True

    return principals
