# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Plugins """

import bisect
import importlib
import io
import logging
import os
import sys

from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import Response

from pyramid_helpers.forms import validate
from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.funcs import export_file
from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.resources import ALL_PERMISSIONS
from {{ cookiecutter.module_name }}.views.common import ensure_permission


# Global variables
log = logging.getLogger(__name__)

_plugins = {}


class ExportMixin:
    """ Export plugin mixin """

    __section__ = N_('Exports')
    __renderers__ = {}

    def render(self, request, criteria, **kwargs):
        """ Export objects """

        kwargs['format'] = criteria.pop('format', 'xlsx')

        config = get_config(self.__export__)
        export_func = config['export_func']
        search_func = config['search_func']

        query = search_func(request, isouter=True, **criteria)

        fp, content_type, filename = export_file(request, self.__export__, export_func, query, **kwargs)

        if hasattr(fp, 'encoding'):
            fp = io.BytesIO(bytes(fp.read(), encoding=fp.encoding or 'utf-8'))

        response = Response(
            body_file=fp,
            content_encoding='utf-8',
            content_type=content_type,
            content_disposition=f'attachment; filename="{filename}"'
        )

        return response

    # pylint: disable=unused-argument
    def render_batch(self, request, batch, *args, **kwargs):
        """ Export objects from batch """

        return self.render(request, {'selected_ids': batch['selected_ids']})

    # pylint: disable=unused-argument
    def render_query(self, request, *args, **kwargs):
        """ Export objects from search query """

        config = get_config(self.__export__)
        search_form = config['search_form']

        @validate('search', search_form, method='get')
        def validate_(request):
            return request.forms['search']

        form = validate_(request)

        if form.errors or form.result['format'] not in ('csv', 'xlsx'):
            # Let the search view handle this
            return HTTPFound(location=request.route_path(f'{self.__target__}.search', _query=request.GET))

        return self.render(request, form.result)


class PluginRegistry(type):
    """ Plugin registry meta class """

    def __init__(cls, *args, **kwargs):
        # Calling inherited
        super().__init__(*args, **kwargs)

        if not hasattr(cls, '__plugins__'):
            # First call, cls is the Plugin class
            cls.__plugins__ = True
            return

        # Sanity checks
        if cls.__target__ is None:
            log.error('Missing target for plugin %s, please set __target__ attribute', cls.__name__)
            return

        if cls.__target__ not in Plugin.TARGETS:
            log.error('Invalid target for plugin %s.', cls.__name__)
            return

        if cls.__section__ is None:
            log.error('Missing section for plugin %s, please set __section__ attribute', cls.__name__)
            return

        if cls.__id__ is None:
            log.error('Missing id for plugin %s, please set __id__ attribute', cls.__name__)
            return

        if cls.__renderers__:
            for key, renderer in cls.__renderers__.items():
                if renderer.endswith('.mako') and '/' not in renderer and ':' not in renderer:
                    # Set template relative to parent
                    parent_name = '.'.join(cls.__module__.split('.')[:-1])
                    cls.__renderers__[key] = f'{parent_name}:{renderer}'
        else:
            cls.__renderers__ = {}

        if cls.__target__ not in _plugins:
            _plugins[cls.__target__] = {'__ids__': {}}

        elif cls.__id__ in _plugins[cls.__target__]['__ids__']:
            log.error('Duplicate id for plugin %s: %s, please change __id__ attribute', cls.__name__, cls.__id__)
            return

        if cls.__section__ not in _plugins[cls.__target__]:
            _plugins[cls.__target__][cls.__section__] = []

        # Add permission if needed
        if cls.__permission__ and cls.__permission__ not in ALL_PERMISSIONS:
            ALL_PERMISSIONS.append(cls.__permission__)

        # Instantiate the plugin
        plugin = cls()

        _plugins[plugin.__target__]['__ids__'][plugin.__id__] = plugin

        # Store it at the right place
        plugins = _plugins[plugin.__target__][plugin.__section__]
        ranks = [p.__rank__ for p in plugins]
        index = bisect.bisect_left(ranks, plugin.__rank__)

        plugins.insert(index, plugin)

        log.debug('Plugin %s (%s.%s) now registered.', plugin.__name__, plugin.__target__, plugin.__id__)


class Plugin(metaclass=PluginRegistry):
    """ Base class plugin """

    __id__ = None

    __target__ = None
    __section__ = None
    __renderers__ = None

    __icon__ = None
    __name__ = None
    __desc__ = None
    __rank__ = 0

    TARGETS = {
        'users': N_('Users'),
    }

    def __call__(self, request, mode, *args, **kwargs):
        session = request.session
        translate = request.translate

        # Set main renderer
        request.override_renderer = self.__renderers__.get('main', '/plugins/missing.mako')

        if mode == 'batch':
            batch = get_batch(request)
            if batch is None or batch['target'] != self.__target__:
                raise HTTPNotFound(detail=translate('Invalid batch id'))

            if not batch['selected_ids']:
                # Flash message
                session.flash({
                    'message': translate('No items selected'),
                    'type': 'error',
                })
                return HTTPFound(location=request.route_path(f'{self.__target__}.search'))

            data = self.render_batch(request, batch, *args, **kwargs)   # pylint: disable=assignment-from-no-return
        else:
            data = self.render_query(request, *args, **kwargs)          # pylint: disable=assignment-from-no-return

        if request.is_response(data):
            # View returned a response object
            return data

        # Add missing keys to data
        # pylint: disable=unsupported-membership-test,unsupported-assignment-operation
        if 'plugin' not in data:
            data['plugin'] = self

        if mode == 'batch':
            if 'batch' not in data:
                data['batch'] = batch

            if 'breadcrumb' not in data:
                data['breadcrumb'] = [
                    (translate(self.TARGETS[self.__target__]), request.route_path(f'{self.__target__}.search')),
                    (translate('Batch'), request.route_path('batches.index', batch=batch['id'])),
                    (translate(self.__name__), request.path_qs),
                ]

            if 'cancel_link' not in data:
                data['cancel_link'] = request.route_path('batches.index', batch=batch['id'])

        else:
            if 'breadcrumb' not in data:
                data['breadcrumb'] = [
                    (translate(self.TARGETS[self.__target__]), request.route_path(f'{self.__target__}.search')),
                    (translate(self.__name__), request.path_qs),
                ]

            if 'cancel_link' not in data:
                data['cancel_link'] = request.route_path(f'{self.__target__}.search')

        if 'subtitle' not in data:
            data['subtitle'] = translate(self.__desc__)

        if 'title' not in data:
            data['title'] = translate(self.__name__)

        return data

    # pylint: disable=unused-argument
    def render_batch(self, request, batch, *args, **kwargs):
        """ Method should be overridden by plugin to implement call from batch process """
        translate = request.translate

        raise HTTPNotFound(detail=translate('not found'))

    def render_query(self, request, *args, **kwargs):
        """ Method should be overridden by plugin to implement direct call from query """
        translate = request.translate

        raise HTTPNotFound(detail=translate('not found'))


def get_plugins(target):
    """ Get registered plugins for target """

    if target not in _plugins:
        return {}

    return {
        section: plugins
        for section, plugins in _plugins[target].items()
        if section != '__ids__'
    }


def get_plugin(target, plugin_id):
    """ Get plugin by target and id """

    plugins = _plugins.get(target)
    if plugins is None:
        return None

    return plugins['__ids__'].get(plugin_id)


def load_source(module_name, filepath):
    """ Load a python module from source file """

    spec = importlib.util.spec_from_file_location(module_name, filepath)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)

    return module


def view(request, mode):
    """ Pyramid view for plugins """

    translate = request.translate

    target = request.matchdict.get('target')
    __id__ = request.matchdict.get('plugin')

    # Retrieve plugin instance
    plugin = get_plugin(target, __id__)
    if plugin is None:
        raise HTTPNotFound(detail=translate('Invalid plugin Id'))

    # Check permission if any
    if plugin.__permission__:
        ensure_permission(request, plugin.__permission__)

    # Call the plugin
    return plugin(request, mode)


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('{{ cookiecutter.module_name }}.plugins')
    """

    registry = config.registry
    settings = registry.settings

    # Register core plugins
    config.scan('.')

    # Register external plugins
    directories = [
        path.strip()
        for path in settings.get('plugins.directories', '').split()
        if path.strip()
    ]

    for index, directory in enumerate(directories):
        dirpath = os.path.abspath(directory)
        if not os.path.isdir(dirpath) or not os.access(dirpath, os.R_OK | os.X_OK):
            continue

        # Load __init__ file to create parent module
        filepath = os.path.join(dirpath, '__init__.py')
        if not os.path.isfile(filepath) or not os.access(filepath, os.R_OK):
            continue

        dirname = os.path.basename(dirpath)
        parent = load_source(f'{{ cookiecutter.module_name }}.plugins.{dirname}_{index:03d}', filepath)

        for filename in os.listdir(dirpath):
            if not filename.endswith('.py') or filename == '__init__.py':
                continue

            filepath = os.path.join(dirpath, filename)
            if not os.path.isfile(filepath) or not os.access(filepath, os.R_OK):
                continue

            name = os.path.splitext(filename)[0].replace('-', '_')
            fullname = f'{parent.__name__}.{name}'
            try:
                # Load module and launch setup if needed
                module = load_source(fullname, filepath)
                if hasattr(module, 'setup'):
                    module.setup(config)

            except Exception:   # pylint: disable=broad-except
                log.exception('Failed to load plugin %s from file %s', name, filepath)

    # Add plugins views with fake template
    config.add_view(lambda request: view(request, 'batch'), route_name='batches.plugins', renderer='mako')
    config.add_view(lambda request: view(request, 'query'), route_name='reports.plugins', renderer='mako')
