<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<form name="delete" method="post" role="form">
    <div class="card w-50 m-auto">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-trash fa-fw me-1"></i> ${subtitle}</h3>
        </div><!-- /.card-header -->

        <div class="card-body">
            ${message | n}
        </div><!-- /.card-body -->

        <div class="card-footer text-end p-3">
            <a href="${cancel_link}" class="btn btn-secondary me-3"><i class="fa fa-times fa-fw me-1"></i> ${translate('Cancel')}</a>
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash fa-fw me-1"></i> ${translate('Delete')}</button>
        </div><!-- /.card-footer -->
    </div><!-- /.card -->
</form>
