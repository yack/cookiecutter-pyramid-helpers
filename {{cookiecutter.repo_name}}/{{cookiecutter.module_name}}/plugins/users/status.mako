<%!
from {{ cookiecutter.module_name }}.models.users import User

all_statuses = set()
for statuses in User.STATUS_FRAMEWORK.values():
    all_statuses.update(statuses)

statuses = {
    value: name
    for value, name in User.STATUSES.items()
    if value in all_statuses
}
%>\
<%namespace name="form" file="/form-tags.mako"/>
<%inherit file="/site.mako" />
<%form:form name="status" method="post" role="form">
    <div class="card w-50 m-auto">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-power-off fa-fw me-1"></i> ${subtitle}</h3>
        </div><!-- /.card-header -->

        <div class="card-body">
            <%form:errors name="form" />
            <div class="row form-group form-required">
                <div class="col-12 col-md-2">
                    <label for="status" class="col-form-label">${translate('Status')}</label>
                </div><!-- /.col -->
                <div class="col-12 col-md-10">
                    <%form:select class_="form-control form-select" id="status" name="status">
% for value, name in statuses.items():
                        <%form:option value="${value}">${translate(name)}</%form:option>
% endfor
                    </%form:select>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.card-body -->

        <div class="card-footer text-end p-3">
            <a href="${cancel_link}" class="btn btn-secondary me-3"><i class="fa fa-times fa-fw me-1"></i> ${translate('Cancel')}</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save fa-fw me-1"></i> ${translate('Save')}</button>
        </div><!-- /.card-footer -->
    </div><!-- /.card -->
</%form:form>
