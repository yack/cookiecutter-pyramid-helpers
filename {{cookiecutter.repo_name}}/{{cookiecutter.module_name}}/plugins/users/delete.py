# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Deletion plugin for users """


from pyramid.httpexceptions import HTTPFound

from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.plugins import Plugin
from {{ cookiecutter.module_name }}.plugins.users import build_batch_query


class UsersDeletePlugin(Plugin):
    """ Plugin that deletes users """

    __id__ = 'delete'

    __target__ = 'users'
    __section__ = N_('Manage')
    __renderers__ = {
        'main': 'delete.mako',
    }
    __permission__ = 'batches.users.delete'

    __icon__ = 'trash'
    __name__ = N_('Delete')
    __desc__ = N_('Delete selected user(s)')
    __rank__ = 0

    def render_batch(self, request, batch, *args, **kwargs):
        """ Delete users from batch """

        pluralize = request.pluralize
        session = request.session
        translate = request.translate

        # Build query
        _, select, count = build_batch_query(request)
        if select is None:
            return HTTPFound(location=request.route_path('batches.index', batch=batch['id']))

        if request.method == 'POST':
            delete_objs = []
            for user in DBSession.execute(select).scalars():
                delete_objs.append(user.fullname)

                DBSession.delete(user)

            # Flash message
            session.flash({
                'message': pluralize(
                    'Deleted {0} user.',
                    'Deleted {0} users.',
                    len(delete_objs)
                ).format(len(delete_objs)),
                'type': 'success',
            })

            return HTTPFound(location=request.route_path('users.search'))

        breadcrumb = [
            (translate('Users'), request.route_path('users.search')),
            (translate('Batch'), request.route_path('batches.index', batch=batch['id'])),
            (translate('Deletion'), request.route_path('batches.plugins', batch=batch['id'], plugin=self.__id__, target=self.__target__)),
        ]

        cancel_link = request.route_path('batches.index', batch=batch['id'])

        message = pluralize(
            'Do you really want to delete <b>{0}</b> user?',
            'Do you really want to delete <b>{0}</b> users?',
            count
        ).format(count)

        return {
            'batch': batch,
            'breadcrumb': breadcrumb,
            'cancel_link': cancel_link,
            'message': message,
            'show_batch': False,
            'subtitle': translate('Confirmation is required'),
            'title': translate('Users'),
        }
