# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} plugins for users """

from pyramid.httpexceptions import HTTPNotFound

import sqlalchemy as sa

from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.funcs.users import search_users
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User


def build_batch_query(request, **criteria):
    """ Build a batch query """

    session = request.session
    translate = request.translate

    batch = get_batch(request)
    if batch is None or batch['target'] != 'users':
        raise HTTPNotFound(detail=translate('Invalid batch id'))

    if not batch['selected_ids']:
        # Flash message
        session.flash({
            'message': translate('No users selected'),
            'type': 'error',
        })
        return (batch, None, 0)

    criteria['selected_ids'] = batch['selected_ids']

    # Query
    select = search_users(request, sort='fullname', order='asc', **criteria)

    # Check selection
    # pylint: disable=not-callable
    count = DBSession.execute(select.order_by(None).with_only_columns(sa.func.count(User.id))).scalar()

    # Empty selection
    if count == 0:
        # Flash message
        session.flash({
            'message': translate('No users selected'),
            'type': 'error',
        })
        return (batch, None, count)

    return (batch, select, count)
