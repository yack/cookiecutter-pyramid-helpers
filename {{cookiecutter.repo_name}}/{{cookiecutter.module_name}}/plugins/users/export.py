# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Export plugin for users """


from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.plugins import ExportMixin
from {{ cookiecutter.module_name }}.plugins import Plugin


class UsersExportPlugin(ExportMixin, Plugin):
    """ Plugin that exports users """

    __id__ = 'export'

    __target__ = 'users'
    __export__ = 'users'
    __permission__ = 'users.export'

    __icon__ = 'users'
    __name__ = N_('Users')
    __desc__ = N_('Export selected user(s)')
    __rank__ = 0
