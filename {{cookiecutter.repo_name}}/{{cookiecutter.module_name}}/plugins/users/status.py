# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Status plugin for users """


from pyramid.httpexceptions import HTTPFound

from pyramid_helpers.forms import validate
from pyramid_helpers.i18n import N_

from {{ cookiecutter.module_name }}.forms.users import StatusForm
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User
from {{ cookiecutter.module_name }}.plugins import Plugin
from {{ cookiecutter.module_name }}.plugins.users import build_batch_query


class UsersStatusPlugin(Plugin):
    """ Plugin that changes users' status """

    __id__ = 'status'

    __target__ = 'users'
    __section__ = N_('Manage')
    __renderers__ = {
        'main': 'status.mako',
    }
    __permission__ = 'batches.users.status'

    __icon__ = 'power-off'
    __name__ = N_('Status')
    __desc__ = N_('Change status for selected user(s) regardless current status value')
    __rank__ = 0

    @validate('status', StatusForm)
    def render_batch(self, request, batch, *args, **kwargs):
        """ Change users's status from batch """

        pluralize = request.pluralize
        session = request.session
        translate = request.translate
        form = request.forms['status']

        # Build query
        _, select, _ = build_batch_query(request)
        if select is None:
            return HTTPFound(location=request.route_path('batches.index', batch=batch['id']))

        if request.method == 'POST':
            if not form.errors:
                new_status = form.result['status']
                modify_objs = []
                failed_objs = []
                for user in DBSession.execute(select).scalars():
                    # Change status
                    if user.set_status(new_status):
                        modify_objs.append(user)
                    else:
                        failed_objs.append(user)

                if modify_objs:
                    # Flash message
                    session.flash({
                        'message': pluralize(
                            'Changed status to «{0}» for {1} user.',
                            'Changed status to «{0}» for {1} users.',
                            len(modify_objs)
                        ).format(
                            translate(User.STATUSES[new_status]),
                            len(modify_objs)
                        ),
                        'type': 'success',
                    })

                if failed_objs:
                    # Flash message
                    session.flash({
                        'message': pluralize(
                            'Failed to change status to «{0}» for {1} user.',
                            'Failed to change status to «{0}» for {1} users.',
                            len(failed_objs)
                        ).format(
                            translate(User.STATUSES[new_status]),
                            len(failed_objs)
                        ),
                        'type': 'error',
                    })

                return HTTPFound(location=request.route_path('users.search'))

        else:
            data = {
                'status': 'active',
            }
            form.from_python(data)

        breadcrumb = [
            (translate('Users'), request.route_path('users.search')),
            (translate('Batch'), request.route_path('batches.index', batch=batch['id'])),
            (translate('Status'), request.route_path('batches.plugins', batch=batch['id'], plugin=self.__id__, target=self.__target__)),
        ]

        cancel_link = request.route_path('batches.index', batch=batch['id'])

        return {
            'batch': batch,
            'breadcrumb': breadcrumb,
            'cancel_link': cancel_link,
            'show_batch': False,
            'subtitle': translate('Change status (batch)'),
            'title': translate('Users'),
        }
