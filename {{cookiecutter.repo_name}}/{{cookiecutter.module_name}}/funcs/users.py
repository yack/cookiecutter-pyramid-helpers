# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} functions for users management """

import logging

import unidecode

import sqlalchemy as sa

from pyramid_helpers.i18n import N_
from pyramid_helpers.utils import get_settings

from {{ cookiecutter.module_name }}.forms.users import CreateForm
from {{ cookiecutter.module_name }}.forms.users import SearchForm
from {{ cookiecutter.module_name }}.funcs import register
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User


# Export interface
EXP_INTERFACE = 'USER'

EXP_COLUMNS = {}
EXP_COLUMNS['V1'] = [
    'interface',
    'version',
    'status',
    'username',
    'firstname',
    'lastname',
    'profiles',
    'email',
    'timezone',
    'description',
]

EXP_VERSION = 'V1'

# Mandatory columns
COLS_MANDATORY_CREATE = [
    'profiles',
    'status',
    'username',
]

COLS_MANDATORY_SELECT = [
    'status',
    'username',
]

# Partial import
COLS_PARTIAL_IMPORT = [
    'description',
    'email',
    'firstname',
    'lastname',
    'profiles',
    'status',
    'timezone',
]


log = logging.getLogger(__name__)


def can_set_password(request):
    """ Check whether we can to set password or not """

    params = get_settings(request, 'auth', 'auth')
    return params['backend'] == 'database'


def create_or_modify(request, form, user=None):
    """ Create or modify a user """

    translate = request.translate

    if form.errors:
        return None

    # Check whether we need to set password or not
    set_password = can_set_password(request)

    # Check username unicity
    same_user = DBSession.execute(sa.select(User).filter_by(username=form.result['username'].lower()).limit(1)).scalar()
    if same_user and same_user != user:
        form.errors['username'] = translate('Username already used by another user')

    # if new user, check that both password and password_confirm are not empty
    if set_password and user is None and form.result['password'] is None and form.result['password_confirm'] is None:
        form.errors['password'] = translate('Please enter a value')
        form.errors['password_confirm'] = translate('Please enter a value')

    # Check profiles
    profiles = form.result.get('profiles')
    if not profiles and (user is None or 'profiles' in form.result):
        form.errors['profiles'] = translate('Missing value')

    if form.errors:
        return None

    if user is None:
        user = User()
        DBSession.add(user)

    data = form.result.copy()
    if not set_password or data['password'] is None:
        del data['password']

    # Update object
    user.from_dict(data)

    # Flush the session to get new user id
    DBSession.flush()

    return user


# pylint: disable=unused-argument
def export_users(request, select, doc, add_header=True, callback=None, version=EXP_VERSION):
    """ Export users from query """

    doc.clear()

    # Get header
    if version not in EXP_COLUMNS:
        raise ValueError(f'Invalid export version {version}')

    header = EXP_COLUMNS[version][:]

    # Let the callback add it's columns
    if callback:
        callback(None, header)

    # Add header
    if add_header:
        doc.append(header)

    for user in DBSession.execute(select).scalars():
        # Get data from user
        data = user.to_dict(context='export')

        # Convert some items to suitable format
        data['profiles'] = ', '.join(data['profiles'])

        if callback:
            callback(user, data)

        doc.append_dict(data, EXP_INTERFACE, version, header)


def get_user(request):
    """ Get an user """

    user_id = request.matchdict.get('user')
    if user_id is None:
        return None

    return DBSession.get(User, user_id)


# pylint: disable=unused-argument
def get_user_by_username(request, username, warn=True):
    """ Get user from database by username """

    if username is None:
        return None

    # Transform username to lowercase
    username = username.lower()

    user = DBSession.execute(sa.select(User).where(User.username == username).limit(1)).scalar()
    if user is None and warn:
        log.warning('Failed to get user with username=%s', username)

    return user


# pylint: disable=unused-argument
def get_username_by_token(request, token, warn=True):
    """ Get usermame from database by token """

    if token is None:
        return None

    username = DBSession.execute(sa.select(User.username).where(User.token == token).limit(1)).scalar()
    if username is None and warn:
        log.warning('Failed to get user from with token=%s', token)

    return username


def import_users(request, doc, asquery=True, columns=(), create_only=False, dry_run=False, version=EXP_VERSION):
    """ Import users from doc """

    # Get header
    if version not in EXP_COLUMNS:
        raise ValueError(f'Invalid export version {version}')

    if dry_run:
        sp = request.tm.savepoint()

    header = EXP_COLUMNS[version][:]
    lineno = 0

    create_ids = set()
    delete_ids = set()
    modify_ids = set()
    import_ids = set()
    for lineno, data in doc.dict_iterator(EXP_INTERFACE, version, header, mandatories=COLS_MANDATORY_SELECT):
        status = data['status'].lower()
        if status not in User.STATUSES:
            log.error('[%s:%06d] [SKIP   ] Invalid row status %s', doc.filename, lineno, status)
            continue

        # Get user
        user = get_user_by_username(request, data['username'], warn=False)

        if status == 'deleted':
            # DELETE
            if user is None:
                continue

            if create_only:
                log.info('[%s:%06d] [KEEP   ] Keeping existing user «%s» (create only)', doc.filename, lineno, user.fullname)
                continue

            log.info('[%s:%06d] [DELETE ] Deleting user «%s»', doc.filename, lineno, user.fullname)

            delete_ids.add(user.username)

            DBSession.delete(user)
            continue

        # CREATE/UPDATE
        if user is None:
            if columns:
                log.info('[%s:%06d] [SKIP   ] Skipping missing user (partial)', doc.filename, lineno)
                continue

            # Check mandatory columns for creation
            if not doc.check_mandatories(lineno, data, COLS_MANDATORY_CREATE):
                continue

        elif create_only:
            log.info('[%s:%06d] [SKIP   ] Skipping existing user «%s» (create only)', doc.filename, lineno, user.fullname)

            import_ids.add(user.id)
            continue

        if columns:
            # Keep only selected columns
            data = {
                column: value
                for column, value in data.items()
                if column in columns
            }

        # Convert some items to suitable format
        if 'profiles' in data:
            data['profiles'] = [
                profile.strip()
                for profile in data['profiles'].split(',')
                if profile.strip()
            ] if data['profiles'] else []

        # Update database
        if user is None:
            user = User()
            DBSession.add(user)

            action = 'create'
        else:
            action = 'modify'

        is_modified = user.from_dict(data)

        # Flush the session to get new user id
        DBSession.flush()

        if action == 'create':
            log.info('[%s:%06d] [CREATE ] Creating user «%s»', doc.filename, lineno, user.fullname)
            create_ids.add(user.id)

        elif is_modified:
            log.info('[%s:%06d] [UPDATE ] Updating user «%s»', doc.filename, lineno, user.fullname)
            modify_ids.add(user.id)

        import_ids.add(user.id)

    if dry_run:
        log.info('[%s:%06d] [ABORT  ] DRY RUN MODE', doc.filename, lineno)

        sp.rollback()

        create_ids = set()
        delete_ids = set()
        modify_ids = set()

    if not asquery:
        return {
            'create_ids': create_ids,
            'delete_ids': delete_ids,
            'modify_ids': modify_ids,
            'import_ids': import_ids,
        }

    return search_users(request, selected_ids=list(import_ids))


# pylint: disable=unused-argument
def search_users(request, sort='id', order='asc', **criteria):
    """ Build a search query """

    select = sa.select(User)

    # Filters
    if criteria.get('excluded_ids'):
        select = select.where(~User.id.in_(criteria['excluded_ids']))

    if criteria.get('selected_ids'):
        select = select.where(User.id.in_(criteria['selected_ids']))

    if criteria.get('profile'):
        select = select.where(User.profiles.any(criteria['profile']))
    elif criteria.get('profiles'):
        select = select.where(User.profiles.overlap(criteria['profiles']))

    if criteria.get('term'):
        if criteria.get('exact') is True:
            select = select.where(User.username == criteria['term'])
        else:
            sql_term = unidecode.unidecode(criteria['term']).replace(' ', '%')
            select = select.where(
                sa.or_(
                    User.email.ilike(f'%{sql_term}%'),
                    User.firstname.ilike(f'%{sql_term}%'),
                    User.lastname.ilike(f'%{sql_term}%'),
                    User.username.ilike(f'%{sql_term}%'),
                )
            )

    if criteria.get('status'):
        select = select.where(User.status == criteria['status'])
    elif criteria.get('statuses'):
        select = select.where(User.status.in_(criteria['statuses']))

    # Order
    if sort == 'id':
        order_by = [User.id]
    elif sort == 'email':
        order_by = [User.email]
    elif sort in ('firstname', 'fullname'):
        order_by = [User.firstname, User.lastname, User.username]
    elif sort == 'lastname':
        order_by = [User.lastname, User.firstname, User.username]
    elif sort == 'status':
        order_by = [User.status, User.lastname, User.firstname, User.username]
    elif sort == 'username':
        order_by = [User.username]
    else:
        order_by = []

    if order_by:
        sa_func = sa.asc if order == 'asc' else sa.desc
        select = select.order_by(*[sa_func(column) for column in order_by])

    return select


def select_users(request, doc, asquery=True, version=EXP_VERSION):
    """ Select objects from Doc """

    # Get header
    header = EXP_COLUMNS.get(version)
    if header is None:
        raise ValueError(f'Invalid export version {version}')

    selected_ids = set()
    for _, data in doc.dict_iterator(EXP_INTERFACE, version, header, mandatories=COLS_MANDATORY_SELECT):
        user_id = DBSession.execute(sa.select(User.id).where(User.username == data['username']).limit(1)).scalar()
        if user_id:
            selected_ids.add(user_id)

    if not asquery:
        return selected_ids

    return search_users(request, selected_ids=selected_ids)


# Register functions
register(
    'users',
    {
        'target': 'users',
        'title': N_('Users'),
        'description': N_('Import/export functions for users'),
        'icons': ['user', 'users'],
        'features': ['create', 'export', 'import', 'modify', 'search', 'select'],
        'model': User,
        'path': 'users',
        'prefix': 'USERS',
        'interface': EXP_INTERFACE,
        'version': EXP_VERSION,
        'columns': EXP_COLUMNS,
        'create_form': CreateForm,
        'create_func': create_or_modify,
        'export_func': export_users,
        'import_func': import_users,
        'import_options': {
            'create_only': True,
            'partial': COLS_PARTIAL_IMPORT,
        },
        'modify_func': create_or_modify,
        'search_form': SearchForm,
        'search_func': search_users,
        'select_func': select_users,
    }
)
