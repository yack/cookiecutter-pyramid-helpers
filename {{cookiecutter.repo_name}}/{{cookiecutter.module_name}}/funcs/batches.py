# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} batches functions """

from pyramid_helpers.utils import random_string


def get_batch(request, matched_ids=None, target=None, force=False, selected=False):
    """ Get batch object from path_info or create a new one """

    session = request.session
    batches = session.setdefault('batches', {})

    # Ensure that the session will be saved
    session.save()

    if target is None:
        batch_id = request.matchdict.get('batch')
        return batches.get(batch_id)

    if matched_ids is None:
        matched_ids = set()

    batch_key = '[batch] {}{}::{}'.format(
        request.matched_route.name,
        '({})'.format(
            ', '.join(
                f'{k}={request.matchdict[k]}'
                for k in sorted(request.matchdict.keys())
            )
        ) if request.matchdict else '',
        target,
    )
    batch_id = session.get(batch_key)
    if 'search' in request.params or batch_id is None or force:
        # New batch
        batch_id = random_string(32)

        # Store matched ids as selected ids
        batch = {
            'id': batch_id,
            'target': target,
            'selected_ids': matched_ids.copy() if selected else set(),
        }

        # Store current batch id to session
        session[batch_key] = batch_id

        # Store batch to session
        batches[batch_id] = batch

    else:
        # Get batch from session
        batch = batches[batch_id]

    # Store matched_ids
    batch['matched_ids'] = matched_ids

    # Ensure that selected_ids is a subset of matched_ids
    batch['selected_ids'] = batch['selected_ids'].intersection(matched_ids)

    return batch
