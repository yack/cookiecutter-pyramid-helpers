# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} functions for searches management """

from formencode.variabledecode import variable_decode

import sqlalchemy as sa

from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import Search
from {{ cookiecutter.module_name }}.models.users import User


def create_or_modify(request, form, search=None):
    """ Create or modify a search """

    session = request.session
    translate = request.translate

    # Get batch is requested
    if form.result['batch']:
        request.matchdict['batch'] = form.result['batch']
        batch = get_batch(request)
        if batch is None:
            form.errors['batch'] = translate('Invalid value')
            return None
    else:
        batch = None

    data = {
        'description': form.result['description'],
        'label': form.result['label'],
    }

    if search is None:
        search = Search()
        DBSession.add(search)

        target = request.matchdict.get('target')

        data.update({
            'target': target,
            'user': request.authenticated_user,
        })
    else:
        target = search.target

    if form.result['shared'] and request.has_permission('searches.advanced'):
        data['shared'] = True

    if batch is None:
        # Get search parameters from session
        session_key = f'[form] {target}.search::search'
        form_params = variable_decode(session.get(session_key, {}))

        data['parameters'] = {
            key: value
            for key, value in form_params.items()
            if key not in (f'{target}.limit', f'{target}.page', f'{target}.partial')
        }
    else:
        # Get search parameters from batch
        data['parameters'] = {
            'selected_ids': list(batch['selected_ids'])
        }

    # Update object
    search.from_dict(data)

    # Flush the session to get new search id
    DBSession.flush()

    if batch is None:
        # Store search id to session
        session_key = f'[form] {target}.search::search_id'
        session[session_key] = search.id
    else:
        # Store search id to batch
        batch['search_id'] = search.id

    return search


def get_search(request):
    """ Get search object from path_info """

    search_id = request.matchdict.get('search')
    if search_id is None:
        return None

    return DBSession.get(Search, search_id)


# pylint: disable=unused-argument
def get_search_by_label(request, target, label, username=None):
    """ Get a search by label """

    select = sa.select(Search)
    select = select.join(User, Search.user)
    select = select.where(
        sa.and_(
            Search.label == label,
            Search.target == target,
        )
    )

    if username is None:
        select = select.where(Search.shared.is_(True))
    else:
        select = select.where(
            sa.or_(
                Search.shared.is_(True),
                User.username == username,
            )
        )

    return DBSession.execute(select.limit(1)).scalar()
