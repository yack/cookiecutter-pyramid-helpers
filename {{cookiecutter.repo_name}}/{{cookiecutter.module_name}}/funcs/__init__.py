# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} functions """

from configparser import ConfigParser
import csv
import datetime
import fcntl
import importlib
import io
import json
import logging
import os

from openpyxl import Workbook
from openpyxl import load_workbook

from pyramid_helpers.utils import ConfigurationError


log = logging.getLogger(__name__)

CONFIGS = {}


class RowDoc:
    """ Base class for export document """

    prefix = None
    sheets = False
    dirpath = None

    def __init__(self, file_, *args, **kwargs):
        self.file = file_
        self.args = args
        self.kwargs = kwargs
        self.rows = []

        self.mode = kwargs.pop('mode', 'r')
        if self.mode not in ('r', 'w', '+'):
            raise ValueError(f'Invalid mode: {self.mode}')

    def __contains__(self, row):
        return row in self.rows

    def __enter__(self):
        if self.mode not in ('r', '+'):
            return self

        self.load()
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.mode not in ('w', '+'):
            return

        self.save()

    def __getitem__(self, x):
        return self.rows[x]

    def __getslice__(self, x, y):
        return self.rows[x:y]

    def __iter__(self):
        yield from self.rows

    def __len__(self):
        return len(self.rows)

    def __setitem__(self, x, row):
        self.rows[x] = row

    def __setslice__(self, x, y, rows):
        self.rows[x:y] = rows

    @property
    def filename(self):
        """ Name of opened file """

        return os.path.basename(self.filepath)

    @property
    def filepath(self):
        """ Path of opened file """

        return str(self.file.name)

    def append(self, row):
        """ Append a row to the document """

        self.rows.append(row)

    def append_dict(self, data, interface, version, header):
        """ Append a dict row to the document """

        def get_value(data, column):
            if column == 'reserved' or not data:
                return None

            if column in data:
                return sanitize_value(data[column])

            if '.' in column:
                prefix, column = column.split('.', 1)
                return get_value(data.get(prefix), column)

            return None

        def sanitize_value(value, default=None):
            if value is None:
                return None

            if isinstance(value, bool):
                return 'X' if value else ''

            if isinstance(value, dict):
                return json.dumps(value, default=str)

            if isinstance(value, list):
                return ','.join(
                    sanitize_value(value, default=str) or ''
                    for value in value
                )

            if default:
                return default(value)

            return value

        data.setdefault('interface', interface)
        data.setdefault('version', version)

        # Create row
        row = [None for _ in range(len(header))]

        for index, column in enumerate(header):
            row[index] = get_value(data, column)

        self.append(row)

    def check_mandatories(self, lineno, data, mandatories):
        """ Check mandatory columns """

        for column in mandatories:
            if not data.get(column):
                log.error('[%s:%06d] [SKIP   ] Missing value for column %s', self.filename, lineno, column)
                return False

        return True

    def clear(self):
        """ Clear document """

        self.rows = []

    def dict_iterator(self, interface, version, header, mandatories=()):
        """ Iterator which yields filtered rows as dictionaries """

        def sanitize_value(value):
            if isinstance(value, (float, int)):
                return value

            if isinstance(value, str):
                value = value.strip()

            if not value:
                return None

            return value

        col_interface = header.index('interface')
        col_version = header.index('version')
        col_min = max(col_interface, col_version)

        count = 0
        for row_index, row in enumerate(self):
            count += 1
            length = len(row)
            lineno = row_index + 1

            if count > 100:
                # Stopping here
                log.warning('[%s:%06d] [INVALID] Too many consecutive invalid lines were detected, aborting', self.filename, lineno)
                break

            if not row or length <= col_min:
                continue

            if row[col_interface] != interface:
                # Wrong interface
                continue

            if row[col_version] != version:
                # Invalid version
                log.error('[%s:%06d] [INVALID] Interface version does not match (%s instead of %s)', self.filename, lineno, row[col_version], version)
                continue

            count = 0

            # Convert row to dictionary
            data = {
                column: sanitize_value(row[col_index])
                for col_index, column in enumerate(header[:length])
                if column not in ('interface', 'reserved', 'version')
            }

            # Check mandatory columns
            if not self.check_mandatories(lineno, data, mandatories):
                continue

            # Data is ready to use
            yield lineno, data

    def extend(self, rows):
        """ Extend document with rows """

        self.rows.extend(rows)

    def index(self, row):
        """ Get index of a row """

        self.rows.index(row)

    def insert(self, index, row):
        """ Insert row before index """

        self.rows.insert(index, row)

    def load(self, **kwargs):
        """ Load rows from file """

        raise NotImplementedError()

    def pop(self):
        """ Remove and return row at index (default last) """

        self.rows.pop()

    def remove(self, row):
        """ Remove first occurrence of row """

        self.rows.remove(row)

    def reverse(self):
        """ Reverse *IN PLACE* """

        self.rows.reverse()

    def save(self, **kwargs):
        """ Save rows to file """

        raise NotImplementedError()

    def sort(self, *args, **kwargs):
        """ Sort *IN PLACE* """

        self.rows.sort(*args, **kwargs)


class CSVDoc(RowDoc):
    """ CSV export document """

    def load(self, **kwargs):
        """ Load rows from file """

        self.file.seek(0)

        reader = csv.reader(self.file, *self.args, **self.kwargs)

        self.rows = list(reader)

    def save(self, **kwargs):
        """ Save rows to file """

        self.file.seek(0)
        self.file.truncate()

        writer = csv.writer(self.file, *self.args, **self.kwargs)
        writer.writerows(self.rows)

        self.file.seek(0)


class XLSXDoc(RowDoc):
    """ XLSX/XLSM export document """

    sheets = True

    def __init__(self, fp, *args, **kwargs):

        self.config = kwargs.pop('config', None)
        self.title = kwargs.pop('title', None)

        if self.title is None and self.config:
            self.title = self.config.get('sheet')

        self.__wb = None

        # Calling inherited
        super().__init__(fp, *args, **kwargs)

    def __get_title(self, sheet):
        """ Get sheet tile """

        if sheet and self.config:
            return self.config.get(f'{sheet}.sheet')

        return self.title

    def load(self, **kwargs):
        """ Load rows from file """

        sheet = kwargs.get('sheet')
        title = self.__get_title(sheet)

        if self.__wb is None:
            self.file.seek(0)
            self.__wb = load_workbook(self.file, *self.args, **self.kwargs)

        if title:
            if title not in self.__wb:
                self.rows = []
                return

            ws = self.__wb[title]
        else:
            ws = self.__wb.active

        self.rows = [
            [cell.value for cell in row]
            for row in ws.rows
        ]

    def save(self, **kwargs):
        """ Save rows to file """

        sheet = kwargs.get('sheet')
        title = self.__get_title(sheet)

        if title is None:
            raise ValueError('Title is undefined')

        if self.__wb is None:
            self.__wb = Workbook(*self.args, **self.kwargs)

        if self.config:
            if title in self.__wb:
                ws = self.__wb[title]
            else:
                ws = self.__wb.create_sheet(title)

            start_col = int(self.config.get('{}col'.format(f'{sheet}.' if sheet else ''), 1))
            start_row = int(self.config.get('{}row'.format(f'{sheet}.' if sheet else ''), 1))
            for i, row in enumerate(self.rows):
                for j, value in enumerate(row):
                    ws.cell(column=start_col + j, row=start_row + i, value=value)
        else:
            ws = self.__wb.active
            ws.title = title

            for row in self.rows:
                ws.append(row)

        if sheet is None:
            # Erase file content
            self.file.seek(0)
            self.file.truncate()

            self.__wb.save(self.file)

            # Rewind file
            self.file.seek(0)

        self.clear()


def backup_file(request, doc, output_format, output_fp, output_target):
    """ Wrapper function for backup """

    def copy(source, doc, **_):
        doc.clear()
        for row in source:
            doc.append(row)

    return export_file(request, output_target, copy, doc, format=output_format, fp=output_fp)


def export_file(request, target, func, source, **kwargs):
    """ Wrapper function for export """

    # Extract parameters
    format_ = kwargs.pop('format', 'xlsx')
    if format_ not in ('csv', 'xlsx'):
        raise ValueError(f'Invalid export format: {format_}')

    fp = kwargs.pop('fp', None)
    if fp is not None and not isinstance(fp, (io.BytesIO, io.StringIO)):
        # Lock the file to prevent concurrent write access
        try:
            fcntl.flock(fp.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except OSError:
            log.error('[Export] Failed to lock file %s, aborting.', fp.name)
            return None, None, None

    suffix = kwargs.pop('suffix', None)
    if suffix is None:
        today = datetime.date.today()
        suffix = '{}-01'.format(today.strftime('%Y%m%d'))

    if format_ == 'csv':
        if fp is None:
            fp = io.StringIO()

        content_type = 'text/csv; charset="utf-8"'
        doc = CSVDoc(fp, delimiter=';', mode='w')
        add_header = True

    else:
        if fp is None:
            fp = io.BytesIO()

        content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset="utf-8"'
        config = get_config(target)
        if config is not None and config.get('template'):
            with open(config['template'], 'rb') as fpt:
                fp.write(fpt.read())

            if os.path.splitext(config['template'])[1] == '.xlsm':
                format_ = 'xlsm'

            doc = XLSXDoc(fp, mode='+', config=config, keep_vba=format_ == 'xlsm')
            add_header = False
        else:
            doc = XLSXDoc(fp, mode='w', title=target.capitalize())
            add_header = True

    with doc:
        func(request, source, doc, add_header=add_header, **kwargs)

    filename = f'{doc.prefix}-{target.upper()}-{suffix}.{format_}'

    return fp, content_type, filename


def get_configs(target=None, feature=None):
    """ Get configs """

    configs = []
    for id_, config in CONFIGS.items():
        if config.get('setup') is not True:
            continue

        if target and config['target'] != target:
            continue

        if feature and feature not in config['features']:
            continue

        configs.append((id_, config))

    def sort_func(item):
        id_, config = item
        return (0 if id_ == config['target'] else 1, config.get('rank', 999), config['title'])

    return sorted(configs, key=sort_func)


def get_config(id_):
    """ Get config by id """

    if id_ not in CONFIGS:
        # Load module to register config
        module_name = id_.split('.')[0]
        importlib.import_module(f'{__name__}.{module_name}')

    return CONFIGS.get(id_)


def load_config(dirpath):
    """ Load config from dirapth """

    filepath = os.path.join(dirpath, 'config.ini')
    if not os.access(filepath, os.F_OK):
        raise ConfigurationError(f'The config file {filepath} is missing')

    if not os.access(filepath, os.R_OK):
        raise ConfigurationError(f'The config file {filepath} is not readable')

    parser = ConfigParser(defaults={'here': dirpath})
    parser.read(filepath)

    for id_ in parser.sections():
        options = parser.options(id_)
        if not options:
            log.warning('[Export] The section «%s» is empty in config file %s', id_, filepath)
            continue

        if 'template' not in options:
            log.warning('[Export] The option «template» of section «%s» is missing in config file %s', id_, filepath)

        config = {}
        for option in options:
            value = parser.get(id_, option)

            if option == 'template':
                value = os.path.join(dirpath, value)
                if not os.access(value, os.F_OK) or not os.access(value, os.R_OK):
                    # template file doesn't exist or is not readable
                    log.warning('[Export] Template file defined in section «%s» of config file %s does not exist or is not readable', id_, filepath)
                    continue

            config[option] = value

        # Update config with configuration from file
        CONFIGS.setdefault(id_, {})
        CONFIGS[id_].update(config)


def import_file(request, target, func, fp, **kwargs):
    """ Wrapper function for import """

    # Extract parameters
    format_ = kwargs.pop('format', 'xlsx')
    if format_ not in ('csv', 'xlsx'):
        raise ValueError(f'Invalid import format: {format_}')

    if not isinstance(fp, (io.BytesIO, io.StringIO)):
        # Lock the file to prevent concurrent write access
        try:
            fcntl.flock(fp.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except OSError:
            log.error('[Import] Failed to lock file %s, aborting.', fp.name)
            return False

    title = kwargs.pop('sheet', None)

    if format_ == 'csv':
        doc = CSVDoc(fp, delimiter=';', mode='r')
    else:
        config = get_config(target)
        if title is None:
            if config is not None:
                title = config.get('sheet', target.capitalize())
            else:
                title = target.capitalize()

        doc = XLSXDoc(fp, mode='r', title=title, config=config, data_only=True)

    with doc:
        return func(request, doc, **kwargs)


def register(id_, config):
    """ Register a module """

    if id_ in CONFIGS:
        if CONFIGS[id_].get('setup') is True:
            log.error('Duplicate module id: %s', id_)
            return

        # Update config with configuration from file
        config.update(CONFIGS[id_])
    else:
        # Set defaults
        config.setdefault('features', [])
        config.setdefault('requires', [])
        config.setdefault('target', None)

        for feature in config['features']:
            config.setdefault(f'{feature}_options', {})

    config['setup'] = True

    CONFIGS[id_] = config


def includeme(config):
    """
    Set up standard configurator registrations. Use via:

    .. code-block:: python

    config = Configurator()
    config.include('{{ cookiecutter.module_name }}.funcs')
    """

    registry = config.registry
    settings = registry.settings

    log.debug('Initializing functions...')

    # Set filename prefix
    RowDoc.prefix = settings['app.name'].upper()

    # Scan to register functions
    config.scan('.')

    # Set dirpath to RowDoc class
    RowDoc.dirpath = config.get_dirpath('imports')

    # Load configuration from exports directory
    dirpath = config.get_dirpath('exports')
    if dirpath:
        load_config(dirpath)

    # Final checks
    for config_ in CONFIGS.values():
        if RowDoc.dirpath:
            features = config_.get('features')
            if features and 'import' in features:
                if config_.get('path'):
                    config_['dirpath'] = os.path.join(RowDoc.dirpath, config_['path'])

                    os.makedirs(config_['dirpath'], exist_ok=True)
                else:
                    config_['dirpath'] = RowDoc.dirpath
