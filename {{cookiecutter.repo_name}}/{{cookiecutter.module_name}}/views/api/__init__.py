# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Internal API """

import logging

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.view import view_config

from pyramid_helpers.forms import validate

from {{ cookiecutter.module_name }}.forms.gui import GuiSettingsForm


log = logging.getLogger(__name__)


@view_config(route_name='api.gui', renderer='json')
@validate('settings', GuiSettingsForm)
def gui(request):
    """
    Save GUI settings
    """
    form = request.forms['settings']
    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    session = request.session

    settings = session.get('gui_settings', {})
    for name, value in form.result.items():
        if value is not None:
            settings[name] = value

    session['gui_settings'] = settings
    session.save()

    return {
        'apiVersion': '1.0',
        'method': request.method,
        'params': form.params,
        'result': True,
        'url': request.path_qs,
    }
