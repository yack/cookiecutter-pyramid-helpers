# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Users API """

import logging

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from {{ cookiecutter.module_name }}.forms.users import CreateForm
from {{ cookiecutter.module_name }}.forms.users import SearchForm
from {{ cookiecutter.module_name }}.forms.users import StatusForm
from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs.users import get_user
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User

log = logging.getLogger(__name__)


@view_config(route_name='api.users.create', renderer='json', permission='users.create')
@validate('user', CreateForm)
def create(request):
    """
    Creates an user
    """
    form = request.forms['user']

    config = get_config('users')
    create_func = config['create_func']

    user = create_func(request, form)
    if user is None:
        raise HTTPBadRequest(json=form.errors)

    request.response.status_int = 201

    return {
        'url': request.path_qs,
        'method': request.method,
        'params': form.params,
        'apiVersion': '1.0',
        'result': True,
        'user': user.to_dict(context='create'),
    }


@view_config(route_name='api.users.delete', renderer='json', permission='users.delete')
def delete(request):
    """
    Deletes an user
    """
    translate = request.translate

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(detail=translate('Invalid user'))

    if user == request.authenticated_user:
        raise HTTPBadRequest(detail=translate('Invalid user'))

    DBSession.delete(user)

    return {
        'url': request.path_qs,
        'method': request.method,
        'apiVersion': '1.0',
        'result': True,
    }


@view_config(route_name='api.users.modify', renderer='json', permission='users.modify')
@validate('user', CreateForm)
def modify(request):
    """
    Modifies an user
    """
    translate = request.translate
    form = request.forms['user']

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(detail=translate('Invalid user'))

    config = get_config('users')
    modify_func = config['modify_func']

    if not modify_func(request, form, user):
        raise HTTPBadRequest(json=form.errors)

    return {
        'url': request.path_qs,
        'method': request.method,
        'params': form.params,
        'apiVersion': '1.0',
        'result': True,
        'user': user.to_dict(context='modify'),
    }


@view_config(route_name='api.users.search', renderer='json', permission='users.search')
@paginate('users', limit=10, sort='id', order='asc')
@validate('search', SearchForm, method='get')
def search(request):
    """
    Searches in users
    """
    form = request.forms['search']
    pager = request.pagers['users']

    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    config = get_config('users')
    search_func = config['search_func']

    select = search_func(request, order=pager.order, sort=pager.sort, **form.result)

    def get_items(offset, limit, select=select):
        return DBSession.execute(select.offset(offset).limit(limit))

    # pylint: disable=not-callable
    count = DBSession.execute(select.order_by(None).with_only_columns(sa.func.count(User.id))).scalar()
    pager.set_collection(count=count, items=get_items)

    return {
        'url': request.path_qs,
        'method': request.method,
        'params': form.params,
        'apiVersion': '1.0',
        'result': True,
        'users': {
            'items': [user.to_dict(context='search') for user in pager],
            'pager': pager.to_dict(),
        }
    }


@view_config(route_name='api.users.status', renderer='json', permission='users.modify')
@validate('user', StatusForm)
def status(request):
    """
    Changes user's status
    """
    translate = request.translate
    form = request.forms['user']

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(detail=translate('Invalid user'))

    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    user.from_dict(form.result)

    return {
        'url': request.path_qs,
        'method': request.method,
        'params': form.params,
        'apiVersion': '1.0',
        'result': True,
        'user': user.to_dict(context='modify'),
    }


@view_config(route_name='api.users.visual', renderer='json', permission='users.visual')
def visual(request):
    """
    Gets an user
    """
    translate = request.translate

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(detail=translate('Invalid user'))

    return {
        'url': request.path_qs,
        'method': request.method,
        'apiVersion': '1.0',
        'result': True,
        'user': user.to_dict(context='visual'),
    }
