# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Searches API """

import logging

import sqlalchemy as sa

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPNotFound
from pyramid.renderers import render
from pyramid.view import view_config

from pyramid_helpers.forms import validate

from {{ cookiecutter.module_name }}.forms.searches import CreateForm
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.funcs.searches import create_or_modify
from {{ cookiecutter.module_name }}.funcs.searches import get_search
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import Search

log = logging.getLogger(__name__)


@view_config(route_name='api.searches.create', renderer='json', permission='searches.create')
@validate('search', CreateForm)
def create(request):
    """
    Creates a Search
    """
    translate = request.translate
    form = request.forms['search']

    target = request.matchdict.get('target')

    if not form.errors:
        # Check label unicity
        # label must be unique if shared = True
        # (label, user_id) must be unique if shared = False
        if form.result['shared']:
            same_search = DBSession.execute(sa.select(Search).where(
                sa.and_(
                    Search.label == form.result['label'],
                    Search.target == target,
                )
            ).limit(1)).scalar()
        else:
            same_search = DBSession.execute(sa.select(Search).where(
                sa.and_(
                    Search.label == form.result['label'],
                    Search.target == target,
                    sa.or_(
                        Search.shared.is_(True),
                        Search.user == request.authenticated_user,
                    )
                )
            ).limit(1)).scalar()
        if same_search:
            form.errors['label'] = translate('Label already used for another search')

    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    search = create_or_modify(request, form)
    if search is None:
        raise HTTPBadRequest(json=form.errors)

    request.response.status_int = 201

    return {
        'apiVersion': '1.0',
        'message': translate('Search has been successfully saved'),
        'method': request.method,
        'result': True,
        'search': search.to_dict(context='create'),
        'url': request.path_qs,
    }


@view_config(route_name='api.searches.delete', renderer='json', permission='searches.delete')
def delete(request):
    """
    Removes a Search
    """
    translate = request.translate

    search = get_search(request)
    if search is None:
        raise HTTPNotFound(detail=translate('Invalid search id'))

    if search.user != request.authenticated_user:
        raise HTTPForbidden(explanation=translate('Search #{0} is owned by another user').format(search.id))

    # Delete the search
    DBSession.delete(search)

    return {
        'url': request.path_qs,
        'method': request.method,
        'apiVersion': '1.0',
        'result': True,
        'message': translate('Search has been successfully deleted'),
    }


@view_config(route_name='api.searches.leave', renderer='json', permission='searches.visual')
def leave(request):
    """
    Leaves a Search
    """
    session = request.session
    translate = request.translate

    target = request.matchdict.get('target')

    result = {
        'url': request.path_qs,
        'method': request.method,
        'apiVersion': '1.0',
        'result': True,
    }

    if len(target) == 32:
        # Batch
        request.matchdict['batch'] = target
        batch = get_batch(request)
        if batch is None or 'search_id' not in batch:
            result['message'] = translate('Failed to leave search')
            result['result'] = False
            return result

        batch.pop('search_id')
    else:
        # Form
        session_key = f'[form] {target}.search::search_id'
        if session_key not in session:
            result['message'] = translate('Failed to leave search')
            result['result'] = False
            return result

        session.pop(session_key)

    result['message'] = translate('Search has been successfully leaved')
    return result


@view_config(route_name='api.searches.menu', renderer='json', permission='searches.visual')
def menu(request):
    """
    Generates search menu
    """

    return {
        'url': request.path_qs,
        'method': request.method,
        'apiVersion': '1.0',
        'result': True,
        'data': render('/searches/menu.mako', {}, request),
    }


@view_config(route_name='api.searches.modify', renderer='json', permission='searches.modify')
@validate('search', CreateForm)
def modify(request):
    """
    Modifies a Search
    """
    translate = request.translate
    form = request.forms['search']

    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    search = get_search(request)
    if search is None:
        raise HTTPNotFound(detail=translate('Invalid search id'))

    if search.user != request.authenticated_user:
        raise HTTPForbidden(explanation=translate('Search #{0} is owned by another user').format(search.id))

    if not create_or_modify(request, form, search=search):
        raise HTTPBadRequest(json=form.errors)

    return {
        'apiVersion': '1.0',
        'message': translate('Search has been successfully modified'),
        'method': request.method,
        'result': True,
        'search': search.to_dict(context='modify'),
        'url': request.path_qs,
    }
