# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} views for users management """

from pyramid.httpexceptions import HTTPNotFound
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

import sqlalchemy as sa

from pyramid_helpers.forms import validate
from pyramid_helpers.paginate import paginate

from {{ cookiecutter.module_name }}.forms.users import CreateForm
from {{ cookiecutter.module_name }}.forms.users import SearchForm
from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.funcs.users import get_user

from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import User


@paginate('users', limit=10, sort='label', order='desc', partial_template='/users/list.mako')
def batches(request, batch):
    """ Selection page for users batches """

    pluralize = request.pluralize
    session = request.session
    translate = request.translate
    pager = request.pagers['users']

    cancel_link = request.route_path('users.search')

    breadcrumb = [
        (translate('Users'), request.route_path('users.search')),
        (translate('Batch'), request.route_path('batches.index', batch=batch['id'])),
    ]

    # Check selection
    if not batch['selected_ids']:
        # Flash message
        session.flash({
            'message': translate('No users selected'),
            'type': 'error',
        })
        return HTTPFound(location=cancel_link)

    # Build query
    config = get_config('users')
    search_func = config['search_func']

    select = search_func(request, order=pager.order, sort=pager.sort, selected_ids=batch['selected_ids'])

    def get_items(offset, limit, select=select):
        return DBSession.execute(select.offset(offset).limit(limit)).scalars()

    # pylint: disable=not-callable
    count = DBSession.execute(select.order_by(None).with_only_columns(sa.func.count(User.id))).scalar()
    pager.set_collection(count=count, items=get_items)

    message = pluralize(
        'Please select a batch process below to apply to <b>{0}</b> selected user.',
        'Please select a batch process below to apply to <b>{0}</b> selected users.',
        pager.total,
    ).format(pager.total)

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': cancel_link,
        'message': message,
        'partial_template': '/users/list.mako',
        'title': translate('Users'),
    }


@view_config(route_name='users.create', renderer='/users/modify.mako', permission='users.create')
@validate('user', CreateForm)
def create(request):
    """ Creates a user """

    session = request.session
    translate = request.translate
    form = request.forms['user']

    if request.method == 'POST':
        config = get_config('users')
        create_func = config['create_func']

        user = create_func(request, form)
        if user:
            # Flash message
            session.flash({
                'message': translate('Successfully created user «{0}»').format(user.fullname),
                'type': 'success',
            })

            return HTTPFound(location=request.route_path('users.visual', user=user.id))

        # Flash message
        session.flash({
            'message': translate('Failed to create user'),
            'type': 'error',
        })

    breadcrumb = [
        (translate('Users'), request.route_path('users.search')),
        (translate('New user'), request.route_path('users.create'))
    ]
    title = translate('Users')
    cancel_link = request.route_path('users.search')

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': cancel_link,
        'profiles': User.PROFILES,
        'statuses': User.STATUSES,
        'subtitle': translate('Create a new user'),
        'title': title,
        'user': None,
    }


@view_config(route_name='users.modify', renderer='/users/modify.mako', permission='users.modify')
@validate('user', CreateForm)
def modify(request):
    """ Modifies a user """

    session = request.session
    translate = request.translate
    form = request.forms['user']

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(explanation=translate('Invalid user'))

    if request.method == 'POST':
        config = get_config('users')
        modify_func = config['modify_func']

        if modify_func(request, form, user):
            # Flash message
            session.flash({
                'message': translate('Successfully modified user «{0}»').format(user.fullname),
                'type': 'success',
            })

            return HTTPFound(location=request.route_url('users.visual', user=user.id))

        # Flash message
        session.flash({
            'message': translate('Failed to modify user «{0}»').format(user.fullname),
            'type': 'error',
        })
    else:
        data = user.to_dict()
        form.from_python(data, validate=False)

    breadcrumb = [
        (translate('Users'), request.route_path('users.search')),
        (user.fullname, request.route_path('users.visual', user=user.id)),
        (translate('Edition'), None),
    ]
    cancel_link = request.route_url('users.visual', user=user.id)

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': cancel_link,
        'profiles': User.PROFILES,
        'statuses': User.STATUSES,
        'subtitle': translate('User «{0}» edition').format(user.fullname),
        'title': translate('Users'),
        'user': user,
    }


@view_config(route_name='users.search', renderer='/users/search.mako', permission='users.search')
@paginate('users', form='search', limit=10, sort='username', order='asc', partial_template='/users/list.mako', volatile_items=('format', 'search'))
@validate('search', SearchForm, method='get', persistent=True, volatile_items=('format', 'search'))
def search(request):
    """ Search for users """

    translate = request.translate
    form = request.forms['search']
    pager = request.pagers['users']

    batch = None

    if not form.errors:
        # Build query
        config = get_config('users')
        search_func = config['search_func']

        select = search_func(request, order=pager.order, sort=pager.sort, **form.result)

        def get_items(offset, limit, select=select):
            return DBSession.execute(select.offset(offset).limit(limit)).scalars()

        # pylint: disable=not-callable
        count = DBSession.execute(select.order_by(None).with_only_columns(sa.func.count(User.id))).scalar()
        pager.set_collection(count=count, items=get_items)

        if request.has_permission('batches.users'):
            matched_ids = set(DBSession.execute(select.with_only_columns(User.id)).scalars())
            batch = get_batch(request, matched_ids=matched_ids, target='users')

    if pager.partial:
        return {
            'batch': batch,
            'show_batch': bool(batch),
        }

    breadcrumb = [
        (translate('Users'), None),
    ]

    return {
        'batch': batch,
        'breadcrumb': breadcrumb,
        'profiles': User.PROFILES,
        'show_batch': bool(batch),
        'statuses': User.STATUSES,
        'subtitle': translate('Find users'),
        'title': translate('Users'),
    }


@view_config(route_name='users.visual', renderer='/users/visual.mako', permission='users.visual')
def visual(request):
    """ Visualizes a user """

    translate = request.translate

    user = get_user(request)
    if user is None:
        raise HTTPNotFound(explanation=translate('Invalid user id'))

    breadcrumb = [
        (translate('Users'), request.route_path('users.search')),
        (user.fullname, request.route_path('users.visual', user=user.id)),
    ]

    return {
        'breadcrumb': breadcrumb,
        'subtitle': translate('User «{0}»').format(user.fullname),
        'title': translate('Users'),
        'user': user,
    }
