# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} batches views """

from copy import deepcopy
import importlib

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config

from pyramid_helpers.forms import validate
from pyramid_helpers.utils import random_string

from {{ cookiecutter.module_name }}.forms.batches import BatchSelectionForm
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.models.users import Search
from {{ cookiecutter.module_name }}.plugins import get_plugins
from {{ cookiecutter.module_name }}.views.common import ensure_permission


def render_select_message(request, batch):
    """ Render select message from batch """

    pluralize = request.pluralize
    translate = request.translate

    selected_count = len(batch['selected_ids'])
    matched_count = len(batch['matched_ids'])

    target = batch['target']

    if target == 'users':
        if selected_count > 1 and selected_count == matched_count:
            message = translate('All matching users are selected ({0})').format(selected_count)
        else:
            message = pluralize('{0} user selected', '{0} users selected', selected_count).format(selected_count)
    else:
        raise ValueError(f'Invalid target: {target}')

    return message


@view_config(route_name='batches.index', renderer='/batches.mako', permission='batches')
def index(request):
    """ Batches index """

    session = request.session
    translate = request.translate

    batch = get_batch(request)
    if batch is None:
        raise HTTPNotFound(detail=translate('Invalid batch id'))

    target = batch['target']

    ensure_permission(request, f'batches.{target}')

    if batch.get('started') is None:
        batch_id = random_string(32)

        # Copy batch
        batch = deepcopy(batch)
        batch['id'] = batch_id
        batch['started'] = True

        # Save batch to session
        session['batches'][batch_id] = batch

        # Ensure that the session will be saved
        session.save()

        # Redirect to new batch
        return HTTPFound(location=request.route_path('batches.index', batch=batch_id))

    try:
        module = importlib.import_module(f'{{ cookiecutter.module_name }}.views.{target}')
    except ModuleNotFoundError as exc:
        raise HTTPNotFound(detail=translate('Unconfigured batch target')) from exc

    result = module.batches(request, batch)
    if isinstance(result, Response):
        return result

    # Add batches from plugins
    def route_path(plugin, request=request, batch=batch):
        return request.route_path('batches.plugins', target=plugin.__target__, batch=batch['id'], plugin=plugin.__id__)

    batches = {}
    plugins = get_plugins(target)
    for section, plugins_ in plugins.items():
        _section = translate(section)
        if _section not in batches:
            batches[_section] = []

        for plugin in plugins_:
            if plugin.__permission__ and not request.has_permission(plugin.__permission__):
                continue

            batches[_section].append({
                'name': translate(plugin.__name__),
                'desc': translate(plugin.__desc__),
                'icon': plugin.__icon__,
                'url': route_path(plugin),
            })

    # Get associated search
    search = DBSession.get(Search, batch['search_id']) if 'search_id' in batch else None

    # Add common data
    result.update({
        'batch': batch,
        'batches': batches,
        'target': target,
        'search': search,
        'subtitle': translate('Batch'),
    })

    return result


@view_config(route_name='batches.selection', renderer='json', permission='batches')
@validate('selection', BatchSelectionForm)
def selection(request):
    """
    Manage batch selection
    """
    translate = request.translate
    form = request.forms['selection']

    # Retrieve batch from session
    batch = get_batch(request)
    if batch is None:
        raise HTTPNotFound(detail=translate('Invalid batch id'))

    if form.errors:
        raise HTTPBadRequest(json=form.errors)

    action = None

    # Toggle selection
    if form.result['select_all']:
        # All
        selected_count = len(batch['selected_ids'])
        matched_count = len(batch['matched_ids'])
        if selected_count == matched_count:
            # None selected
            batch['selected_ids'] = set()
            action = 'unselect'
        else:
            # All selected
            batch['selected_ids'] = batch['matched_ids'].copy()
            action = 'select'

    elif form.result['select_list']:
        # List
        selected_ids = batch['matched_ids'].intersection(form.result['select_list'])
        if len(batch['selected_ids'].intersection(selected_ids)) < len(selected_ids):
            batch['selected_ids'].update(selected_ids)
            action = 'select'
        else:
            batch['selected_ids'].difference_update(selected_ids)
            action = 'unselect'

    elif form.result['select_none']:
        # None
        batch['selected_ids'] = set()
        action = 'unselect'

    elif form.result['select_one']:
        # One
        selected_id = form.result['select_one']
        if selected_id in batch['matched_ids']:
            if selected_id not in batch['selected_ids']:
                batch['selected_ids'].add(selected_id)
                action = 'select'
            else:
                batch['selected_ids'].remove(selected_id)
                action = 'unselect'

    selected_count = len(batch['selected_ids'])
    matched_count = len(batch['matched_ids'])

    return {
        'action': action,
        'apiVersion': '1.0',
        'matched_count': matched_count,
        'message': render_select_message(request, batch),
        'method': request.method,
        'result': True,
        'selected_count': selected_count,
        'url': request.path_qs,
    }
