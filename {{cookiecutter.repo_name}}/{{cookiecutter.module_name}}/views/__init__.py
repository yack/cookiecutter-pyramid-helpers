# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" {{ cookiecutter.project_name }} common views """

import datetime
import mimetypes
import os.path
import tempfile

from pyramid.httpexceptions import HTTPBadRequest
from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPInternalServerError
from pyramid.httpexceptions import HTTPNotFound
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.security import forget
from pyramid.security import remember
from pyramid.view import exception_view_config
from pyramid.view import view_config

from pyramid_helpers.api_doc import api_doc as api_doc_
from pyramid_helpers.auth import check_credentials
from pyramid_helpers.forms import validate
from pyramid_helpers.i18n import N_
from pyramid_helpers.utils import get_settings

from {{ cookiecutter.module_name }}.forms.auth import SignInForm
from {{ cookiecutter.module_name }}.forms.common import ImportForm
from {{ cookiecutter.module_name }}.forms.common import ProfileForm
from {{ cookiecutter.module_name }}.forms.common import SelectForm
from {{ cookiecutter.module_name }}.funcs import import_file
from {{ cookiecutter.module_name }}.funcs import get_config
from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.views.common import ensure_permission

# HTTPExceptions' titles
N_('Bad Request')
N_('Forbidden')
N_('Not Found')

N_('Access was denied to this resource.')
N_('The resource could not be found.')


# We use a tempfile because of fuckin' cgi.FieldStorage which may return a StringIO object
# in certain circumstances but select_func() *requires* file.name to be set (even with dumb value)
# File will be deleted when closed
def get_tempfile(qfile, kind):
    """ Copy cgi.FieldStorage content to temp file """

    content = qfile.file.read()

    if kind == 'csv':
        # Text mode
        mode = 'w+t'
        content = content.decode(qfile.encoding)
    else:
        # Binary mode
        mode = 'w+b'

    # pylint: disable=consider-using-with
    fp = tempfile.TemporaryFile(mode=mode)
    fp.write(content)
    fp.seek(0)

    return fp


@view_config(route_name='about', renderer='/about.mako', permission='index')
def about(request):
    """ About view² """

    translate = request.translate

    return {
        'breadcrumb': [(translate('About'), None)],
        'subtitle': None,
        'title': translate('About'),
    }


@view_config(route_name='api-doc', renderer='', permission='api-doc')
def api_doc(request):
    """ The API doc view """

    translate = request.translate

    renderer_values = {
        'breadcrumb': [(translate('API documentation'), None)],
        'subtitle': None,
        'title': translate('API documentation'),
    }

    return api_doc_(request, renderer_values)


@view_config(route_name='import', renderer='/import.mako', permission='import')
@validate('import', ImportForm)
def import_(request):
    """ Import view """

    session = request.session
    translate = request.translate
    form = request.forms['import']

    # Check type
    type_ = request.matchdict.get('type')
    config = get_config(type_)
    if config is None or 'import' not in config['features']:
        raise HTTPNotFound(detail=translate('Invalid import type'))

    if not os.path.isdir(config['dirpath']):
        raise HTTPNotFound(detail=translate('Invalid import directory'))

    # Check permission
    target = config['target']
    ensure_permission(request, f'{target}.import')

    if request.method == 'POST':
        if not form.errors:
            # Check suffix
            basename = f'{config["prefix"]}-{form.result["suffix"]}'
            if not form.result['force']:
                for kind in ('csv', 'xlsx'):
                    if os.path.isfile(os.path.join(config['dirpath'], f'{basename}.{kind}')):
                        form.errors['suffix'] = translate('Suffix already exists')
                        break

            # Check version
            if form.result['version'] not in config['columns']:
                form.errors['version'] = translate('Invalid value')

        if not form.errors:
            # Check file content
            qfile = form.result['qfile']
            qfile_type = mimetypes.guess_type(qfile.filename)[0]

            if qfile_type == 'text/csv':
                format_ = 'csv'
            elif qfile_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                format_ = 'xlsx'
            else:
                form.errors['qfile'] = translate('Invalid file content')

        if not form.errors:
            # Get import function
            import_func = config['import_func']

            # Compute import options
            kwargs = {}
            if config['import_options'].get('author'):
                kwargs['author'] = request.authenticated_user

            if config['import_options'].get('create_only'):
                kwargs['create_only'] = form.result['create_only']

            if config['import_options'].get('partial'):
                kwargs['columns'] = form.result['columns']

            if form.result['version']:
                kwargs['version'] = form.result['version']

            if format_ == 'xlsx':
                kwargs['sheet'] = form.result['sheet']

            # Remove existing file if needed
            for kind in ('csv', 'xlsx'):
                filepath = os.path.join(config['dirpath'], f'{basename}.{kind}')
                if os.path.isfile(filepath):
                    os.remove(filepath)

            # Save uploaded file
            filepath = os.path.join(config['dirpath'], f'{basename}.{format_}')

            with open(filepath, 'wb') as fp:
                while True:
                    buf = qfile.file.read(16384)
                    if not buf:
                        break
                    fp.write(buf)

            # Import saved file so that fp.name is correctly set
            with open(filepath, 'rb' if format_ == 'xlsx' else 'rt') as fp:
                import_data = import_file(request, target, import_func, fp, asquery=False, format=format_, **kwargs)

            # Store Ids to session to be able to create a batch if requested
            session[f'{target}.import_data'] = import_data

            request.override_renderer = '/report.mako'
            return report(request, config)

        if 'qfile' in form.errors:
            # Move errors to filename field
            form.errors['filename'] = form.errors.pop('qfile')

    elif 'dismiss' in request.params:
        session.pop(f'{target}.import_data', None)
        return HTTPFound(location=request.path)

    elif session.get(f'{target}.import_data'):
        request.override_renderer = '/report.mako'
        return report(request, config)

    else:
        data = {
            'create_only': True,
            'sheet': config.get('sheet'),
            'suffix': datetime.datetime.today().strftime('%Y%m%d-01'),
            'version': config['version'],
        }
        form.from_python(data)

    title = config['title']

    breadcrumb = [
        (title, request.route_path(f'{target}.search')),
        (translate('Import'), request.path_qs),
    ]

    options = config['import_options'].copy()
    options['sheet'] = config.get('sheet') is not None

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': request.route_path(f'{target}.search'),
        'options': options,
        'subtitle': translate('Import data'),
        'title': title,
        'versions': list(config['columns']),
    }


@exception_view_config(HTTPBadRequest, renderer='/error-400.mako')
def http_400(request):
    """ HTTP 400 error view """

    translate = request.translate

    request.response.status_int = 400

    if request.matched_route.name.startswith('api.'):
        request.override_renderer = 'json'

        return {
            'url': request.path_qs,
            'method': request.method,
            'params': request.params.mixed(),
            'errors': request.exception.json if request.exception.body else {},
            'apiVersion': '1.0',
            'result': False,
            'message': request.exception.message or translate('Invalid or missing parameter(s)'),
        }

    # Prevent from rendering wrong template
    if hasattr(request, 'override_renderer'):
        del request.override_renderer

    return {
        'breadcrumb': [],
        'title': translate(request.exception.title),
        'subtitle': None,
    }


@exception_view_config(HTTPForbidden, renderer='/error-403.mako')
@exception_view_config(HTTPUnauthorized, renderer='/error-403.mako')
def http_403(request):
    """ HTTP 403 error view """

    translate = request.translate

    if request.authenticated_user is None:
        params = get_settings(request, 'auth', 'auth')
        if params.get('policies') == ['basic']:
            # Issuing a challenge
            response = HTTPUnauthorized()
            response.headers.update(forget(request))
            return response

        return HTTPFound(location=request.route_path('auth.sign-in', _query={'redirect': request.path_qs}))

    request.response.status_int = 403

    if request.matched_route.name.startswith('api.'):
        request.override_renderer = 'json'

        return {
            'url': request.path_qs,
            'method': request.method,
            'params': request.params.mixed(),
            'apiVersion': '1.0',
            'result': False,
            'message': request.exception.message or translate('Access denied'),
        }

    # Prevent from rendering wrong template
    if hasattr(request, 'override_renderer'):
        del request.override_renderer

    return {
        'breadcrumb': [],
        'title': translate(request.exception.title),
        'subtitle': None,
    }


@exception_view_config(HTTPNotFound, renderer='/error-404.mako')
def http_404(request):
    """ HTTP 404 error view """

    translate = request.translate

    request.response.status_int = 404

    if request.matched_route and request.matched_route.name.startswith('api.'):
        request.override_renderer = 'json'

        return {
            'url': request.path_qs,
            'method': request.method,
            'params': request.params.mixed(),
            'apiVersion': '1.0',
            'result': False,
            'message': request.exception.message or translate('Page not found'),
        }

    # Prevent from rendering wrong template
    if hasattr(request, 'override_renderer'):
        del request.override_renderer

    return {
        'breadcrumb': [],
        'title': translate(request.exception.title),
        'subtitle': None,
    }


@view_config(route_name='index', renderer='/index.mako', permission='index')
def index(request):
    """ Index view """

    translate = request.translate

    return {
        'breadcrumb': [],
        'subtitle': None,
        'title': translate('Home'),
    }


@view_config(route_name='profile', renderer='/profile.mako', permission='profile')
@validate('profile', ProfileForm)
def profile(request):
    """ Update user's profile """

    translate = request.translate
    form = request.forms['profile']

    user = request.authenticated_user

    if request.method == 'POST':
        config = get_config('users')
        modify_func = config['modify_func']

        if modify_func(request, form, user):
            return HTTPFound(location=request.route_path('index'))
    else:
        data = user.to_dict()
        form.from_python(data, validate=False)

    breadcrumb = [
        (translate('Profile'), request.route_path('profile')),
    ]
    title = translate('Profile')
    subtitle = translate('«{0}» profile').format(user.fullname)
    cancel_link = request.route_path('index')

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': cancel_link,
        'subtitle': subtitle,
        'title': title,
    }


def report(request, config):
    """ Report view """

    session = request.session
    pluralize = request.pluralize
    translate = request.translate

    target = config['target']

    # Get data
    import_data = session.get(f'{target}.import_data')
    if not import_data:
        raise HTTPNotFound(detail=translate('No imported data'))

    # Batch links and redirection
    if request.has_permission(f'batches.{target}'):
        apply_batch = request.params.get('batch')
        if apply_batch:
            if apply_batch not in ('change', 'create', 'modify', 'import'):
                raise HTTPBadRequest(detail=translate('Invalid value'))

            if apply_batch == 'change':
                matched_ids = import_data['create_ids'].union(import_data['modify_ids'])
            else:
                matched_ids = import_data[f'{apply_batch}_ids']

            batch = get_batch(request, force=True, matched_ids=matched_ids, target=target, selected=True)

            return HTTPFound(location=request.route_path('batches.index', batch=batch['id']))

        remain = import_data['import_ids'].difference(import_data['create_ids'].union(import_data['modify_ids']))

        create_len = len(import_data['create_ids'])
        modify_len = len(import_data['modify_ids'])
        import_len = len(import_data['import_ids'])
        remain_len = len(remain)

        links = {}
        if create_len:
            links['create'] = (create_len, pluralize('newly created object', 'newly created objects', create_len))

        if modify_len:
            links['modify'] = (modify_len, pluralize('modified object', 'modified objects', modify_len))

        if create_len and modify_len:
            links['change'] = (create_len + modify_len, translate('newly created or modified objects'))

        if remain_len:
            if remain_len == import_len:
                links['import'] = (import_len, pluralize('imported object (not modified)', 'imported objects (not modified)', import_len))
            else:
                links['import'] = (import_len, pluralize('imported object (maybe not modified)', 'imported objects (maybe not modified)', import_len))
    else:
        links = None

    title = config['title']

    breadcrumb = [
        (title, request.route_path(f'{target}.search')),
        (translate('Import'), request.path_qs),
    ]

    subtitle = translate('Import report')

    return {
        'breadcrumb': breadcrumb,
        'links': links,
        'subtitle': subtitle,
        'target': target,
        'title': title,
    }


@view_config(route_name='select', renderer='/select.mako', permission='select')
@validate('select', SelectForm)
def select(request):
    """ Select view """

    translate = request.translate
    form = request.forms['select']

    type_ = request.matchdict.get('type')
    config = get_config(type_)
    if config is None or 'select' not in config['features']:
        raise HTTPNotFound(detail=translate('Invalid target'))

    target = config['target']
    ensure_permission(request, f'batches.{target}')

    if request.method == 'POST':
        if not form.errors:
            # Check version
            if form.result['version'] not in config['columns']:
                form.errors['version'] = translate('Invalid value')

        if not form.errors:
            qfile = form.result['qfile']
            qfile_type = mimetypes.guess_type(qfile.filename)[0]

            if qfile_type == 'text/csv':
                format_ = 'csv'
            elif qfile_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                format_ = 'xlsx'
            else:
                form.errors['qfile'] = translate('Invalid file content')

        if not form.errors:
            # Get select function
            select_func = config['select_func']

            fp = get_tempfile(qfile, format_)

            kwargs = {}
            if form.result['version']:
                kwargs['version'] = form.result['version']

            if format_ == 'xlsx':
                kwargs['sheet'] = form.result['sheet']

            # Get selection
            selected_ids = import_file(request, type_, select_func, fp, asquery=False, format=format_, **kwargs)

            # Create batch
            batch = get_batch(request, matched_ids=selected_ids, target=target, force=True, selected=True)

            return HTTPFound(location=request.route_path('batches.index', batch=batch['id']))

        if 'qfile' in form.errors:
            # Move errors to filename field
            form.errors['filename'] = form.errors.pop('qfile')
    else:
        data = {
            'sheet': config.get('sheet'),
            'version': config['version'],
        }

        form.from_python(data)

    title = config['title']

    breadcrumb = [
        (title, request.route_path(f'{target}.search')),
        (translate('Selection from file'), request.path_qs),
    ]

    options = config['select_options'].copy()
    options['sheet'] = config.get('sheet') is not None

    return {
        'breadcrumb': breadcrumb,
        'cancel_link': request.route_path(f'{target}.search'),
        'options': options,
        'subtitle': translate('Select from file'),
        'title': title,
        'versions': list(config['columns']),
    }


@view_config(route_name='auth.sign-in', renderer='/sign-in.mako')
@validate('signin', SignInForm)
def sign_in(request):
    """ Sign-in view """

    translate = request.translate
    form = request.forms['signin']

    params = get_settings(request, 'auth', 'auth')
    if params.get('policies') == ['remote']:
        params = get_settings(request, 'auth', 'policy:remote')
        login_url = params.get('login_url')
        if login_url is None:
            raise HTTPInternalServerError(detail=translate('Authentication not available, please retry later.'))

        return HTTPFound(location=login_url)

    if request.method == 'POST':
        if not form.errors:
            username = form.result['username']
            password = form.result['password']
            redirect = form.result['redirect']

            if check_credentials(request, username, password):
                headers = remember(request, username)
                return HTTPFound(location=redirect, headers=headers)

            form.errors['username'] = translate('Bad username or password')
    else:
        redirect = request.GET.get('redirect')
        if redirect is None or redirect == request.path:
            redirect = request.route_path('index')

        data = {'redirect': redirect}
        form.from_python(data)

    return {
        'title': translate('Sign in'),
    }


@view_config(route_name='auth.sign-out', renderer='/sign-out.mako')
def sign_out(request):
    """ Sign-out view """

    session = request.session
    translate = request.translate

    # Clear session
    session.delete()

    if request.authentication_policy == 'remote':
        params = get_settings(request, 'auth', 'policy:remote')
        logout_url = params.get('logout_url')
        if logout_url is None:
            # Display sign-out page
            return {
                'title': translate('Signed out'),
            }
    else:
        logout_url = request.route_path('index')

    headers = forget(request)
    return HTTPFound(location=logout_url, headers=headers)
