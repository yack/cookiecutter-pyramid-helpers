# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Searches management """

import logging

from formencode.variabledecode import variable_encode

from pyramid.httpexceptions import HTTPForbidden
from pyramid.httpexceptions import HTTPFound
from pyramid.httpexceptions import HTTPNotFound
from pyramid.view import view_config

from {{ cookiecutter.module_name }}.funcs.batches import get_batch
from {{ cookiecutter.module_name }}.funcs.searches import get_search

log = logging.getLogger(__name__)


@view_config(route_name='searches.load', permission='searches.visual')
def load(request):
    """ Load search page from saved search """

    session = request.session
    translate = request.translate

    search = get_search(request)
    if search is None:
        raise HTTPNotFound(detail=translate('Invalid search id'))

    if search.user != request.authenticated_user and not search.shared:
        raise HTTPForbidden(explanation=translate('Search is owned by another user'))

    if search.batch:
        # Create a new batch
        batch = get_batch(request, matched_ids=set(search.parameters['selected_ids']), target=search.target, force=True, selected=True)

        batch['search_id'] = search.id

        return HTTPFound(location=request.route_path('batches.index', batch=batch['id']))

    # Store search id in session
    session_key = f'[form] {search.target}.search::search_id'
    session[session_key] = search.id

    # Add 'search' parameter to query to reset current selection
    _query = variable_encode(search.parameters)
    _query['search'] = 'search'

    return HTTPFound(location=request.route_path(f'{search.target}.search', _query=_query))
