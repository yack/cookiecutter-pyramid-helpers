# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Test Invoke tasks """

# pylint: disable=redefined-builtin,unused-argument

import os

from invoke import task

from tasks.common import print_title


@task()
def eslint(context):
    """ Running eslint tests """

    print_title(eslint.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('eslint {{ cookiecutter.module_name }}/static/js > build/eslint.txt', pty=True)


@task()
def flake8(context):
    """ Running flake8 tests """

    print_title(flake8.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('flake8 {{ cookiecutter.module_name }} tasks tests > build/flake8.txt', pty=True)


@task()
def functional(context, test=None):
    """ Running functional tests """

    print_title(functional.__doc__)

    os.makedirs('build', exist_ok=True)

    cmd = 'pytest --disable-warnings --verbose --cov-report=html:build/coverage --cov-report=term --cov-report=xml:build/coverage.xml --cov={{ cookiecutter.module_name }} --junit-xml=build/pytest.xml tests/'

    if test:
        cmd = f'{cmd}{test}'

    context.run(cmd, pty=True)


@task()
def pylint(context):
    """ Running pylint tests """

    print_title(pylint.__doc__)

    os.makedirs('build', exist_ok=True)

    context.run('pylint {{ cookiecutter.module_name }} tasks tests > build/pylint.txt', pty=True)


@task(eslint, flake8, pylint)
def static(context):
    """ Run the static tests pipeline """


@task(static, functional, default=True)
def all(context):
    """ Run the test pipeline """
