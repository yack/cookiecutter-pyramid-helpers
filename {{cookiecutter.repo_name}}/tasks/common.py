# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Common functions for Invoke tasks """

import os
import subprocess

from termcolor import cprint


def load_environ(filepath, reset=True):
    """
    Load environment variables from file

    @param string filepath: Path of the file to load environment variables from
    @param boolean reset: Whether to reset environment or not before loading the environment variables (Default is to reset the environment)
    """

    if not filepath:
        return

    cmd = f'set -a && . {filepath} && env -0'
    if reset:
        cmd = f'env -i sh -c "{cmd}"'

    output = subprocess.getoutput(cmd)

    os.environ.update(dict(
        line.split('=', 1)
        for line in output.split('\x00')
        if '=' in line
    ))


def print_title(title):
    """ Print a colored title """

    cprint(f'\n* {title.strip()}', 'blue')
