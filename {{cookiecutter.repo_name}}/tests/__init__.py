# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Common functions for API """

from robber import expect

# pylint: disable=no-member


def extract_result(response, status_code, status):
    """  Extract result from response """

    expect(response.status_code).to.eq(status_code)

    try:
        result = response.json
    except ValueError:
        result = None

    expect(result).to.be.a.dict()
    expect(result).to.contain('apiVersion', 'method', 'result')
    expect(result['apiVersion']).to.eq('1.0')

    if status:
        expect(result['result']).to.be.true()
    else:
        expect(result['result']).to.be.false()

    return result
