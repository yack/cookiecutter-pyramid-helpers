# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Tests functions for {{ cookiecutter.project_name }} """

import pytest

from pyramid.events import NewRequest

import transaction
from webtest import TestApp as TestApp_

from pyramid_helpers.forms import State

from {{ cookiecutter.module_name }} import main
from {{ cookiecutter.module_name }}.models import Base
from {{ cookiecutter.module_name }}.models import DBSession
from {{ cookiecutter.module_name }}.scripts.initializedb import add_users


SETTINGS = {
    'use': 'egg:{{ cookiecutter.module_name }}',

    # Auth
    'auth.enabled': 'true',
    'auth': {
        'auth': {
            'backend': 'database',
            'policies': 'cookie',
            'get_principals': '{{ cookiecutter.module_name }}.auth.get_principals',
            'get_user_by_username': '{{ cookiecutter.module_name }}.funcs.users.get_user_by_username',
        },

        'policy:basic': {
            'realm': '{{ cookiecutter.project_name }} Application',
        },

        'policy:cookie': {
            'secret': 'the-big-secret-for-secured-authentication',
            'hashalg': 'sha512',
        },

        'policy:remote': {
            # 'fake_user': 'admin',
            'header': 'X-FORWARDED-USER',
            'login_url': 'https://idp.domain.tld/login',
            'logout_url': 'https://idp.domain.tld/logout',
        },
    },

    # Forms
    'forms.enabled': 'true',

    # I18n
    'i18n.enabled': 'true',
    'i18n.available_languages': 'fr en',
    'i18n.default_locale_name': 'en',
    'i18n.directories': '{{ cookiecutter.module_name }}:locale formencode:i18n',
    'i18n.domain': '{{ cookiecutter.module_name }}',

    # LDAP
    'ldap.enabled': 'false',

    # Pagers
    'pagers.enabled': 'true',
    'pagers.limit': '10 20 30 50',

    # Predicates
    'predicates.enabled': 'true',

    # Radius
    'radius.enabled': 'false',

    # Renderers
    'renderers.json.enabled': 'true',

    # Default timezone
    'timezone': 'Europe/Paris',

    # Third party libraries
    'mako.directories': '{{ cookiecutter.module_name }}:templates pyramid_helpers:templates',
    'sqlalchemy.url': 'sqlite:///',

    # Import/export
    'exports.dirpath': '%(here)s/../data/exports',
    'imports.dirpath': '/tmp',
}


USERS = {
    'admin': {
        'profiles': ['admin', ],
        'password': 'admin',
    },  # Timezone will be set to Europe/Paris
    'user1': {
        'profiles': ['user', ],
        'firstname': 'John',
        'lastname': 'Doe',
        'password': 'user1',
        'timezone': 'UTC',
    },  # Hum... UTC
    'user2': {
        'profiles': ['user', ],
        'firstname': 'Jane',
        'lastname': 'Doe',
        'password': 'user2',
        'timezone': 'Indian/Reunion',
    },  # UTC+4
    'user3': {
        'profiles': ['user', ],
        'password': 'user3',
        'timezone': 'Invalid',
    },  # Timezone will be set to Europe/Paris
}


# pylint: disable=redefined-outer-name,no-member


class TestRoute:
    """ Test route for Pyramid-Helpers """

    # pylint: disable=too-few-public-methods
    name = 'index'


class TestApp(TestApp_):
    """ Test application for Pyramid-Helpers """

    def __init__(self, app, **kwargs):
        # calling inherited
        super().__init__(app, **kwargs)

        # For `route_path()`
        self.__request = app.request_factory.blank('index')
        self.__request.registry = app.registry

    def get_request(self, route_name, *elements, data=None, method='GET', **kw):
        """ Get a request object """

        path = self.route_path(route_name, *elements, **kw)

        request = self.app.request_factory.blank(path, POST=data)
        request.headers['Accept-Language'] = 'en'
        request.matched_route = TestRoute()
        request.method = method
        request.registry = self.app.registry

        # Setup the request
        self.app.registry.notify(NewRequest(request))

        return request

    def login(self, username, password, redirect='/'):
        """ Log in to application """

        # Perform login
        params = {
            'username': username,
            'password': password,
            'redirect': redirect,
        }

        return self.post(self.route_path('auth.sign-in'), params=params)

    def logout(self):
        """ Log out from application """

        return self.get(self.route_path('auth.sign-out'))

    def route_path(self, route_name, *elements, **kw):
        """ Wrapper to `Request.route_path()` """

        return self.__request.route_path(route_name, *elements, **kw)


@pytest.fixture(scope='session')
def wsgi_app():
    """ Initialize {{ cookiecutter.project_name }} application """

    # Create WSGI application
    app = main({}, **SETTINGS)

    # Initialize the database
    Base.metadata.create_all(bind=DBSession.get_bind())

    # Populate table users
    with transaction.manager:
        add_users(None, USERS)

    return app


@pytest.fixture(scope='session')
def app(wsgi_app):
    """ Initialize main session (anonymous) """

    yield TestApp(wsgi_app)


@pytest.fixture(scope='session')
def app_as_admin(wsgi_app):
    """ Initialize a session for user admin """

    app = TestApp(wsgi_app)

    app.login('admin', 'admin')

    yield app

    app.logout()


@pytest.fixture(scope='session')
def app_as_user1(wsgi_app):
    """ Initialize a session for user user1 """

    app = TestApp(wsgi_app)

    app.login('user1', 'user1')

    yield app

    app.logout()


@pytest.fixture(scope='session')
def app_as_user2(wsgi_app):
    """ Initialize a session for user user2 """

    app = TestApp(wsgi_app)

    app.login('user2', 'user2')

    yield app

    app.logout()


@pytest.fixture(scope='session')
def app_as_user3(wsgi_app):
    """ Initialize a session for user user3 """

    app = TestApp(wsgi_app)

    app.login('user3', 'user3')

    yield app

    app.logout()


@pytest.fixture(scope='session')
def state(app):
    """ Initialize main session (anonymous) """

    yield State(app.get_request('validators'))
