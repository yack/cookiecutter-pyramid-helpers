# {{ cookiecutter.project_name }} -- {{ cookiecutter.project_desc }}
# By: {{ cookiecutter.author_name }} <{{ cookiecutter.author_email }}>
#
# Copyright (C) {{ cookiecutter.copyright_name }}
# {{ cookiecutter.project_url }}
#
# This file is part of {{ cookiecutter.project_name }}
#
# {{ cookiecutter.project_name }} is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# {{ cookiecutter.project_name }} is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Test functions for users """

from pytest_steps import test_steps
from robber import expect

from . import extract_result

# pylint: disable=no-member


@test_steps('anonymous', 'admin_ok')
def test_visual(app, app_as_admin):
    """ Test function for api.users.visual view """

    # User #2
    url = app.route_path('api.users.visual', user=2)

    response = app.get(url, expect_errors=True)
    expect(response.status_code).to.eq(302)
    expect(response.headers.get('location')).to.contain('http://localhost/auth/sign-in')
    yield

    response = app_as_admin.get(url)
    result = extract_result(response, 200, True)

    expect(result).to.contain('user')

    for key in ('creation_date', 'modification_date'):
        result['user'].pop(key)

    expect(result['user']).to.eq({
        'id': 2,
        'firstname': 'John',
        'fullname': 'John Doe',
        'lastname': 'Doe',
        'profiles': ['user', ],
        'status': 'active',
        'username': 'user1',
        'description': None,
        'email': 'user1@domain.tld',
        'timezone': 'UTC',
        'token': None,
    })
    yield
